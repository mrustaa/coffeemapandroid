package mrusta.coffeeMapAndroid.ui.launch;

import mrusta.coffeeMapAndroid.R;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

import mrusta.coffeeMapAndroid.managers.CoffeeDataManagerNew;
import mrusta.coffeeMapAndroid.managers.CoffeeRemoteConfig;
import mrusta.coffeeMapAndroid.managers.pushNotification.CoffeePushNotificationManager;
import mrusta.coffeeMapAndroid.managers.utils.HideStatusBar;
import mrusta.coffeeMapAndroid.managers.utils.ViewAlphaAnimation;
import mrusta.coffeeMapAndroid.model.CoffeeAddress;
import mrusta.coffeeMapAndroid.ui.screen.list.CoffeeListActivity;
import static mrusta.coffeeMapAndroid.managers.utils.MainThread.delay;
import java.util.List;

public class LaunchActivity extends AppCompatActivity  {
    //----------------------------------------------------------------------------------------------
    ImageView launchImage;
    TextView  launchText;
    //----------------------------------------------------------------------------------------------
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_screen);

        HideStatusBar.hideStatusBar(this);

        launchImage = findViewById(R.id.imageNew);
        //launchText  = findViewById(R.id.text);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        CoffeePushNotificationManager.loadContext(this);

        AppCenter.start(getApplication(), "0d054c86-5644-444a-8b8a-745bd953c888", Analytics.class, Crashes.class);

        CoffeeDataManagerNew.setContext(this);
        CoffeeDataManagerNew.setLoadingLocalCallback(new CoffeeDataManagerNew.LoadingLocalCallback() {

            @Override public void loadingLocalCallback(List<CoffeeAddress> result) {
                                                                                                    Log.d("✅", "LaunchActivity | loadingLocal.Callback ");
                //startDelayMainActivity();
            }
        });

        CoffeeDataManagerNew.setDownloadFirebaseCallback(new CoffeeDataManagerNew.DownloadFirebaseCallback() {
            @Override public void downloadFirebaseCallback(final List<CoffeeAddress> result) {
                                                                                                    Log.d("✅", "LaunchActivity | downloadFirebase.Callback ");
                startDelayMainActivity();
            }
        });
        CoffeeDataManagerNew.loadCoffee();


        CoffeeRemoteConfig.load(this, new Runnable() { @Override public void run() {

        }});
    }

    //----------------------------------------------------------------------------------------------
    void startDelayMainActivity() {

        delay(600, new Runnable() { @Override public void run() {

            ViewAlphaAnimation.change(launchImage, false, 600);

            delay(600, new Runnable() { @Override public void run() {
                startMainActivity();
            }});
        }});
    }
    //----------------------------------------------------------------------------------------------
    void startMainActivity() {

        Intent intent = new Intent(this, CoffeeListActivity.class);
        startActivity(intent);

        finish();
    }

    //----------------------------------------------------------------------------------------------
}