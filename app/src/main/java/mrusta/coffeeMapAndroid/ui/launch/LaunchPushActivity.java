package mrusta.coffeeMapAndroid.ui.launch;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Objects;

import mrusta.coffeeMapAndroid.R;
import mrusta.coffeeMapAndroid.managers.CoffeeDataManagerNew;
import mrusta.coffeeMapAndroid.managers.utils.HideStatusBar;

public class LaunchPushActivity extends AppCompatActivity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_screen);

        HideStatusBar.hideStatusBar(this);

        Objects.requireNonNull(getSupportActionBar()).hide();

        CoffeeDataManagerNew.pushNotification = true;

        NotificationManager ntfManager = getSystemService(NotificationManager.class);
        ntfManager.cancelAll();

        startLaunchActivity();
    }

    void startLaunchActivity() {

        Intent intent = new Intent(this, LaunchActivity.class);
        startActivity(intent);

        finish();
    }
}
