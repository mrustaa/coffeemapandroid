package mrusta.coffeeMapAndroid.ui.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import java.util.Timer;
import java.util.TimerTask;

import mrusta.coffeeMapAndroid.R;
import mrusta.coffeeMapAndroid.managers.CoffeeDataManagerNew;
import mrusta.coffeeMapAndroid.managers.utils.DarkMode;
import mrusta.coffeeMapAndroid.managers.utils.ViewAlphaAnimation;

import static android.content.Context.WINDOW_SERVICE;
import static com.microsoft.appcenter.utils.HandlerUtils.runOnUiThread;

public class CoffeeSearchBar extends SearchView {
    //----------------------------------------------------------------------------------------------
    Timer timer;
    static boolean pause;
    static Runnable showLoaderScreenAnimationCallback,
            closeCallback,
              updateSortFilterCallback;

    public boolean open;
    public TextView textView;
    //----------------------------------------------------------------------------------------------
    public CoffeeSearchBar(Context context)
    {
        super(context);
        update();
    }

    public CoffeeSearchBar(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        update();
    }

    public CoffeeSearchBar(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        update();
    }
    //----------------------------------------------------------------------------------------------
    public void updateCallback(Runnable showLoaderScreenAnimation, Runnable updateSortFilter, Runnable closeCallback) {

        CoffeeSearchBar.showLoaderScreenAnimationCallback = showLoaderScreenAnimation;
        CoffeeSearchBar.updateSortFilterCallback = updateSortFilter;
        CoffeeSearchBar.closeCallback = closeCallback;
    }

    //----------------------------------------------------------------------------------------------
    public void update() {

        setOnCloseListener(new SearchView.OnCloseListener() {
            @Override public boolean onClose() {
                open = false;
                if (CoffeeSearchBar.closeCallback != null) {
                    CoffeeSearchBar.closeCallback.run();
                }
                changeWidthSearchView(false);
                return false;
            }
        });

        setOnSearchClickListener(new SearchView.OnClickListener() {
            @Override public void onClick(View v) {
                changeWidthSearchView(true);
                //searchView.setIconified(false);
            }
        });

        setOnQueryTextListener( new SearchView.OnQueryTextListener() {

            @Override public boolean onQueryTextSubmit(String query) {

                clearFocus();
                return true;
                //return false;
            }

            @Override public boolean onQueryTextChange(String newText) {

                CoffeeDataManagerNew.searchText = newText;
                changeSearchViewTextTimer(newText);

                return false;
            }
        });

        Context context = getContext();
        Resources resources = context.getResources();

        int search_close_btn    = resources.getIdentifier("android:id/search_close_btn", null, null);
        int search_button       = resources.getIdentifier("android:id/search_button",    null, null);

        ImageView imageView4 = (ImageView) findViewById(search_close_btn);
        ImageView imageView5 = (ImageView) findViewById(search_button);


        //imageView4.setColorFilter(getResources().getColor(R.color.coffeeGreen));

        boolean darkMode = DarkMode.context(context);

//        imageView5.setColorFilter(getResources().getColor(darkMode ? R.color.white : R.color.black));

        imageView5.setColorFilter(getResources().getColor(R.color.clear)); // darkMode ? R.color.black : R.color.white));
//        imageView5.setVisibility(View.GONE);



        int search_src_text = resources.getIdentifier("android:id/search_src_text", null, null);
        textView = (TextView) findViewById(search_src_text);
        Typeface tf = ResourcesCompat.getFont(context, R.font.proxima_nova_regular);
        textView.setTypeface(tf);


    }

    public void selectOpen(boolean open) {
        ViewAlphaAnimation.change(this, open, 0);
        setIconified(!open);
        this.open = open;
    }

    //----------------------------------------------------------------------------------------------
    void changeWidthSearchView(boolean show) {

        WindowManager wm = (WindowManager) this.getContext().getSystemService(WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        float density = getResources().getDisplayMetrics().density;

        int height = (int) (44.0 * density);
        int width_  = height;
        if (show) {
            int width  = display.getWidth();    // deprecated
            float width_dp = (width / density);
            float padding_dp = (6 + 6);
            float result_dp = (width_dp - padding_dp);
            width_ = (int) (result_dp * density);
        }

        Log.d("❤️ ", "❤️ 4121");

        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        layoutParams.width = width_;
        setLayoutParams(layoutParams);
    }
    //----------------------------------------------------------------------------------------------
    void changeSearchViewTextTimer(String newText) {

        if (timer != null) timer.cancel();
        timer = new Timer();

        if (newText.isEmpty()) {
            changeSearchViewText();
        } else {

            int period = 1000;
            timer.scheduleAtFixedRate( new TimerTask() {
                @Override public void run() {

                    runOnUiThread(new Runnable() {
                        public void run() {

                            changeSearchViewText();
                            timer.cancel();
                        }
                    });

                }
            }, period, period);
        }
    }
    //----------------------------------------------------------------------------------------------
    void changeSearchViewText() {

        if (pause) {
            return;
        } else {
            pause = true;

            showLoaderScreenAnimationCallback.run();

            new Thread(new Runnable() { @Override public void run() {
                try { Thread.sleep(1000); } catch (InterruptedException e) { }
                runOnUiThread(new Runnable() { @Override public void run() {
                    pause = false;
                }});
            }}).start();
        }

        Log.d("❤️ ", "changeSearchViewText");

        updateSortFilterCallback.run();
    }
    //----------------------------------------------------------------------------------------------
}
