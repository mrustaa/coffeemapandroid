package mrusta.coffeeMapAndroid.ui.view;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import mrusta.coffeeMapAndroid.R;
import mrusta.coffeeMapAndroid.managers.CoffeeDataManagerNew;
import mrusta.coffeeMapAndroid.managers.CoffeeDateFormater;
import mrusta.coffeeMapAndroid.managers.CoffeeUtils;
import mrusta.coffeeMapAndroid.model.Coffee;
import mrusta.coffeeMapAndroid.model.CoffeeAddress;


public class CoffeeMapBannerView extends ConstraintLayout {
    //----------------------------------------------------------------------------------------------
    public TextView title, distance, text, price;
    public CoffeeAddress address;
    public Coffee coffee;
    public ImageView fav, news;
    //----------------------------------------------------------------------------------------------
    public CoffeeMapBannerView(Context context) {
        super(context);

        initView();

        title       = (TextView) this.findViewById(R.id.title    );
        distance    = (TextView) this.findViewById(R.id.distance );
        text        = (TextView) this.findViewById(R.id.text     );
        price       = (TextView) this.findViewById(R.id.price    );

        fav  = this.findViewById(R.id.favorite);
        fav.setVisibility(View.GONE);

        news = this.findViewById(R.id.news);
        news.setVisibility(View.GONE);
    }
    //----------------------------------------------------------------------------------------------
    public void updateCoffeeAddress(CoffeeAddress address) {
        this.address = address;
        this.coffee = address.coffee;

        title.setText(coffee.title);

        String strDistance = CoffeeUtils.getMeterDistance(address.distance);
        distance.setText( strDistance );

        String strPrice = CoffeeUtils.getStrEspressoCappuccino(coffee.espresso, coffee.cappuccino);

        //strPrice = Utils.removingSpaces(strPrice);

        price.setText( strPrice );

        String time = CoffeeDateFormater.todayDayWeekTime(address);
        text.setText( time );


        if (address.new_) {
            news.setVisibility(View.VISIBLE);
        } else {
            news.setVisibility(View.GONE);
        }
//        if (address.favorite) {
//            fav.setVisibility(View.VISIBLE);
//        } else {
//            fav.setVisibility(View.GONE);
//        }
//
//        if (address.new_ && address.favorite) {
//            fav.setX(120);
//        }
    }
    //----------------------------------------------------------------------------------------------
    private void initView() {
        inflate(getContext(), R.layout.cell_coffee_map, this);

        this.setFocusable(false);
        this.setClickable(false);

                        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
                    Display display = wm.getDefaultDisplay();
        // int displayWidth_px  = display.getWidth();
        // int displayHeight_px = display.getHeight();

        float density = getContext().getResources().getDisplayMetrics().density;

        // String s = String.format("|w %d |h %d", displayWidth_px, displayHeight_px );
        // Log.d("\uD83D\uDD25", s);

        //        view.setX(size * 2);
        //        view.setY(y);

        //        AdView adView = new AdView(this,"ad_url","my_ad_key",true,true);
        //
        //         mAdView.setLayoutParams(new FrameLayout.LayoutParams(ConstraintLayout.LayoutParams.FILL_PARENT,height));
        //        adView.setAdListener(this);
        //        main.addView(adView);

        float displayWidth = (display.getWidth() / density);

        float padding = 16;
        float height  = 88;
        float width   = (displayWidth - (padding * 2));

        int width_  = (int) (width  * density);
        int height_ = (int) (height * density);

        this.setLayoutParams(new FrameLayout.LayoutParams(width_, height_));
        this.setBackgroundColor(0x00000000);
    }
    //----------------------------------------------------------------------------------------------

//    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//
//    }

}
