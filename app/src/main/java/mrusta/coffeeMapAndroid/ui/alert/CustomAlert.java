package mrusta.coffeeMapAndroid.ui.alert;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import androidx.constraintlayout.widget.ConstraintLayout;

import mrusta.coffeeMapAndroid.R;
import mrusta.coffeeMapAndroid.ui.modal.CoffeeMenuActivity;
import mrusta.coffeeMapAndroid.ui.modal.CoffeeMenuActivityCallback;

public class CustomAlert extends Dialog implements android.view.View.OnClickListener {
    //----------------------------------------------------------------------------------------------
    public Activity c;
    public Dialog d;
    public ConstraintLayout moscow, sbp, kazan, main;
    static CustomAlertActivity callback;
    //----------------------------------------------------------------------------------------------
    public CustomAlert(Activity a, CustomAlertActivity callback) {
        super(a);

        this.c = a;
        CustomAlert.callback = callback;

        this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }
    //----------------------------------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_alert);

        moscow  = (ConstraintLayout) findViewById(R.id.view1);
        sbp     = (ConstraintLayout) findViewById(R.id.view2);
        kazan   = (ConstraintLayout) findViewById(R.id.view3);

        main    = (ConstraintLayout) findViewById(R.id.custom);

        moscow.setOnClickListener(this);
           sbp.setOnClickListener(this);
         kazan.setOnClickListener(this);

        main.setOnClickListener(this);

        this.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override public void onCancel(DialogInterface dialogInterface) {

                callback.customAlertCallback(-1);
            }
        });
    }
    //----------------------------------------------------------------------------------------------
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.view1: callback.customAlertCallback(0); break;
            case R.id.view2: callback.customAlertCallback(1); break;
            case R.id.view3: callback.customAlertCallback(2); break;
        }
        dismiss();
    }
    //----------------------------------------------------------------------------------------------
}
