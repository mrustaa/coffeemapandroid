package mrusta.coffeeMapAndroid.ui.modal;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import mrusta.coffeeMapAndroid.BuildConfig;
import mrusta.coffeeMapAndroid.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.RecyclerAdapter;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.RecyclerAdapterClick;
import mrusta.coffeeMapAndroid.managers.CoffeeDataLocal;
import mrusta.coffeeMapAndroid.managers.CoffeeDataManagerNew;
import mrusta.coffeeMapAndroid.managers.CoffeeRemoteConfig;
import mrusta.coffeeMapAndroid.managers.CoffeeUrlScheme;
import mrusta.coffeeMapAndroid.managers.CoffeeWebView;
import mrusta.coffeeMapAndroid.managers.utils.UtilsAlert;
import mrusta.coffeeMapAndroid.ui.cell.coffeeMenu.CoffeeMenuAboutCell;
import mrusta.coffeeMapAndroid.ui.cell.coffeeMenu.CoffeeMenuClick;
import mrusta.coffeeMapAndroid.ui.cell.coffeeMenu.CoffeeMenuMapSegmentCell;
import mrusta.coffeeMapAndroid.ui.cell.coffeeMenu.CoffeeMenuSocialNetworkCell;
import mrusta.coffeeMapAndroid.ui.cell.coffeeMenu.CoffeeMenuSocialTitleCell;
import mrusta.coffeeMapAndroid.ui.cell.TitleTextCell;
import mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew.CoffeeDetailsNewSectionCell;
import mrusta.coffeeMapAndroid.ui.screen.list.CoffeeListActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static mrusta.coffeeMapAndroid.managers.CoffeeDataLocal.DataType.MAPTYPE;
import static mrusta.coffeeMapAndroid.managers.CoffeeRemoteConfig.httpUrl;
import static mrusta.coffeeMapAndroid.managers.CoffeeRemoteConfig.httpsUrl;


public class CoffeeMenuActivity extends BottomSheetDialogFragment implements RecyclerAdapterClick, CoffeeMenuClick {
    //----------------------------------------------------------------------------------------------
    static CoffeeMenuClick segmentClickIndexCallback;
    static CoffeeMenuActivityCallback menuCloseCallback;
    int selectedMapType;
    CoffeeListActivity a;

    int bottomMenuCount = 0;

    ConstraintLayout layerr;

    RecyclerView recyclerView;
    RecyclerAdapter adapter;
    List <RecyclerAdapterCell> items = new ArrayList<>();
    //----------------------------------------------------------------------------------------

    public CoffeeMenuActivity(CoffeeListActivity a) {
        this.a = a;
        this.selectedMapType = CoffeeDataLocal.loadDataKeyInt(MAPTYPE);
        CoffeeMenuActivity.segmentClickIndexCallback = (CoffeeMenuClick) a;
    }
    //----------------------------------------------------------------------------------------
    public CoffeeMenuActivity() {

    }
    //----------------------------------------------------------------------------------------
    public void setUpListener(CoffeeMenuActivityCallback callback) {
        CoffeeMenuActivity.menuCloseCallback = callback;
    }
    //----------------------------------------------------------------------------------------
    @Override public void onCancel(@NonNull DialogInterface dialog) {

        if (menuCloseCallback != null) {
            menuCloseCallback.menuClose();
        }

        super.onCancel(dialog);
        // Toast.makeText(MainApp.get(), "CANCEL", Toast.LENGTH_SHORT).show();
    }
    //----------------------------------------------------------------------------------------
    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.activity_coffee_menu, container, false);



        layerr = v.findViewById(R.id.layerr);
        recyclerView = v.findViewById(R.id.recycler);

        updateRecyclerView();

        return v;
    }
    //----------------------------------------------------------------------------------------
    @Override public int getTheme() {

        int currentNightMode = getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;

        int theme = 0;
        if (currentNightMode == Configuration.UI_MODE_NIGHT_NO) {
            theme = R.style.BottomSheetDialogTheme;
        } else if (currentNightMode == Configuration.UI_MODE_NIGHT_YES) {
            theme = R.style.BottomSheetDialogThemeDark;
        }

        return theme;
    }
    //----------------------------------------------------------------------------------------
    @Override public Dialog onCreateDialog(Bundle savedInstanceState) {
         return new BottomSheetDialog(requireContext(), this.getTheme());
    }

//    @Override protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_coffee_menu);
//
//        updateRecyclerView();
//    }
    //----------------------------------------------------------------------------------------
    private void updateRecyclerView() {

        Log.d("❤️ Table Update", "updateRecyclerView updateItems");

        updateItems();

        adapter = new RecyclerAdapter(items);
        recyclerView.setAdapter(adapter);
    }
    //----------------------------------------------------------------------------------------
    private void updateItems() {

        items.clear();

        items.add( new CoffeeDetailsNewSectionCell("Меню") );

        items.add( new CoffeeMenuMapSegmentCell(selectedMapType, this) );

        items.add( new TitleTextCell( this,"Предложения и пожелания", true ) );
        items.add( new TitleTextCell( this,"Анкета для кофеен", true ) );
        items.add( new TitleTextCell( this,"О Проекте", true ) );

        items.add( new CoffeeMenuSocialTitleCell("Связаться с нами") );
        items.add( new CoffeeMenuSocialNetworkCell(this) );

        if (bottomMenuCount > 10) {
            items.add( new TitleTextCell( this, CoffeeDataManagerNew.pushToken, true ) );
        }

        items.add( new CoffeeMenuAboutCell( this,"Кофейная карта Москвы", getVersinBuild()) );
    }
    //----------------------------------------------------------------------------------------
    String getVersinBuild() {

        int    versionCode = BuildConfig.VERSION_CODE;
        String versionName = BuildConfig.VERSION_NAME;

        return String.format("Версия %s (%s)", versionName, versionCode);
    }
    //----------------------------------------------------------------------------------------
    @Override public void recyclerViewListClicked(View v, int position) {

        Log.d("❤️ Table Click", String.valueOf(position));

        if (position == 2) {
            CoffeeUrlScheme.email(a, CoffeeRemoteConfig.email_feedback, "Написать");
        } else if (position == 3) {
            CoffeeWebView.url(a, httpUrl(CoffeeRemoteConfig.url_send_forms));
        } else if (position == 4) {
            CoffeeWebView.url(a, httpUrl(CoffeeRemoteConfig.url_site));
        } else if (position == 7) {

            if (bottomMenuCount <= 10) {

                bottomMenuCount = bottomMenuCount + 1;
                Log.d("❤️ Menu bottomMenuCount", String.valueOf(bottomMenuCount));
                if (bottomMenuCount > 10) {


                    float d = this.a.getResources().getDisplayMetrics().density;
                    layerr.getLayoutParams().height = (int)(630 * d);
                    layerr.requestLayout();

//                    layerr.set((int) (616 * d));

                    // ViewGroup.LayoutParams.MATCH_PARENT; // LayoutParams: android.view.ViewGroup.LayoutParams
                    // wv.getLayoutParams().height = LayoutParams.WRAP_CONTENT;

                    updateRecyclerView();

                } else {

                    float d = this.a.getResources().getDisplayMetrics().density;
                    layerr.getLayoutParams().height = (int)(516 * d);
                    layerr.requestLayout();
                }
            } else {

                ClipData myClip;
                ClipboardManager clipboard = (ClipboardManager)this.a.getSystemService(this.a.CLIPBOARD_SERVICE);

                myClip = ClipData.newPlainText("Скопирован токен", CoffeeDataManagerNew.pushToken);
                clipboard.setPrimaryClip(myClip);

                UtilsAlert.alertToastLong(this.a, "Скопирован токен девайса");
            }

        }
    }
    //----------------------------------------------------------------------------------------
    @Override public void segmentClicked(int index) {
        CoffeeDataLocal.saveDataKeyInt(index, MAPTYPE);
        segmentClickIndexCallback.segmentClicked(index);
    }
    //----------------------------------------------------------------------------------------
    @Override public void socialNetworkClick(int index) {

        if (index == 0) {
            CoffeeUrlScheme.email(a, CoffeeRemoteConfig.email_ios, "По техническим вопросам");
        } else if (index == 1) {
            CoffeeUrlScheme.instagram(a, httpUrl(CoffeeRemoteConfig.url_instagram) );
        } else if (index == 2) {
            CoffeeUrlScheme.facebook(a, httpsUrl(CoffeeRemoteConfig.url_facebook) );
        } else if (index == 3) {
            CoffeeUrlScheme.telegram(a, httpUrl(CoffeeRemoteConfig.url_telegram) );
        }


        segmentClickIndexCallback.socialNetworkClick(index);
    }
    //----------------------------------------------------------------------------------------
}