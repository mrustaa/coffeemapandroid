package mrusta.coffeeMapAndroid.ui.cell.coffeeList;

import android.view.View;

public interface CoffeeMarketTagClick {
    void selectedTagIndex(int index);
}

