package mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew;

import androidx.annotation.NonNull;
import mrusta.coffeeMapAndroid.R;
import androidx.recyclerview.widget.RecyclerView;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;
import android.content.Context;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class CoffeeDetailsNewSectionCell extends RecyclerAdapterCell {
    //------------------------------------------------------------------------------------
    @Override public int type() { return 66450; }

    @Override public void fill(RecyclerView.ViewHolder viewHolder) {
        CoffeeDetailsNewSectionHolder holder = (CoffeeDetailsNewSectionHolder) viewHolder;
                holder.text0.setText(text0);
        
    }

    public static class CoffeeDetailsNewSectionHolder extends RecyclerView.ViewHolder {

        public TextView text0;
    

        public CoffeeDetailsNewSectionHolder(View itemView) {
            super(itemView);

            text0 = (TextView) itemView.findViewById(R.id.text0);
    
        }
    }
    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return CoffeeDetailsNewSectionCell.createHolder(parent);
    }

    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
        View xml = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_coffee_details_new_section, parent,false);
        return new CoffeeDetailsNewSectionCell.CoffeeDetailsNewSectionHolder(xml);
    }
    //------------------------------------------------------------------------------------
    private String text0;
    

    public CoffeeDetailsNewSectionCell(String text0) {

        this.text0 = text0;
    
    }
    //------------------------------------------------------------------------------------
}