package mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew;

import androidx.annotation.NonNull;
import mrusta.coffeeMapAndroid.R;
import androidx.recyclerview.widget.RecyclerView;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import mrusta.coffeeMapAndroid.model.CoffeeAddress;

import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;
import android.content.Context;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class CoffeeDetailsNewInfoMiniCell extends RecyclerAdapterCell {
    //------------------------------------------------------------------------------------
    @Override public int type() { return 31999; }

    @Override public void fill(RecyclerView.ViewHolder viewHolder) {
        final CoffeeDetailsNewInfoMiniHolder holder = (CoffeeDetailsNewInfoMiniHolder) viewHolder;


        if (address != null) {

            if (times().length == 1) {
                holder.button.setVisibility(View.GONE);
                shownFull = true;
            }

            holder.button.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {

                    holder.button.setVisibility(View.GONE);
                    shownFull = true;

                    holder.text1.setText(timesLines());

                    holder.text1.setLines(holder.text1.getLineCount());
                }
            });


            holder.text0.setText("Часы работы");
            holder.text1.setLines(1);
            holder.text1.setText(times()[0]);
            holder.button.setText("Показать все");

        } else {

            holder.text0.setText(text0);
            holder.text1.setText(text1);
            holder.button.setText(text2);
        }

    }

    public String[] times() {
        return address.time.split(", ");
    }

    public String timesLines() {

        String result = "";

        for (String time : times()) {

            if (result.isEmpty()) {
                result = time;
            } else {
                result = String.format("%s\n%s", result, time);
            }
        }
        return result;
    }

    public static class CoffeeDetailsNewInfoMiniHolder extends RecyclerView.ViewHolder {
        public TextView text0;
        public TextView text1;
        public TextView button;

        public CoffeeDetailsNewInfoMiniHolder(View itemView) {
            super(itemView);

            text0 = (TextView) itemView.findViewById(R.id.text0);
            text1 = (TextView) itemView.findViewById(R.id.text1);
            button = (TextView) itemView.findViewById(R.id.button);
        }
    }
    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return CoffeeDetailsNewInfoMiniCell.createHolder(parent);
    }

    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
        View xml = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_coffee_details_new_info_mini, parent,false);
        return new CoffeeDetailsNewInfoMiniCell.CoffeeDetailsNewInfoMiniHolder(xml);
    }
    //------------------------------------------------------------------------------------
    private String text0, text1, text2;
    private CoffeeAddress address;
    boolean shownFull;

    public CoffeeDetailsNewInfoMiniCell(CoffeeAddress address) {
        this.address = address;
    }

    public CoffeeDetailsNewInfoMiniCell(String text0, String text1) {
        this.text0 = text0;
        this.text1 = text1;
        this.text2 = "";
    }

    public CoffeeDetailsNewInfoMiniCell(String text0, String text1, String text2) {
        this.text0 = text0;
        this.text1 = text1;
        this.text2 = text2;
    }
    //------------------------------------------------------------------------------------
}