package mrusta.coffeeMapAndroid.ui.cell;

import mrusta.coffeeMapAndroid.R;
import androidx.recyclerview.widget.RecyclerView;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.RecyclerAdapterClick;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ImageView;
import android.content.Context;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class CoffeeListEmplyInfoCell extends RecyclerAdapterCell {
    //------------------------------------------------------------------------------------
    @Override public int type() { return 53225; }
    //----------------------------------------------
    @Override public void fill(RecyclerView.ViewHolder viewHolder) {
        CoffeeListEmplyInfoHolder holder = (CoffeeListEmplyInfoHolder) viewHolder;
        holder.title.setText(title);
        holder.message.setText(message);
        holder.button.setText(button);



    }
    //----------------------------------------------
    public static class CoffeeListEmplyInfoHolder extends RecyclerView.ViewHolder {

        public TextView title, message, button;

        public CoffeeListEmplyInfoHolder(View itemView) {
            super(itemView);

            title   = (TextView) itemView.findViewById(R.id.title);
            message = (TextView) itemView.findViewById(R.id.message);
            button  = (TextView) itemView.findViewById(R.id.button);
            button.setOnClickListener(buttonClickCallback);

            ViewGroup.LayoutParams params = itemView.getLayoutParams();
            params.height = height;
            itemView.setLayoutParams(params);
        }
    }
    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return CoffeeListEmplyInfoCell.createHolder(parent);
    }
    //----------------------------------------------
    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
        View xml = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_coffee_list_emply_info, parent,false);
        return new CoffeeListEmplyInfoCell.CoffeeListEmplyInfoHolder(xml);
    }
    //------------------------------------------------------------------------------------
    private String title, message, button;
    private static View.OnClickListener buttonClickCallback;
    private static int height;

    public CoffeeListEmplyInfoCell(String title,  String message, String button, int height, View.OnClickListener buttonClickCallback ) {
        this.title = title;
        this.message = message;
        this.button = button;
        CoffeeListEmplyInfoCell.buttonClickCallback = buttonClickCallback;
        CoffeeListEmplyInfoCell.height = height;
    }
    //------------------------------------------------------------------------------------
}