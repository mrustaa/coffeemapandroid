package mrusta.coffeeMapAndroid.ui.cell.coffeeMenu;

import mrusta.coffeeMapAndroid.R;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.RecyclerAdapterClick;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.Display;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;


public class CoffeeMenuMapSegmentCell extends RecyclerAdapterCell {
    //------------------------------------------------------------------------------------
    @Override public int type() { return 57382; }

    @Override public void fill(RecyclerView.ViewHolder viewHolder) {

        CoffeeMenuMapSegmentHolder holder = (CoffeeMenuMapSegmentHolder) viewHolder;

    }

    public static class CoffeeMenuMapSegmentHolder extends RecyclerView.ViewHolder {

        public ConstraintLayout view1;
        public ConstraintLayout segmentView;

        public CardView button1;
        public CardView button2;
        public CardView button3;

        public CoffeeMenuMapSegmentHolder(View itemView, Context context) {
            super(itemView);

            view1       = (ConstraintLayout) itemView.findViewById(R.id.view1);
            segmentView = (ConstraintLayout) itemView.findViewById(R.id.segmentView);

            button1 = (CardView) itemView.findViewById(R.id.button1);
            button2 = (CardView) itemView.findViewById(R.id.button2);
            button3 = (CardView) itemView.findViewById(R.id.button3);
            

            move(selectedMapType, context);


            button1.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {
                    buttonClick(0);
                }
            });
            button2.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {
                    buttonClick(1);
                }
            });
            button3.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {
                    buttonClick(2);
                }
            });
        }

        public void buttonClick(int index) {

            clickCallback.segmentClicked(index);

            int duration = (int) (0.35 * 1000);
            moveAnimation(index, duration);
        }

        public float getMovePositionToIndex(int index) {

            int segmentViewWidth = segmentView.getWidth();

            float movePosition = 0;
            if (index == 0) {
                movePosition = 0;
            } else if (index == 1) {
                movePosition = segmentViewWidth;
            } else if (index == 2) {
                movePosition = (segmentViewWidth * 2);
            }

            return movePosition;
        }


        public void move(int index, Context context) {

            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();

            int displayWidth = display.getWidth();
            int padding = 16;

            float density = context.getResources().getDisplayMetrics().density;

            float paddingDP = ((padding * 2) * density);
            float resultWidth = ((displayWidth - paddingDP) / 3);

            float movePosition = 0;
            if (index == 0) {
                movePosition = 0;
            } else if (index == 1) {
                movePosition = resultWidth;
            } else if (index == 2) {
                movePosition = (resultWidth * 2);
            }

            segmentView.setX(movePosition);
        }

        public void moveAnimation(int index, int duration) {

            int movePosition = (int) getMovePositionToIndex(index);

            ObjectAnimator animation = ObjectAnimator.ofFloat(segmentView, "translationX", movePosition);
            animation.setDuration(duration);
            animation.start();
        }
    }
    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return CoffeeMenuMapSegmentCell.createHolder(parent);
    }

    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
        Context context = parent.getContext();
        View xml = LayoutInflater.from(context).inflate(R.layout.cell_coffee_menu_map_segment, parent,false);
        return new CoffeeMenuMapSegmentCell.CoffeeMenuMapSegmentHolder(xml, context);
    }
    //------------------------------------------------------------------------------------
    static CoffeeMenuClick clickCallback;
    static int selectedMapType;

    public CoffeeMenuMapSegmentCell(int selectedMapType, CoffeeMenuClick clickCallback) {
        CoffeeMenuMapSegmentCell.clickCallback   = clickCallback;
        CoffeeMenuMapSegmentCell.selectedMapType = selectedMapType;
    }
    //------------------------------------------------------------------------------------
}