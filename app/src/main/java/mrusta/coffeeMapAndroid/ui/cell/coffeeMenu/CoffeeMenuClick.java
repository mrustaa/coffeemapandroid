package mrusta.coffeeMapAndroid.ui.cell.coffeeMenu;

public interface CoffeeMenuClick {

    void segmentClicked(int index);
    void socialNetworkClick(int index);
}

