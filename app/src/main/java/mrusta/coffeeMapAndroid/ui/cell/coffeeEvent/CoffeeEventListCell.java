package mrusta.coffeeMapAndroid.ui.cell.coffeeEvent;

import mrusta.coffeeMapAndroid.R;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;


public class CoffeeEventListCell extends RecyclerAdapterCell {
    //------------------------------------------------------------------------------------
    @Override public int type() { return 16519; }

    @Override public void fill(RecyclerView.ViewHolder viewHolder) {
        CoffeeEventListHolder holder = (CoffeeEventListHolder) viewHolder;
        holder.text0.setText(text0);
    holder.text1.setText(text1);
    holder.text2.setText(text2);
    holder.text3.setText(text3);
    holder.image4.setImageResource(image4);
    
    }

    public static class CoffeeEventListHolder extends RecyclerView.ViewHolder {

        public TextView text0;
        public TextView text1;
        public TextView text2;
        public TextView text3;
        public ImageView image4;

        public CoffeeEventListHolder(View itemView) {
            super(itemView);

            text0  = (TextView) itemView.findViewById(R.id.text0);
            text1  = (TextView) itemView.findViewById(R.id.text1);
            text2  = (TextView) itemView.findViewById(R.id.text2);
            text3  = (TextView) itemView.findViewById(R.id.text3);
            image4 = (ImageView) itemView.findViewById(R.id.image4);
    
        }
    }
    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return CoffeeEventListCell.createHolder(parent);
    }

    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
        View xml = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_coffee_event_list, parent,false);
        return new CoffeeEventListCell.CoffeeEventListHolder(xml);
    }
    //------------------------------------------------------------------------------------
    private String text0;
    private String text1;
    private String text2;
    private String text3;
    private int image4;
    

    public CoffeeEventListCell(String text0,
             String text1,
            String text2,
            String text3,
            int image4) {

        this.text0 = text0;
    this.text1 = text1;
    this.text2 = text2;
    this.text3 = text3;
    this.image4 = image4;
    
    }
    //------------------------------------------------------------------------------------
}