package mrusta.coffeeMapAndroid.ui.cell.coffeeMenu;

import mrusta.coffeeMapAndroid.R;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.RecyclerAdapterClick;

import android.view.Display;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.ImageView;
import android.content.Context;


public class CoffeeMenuNetworkTagCell extends RecyclerAdapterCell {
    //------------------------------------------------------------------------------------
    @Override public int type() { return 62246; }

    @Override public void fill(RecyclerView.ViewHolder viewHolder) {
        CoffeeMenuNetworkTagHolder holder = (CoffeeMenuNetworkTagHolder) viewHolder;
        holder.text.setText(text0);
        holder.image.setImageResource(image0);
    }

    public static class CoffeeMenuNetworkTagHolder extends RecyclerView.ViewHolder {
        TextView text;
        ImageView image;
        CardView iconClick;

        public CoffeeMenuNetworkTagHolder(View itemView) {
            super(itemView);

            text  = (TextView) itemView.findViewById(R.id.text0);
            image = (ImageView) itemView.findViewById(R.id.imageView);

            iconClick = (CardView) itemView.findViewById(R.id.iconClick);
            iconClick.setOnClickListener(new View.OnClickListener() {

                @Override public void onClick(View view) {
                    clickAction();
                }
            });

//            itemView.setOnClickListener(new View.OnClickListener() {
//
//                @Override public void onClick(View view) {
//                    click.recyclerViewListClicked(view, getLayoutPosition());
//                }
//            });
        }

        public void clickAction() {
            click.socialNetworkClick( getLayoutPosition() );
        }

    }
    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return CoffeeMenuNetworkTagCell.createHolder(parent);
    }

    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_coffee_menu_network_tag, parent,false);

        changeViewWidth(view, parent.getContext() );

        return new CoffeeMenuNetworkTagCell.CoffeeMenuNetworkTagHolder(view);
    }
    ///------------------------------------------------------------------------------------
    private static void changeViewWidth(View view, Context context) {

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
//        Display display = getWindowManager().getDefaultDisplay();

                                       float density = context.getResources().getDisplayMetrics().density;

                          int padding = 12;
        float paddingReal = ((padding * 2) * density);

                     int displayWidth = display.getWidth();
        float result = ((displayWidth - paddingReal) / 4);

        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                               layoutParams.width = (int)result;


        view.setLayoutParams(layoutParams);
    }
    //------------------------------------------------------------------------------------
    private String text0;
    private int image0;
    static CoffeeMenuClick click;

    public CoffeeMenuNetworkTagCell(CoffeeMenuClick click, String text, int image) {
        CoffeeMenuNetworkTagCell.click = click;
        this.text0 = text;
        this.image0 = image;
    }
    //------------------------------------------------------------------------------------
}