package mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew;

import androidx.annotation.NonNull;
import mrusta.coffeeMapAndroid.R;
import androidx.recyclerview.widget.RecyclerView;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import mrusta.coffeeMapAndroid.ui.cell.coffeeList.CoffeeMarketTagClick;

import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;
import android.content.Context;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class CoffeeDetailsNewAboutCell extends RecyclerAdapterCell {
    //------------------------------------------------------------------------------------
    @Override public int type() { return 52864; }

    @Override public void fill(RecyclerView.ViewHolder viewHolder) {
        final CoffeeDetailsNewAboutHolder holder = (CoffeeDetailsNewAboutHolder) viewHolder;

        holder.text0.setText(text0);

        if (!click) {
            holder.text0.setLines(2);

            holder.button.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {
                    click = true;

                    holder.button.setVisibility(View.GONE);
                    holder.text0.setLines(holder.text0.getLineCount());



                    // clickCallback.aboutClick();
                }
            });



        } else {
            holder.button.setVisibility(View.GONE);
            holder.text0.setLines(holder.text0.getLineCount());
        }

    }

    public static class CoffeeDetailsNewAboutHolder extends RecyclerView.ViewHolder {

        public TextView text0;
        public TextView button;
    

        public CoffeeDetailsNewAboutHolder(View itemView) {
            super(itemView);

            text0 = (TextView) itemView.findViewById(R.id.text0);
            button = (TextView) itemView.findViewById(R.id.text2);




        }
    }
    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return CoffeeDetailsNewAboutCell.createHolder(parent);
    }

    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
        View xml = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_coffee_details_new_about, parent,false);
        return new CoffeeDetailsNewAboutCell.CoffeeDetailsNewAboutHolder(xml);
    }
    //------------------------------------------------------------------------------------
    private String text0;
    boolean click;

    public CoffeeDetailsNewAboutCell(String text0) {
        this.text0 = text0;
    
    }
    //------------------------------------------------------------------------------------
}