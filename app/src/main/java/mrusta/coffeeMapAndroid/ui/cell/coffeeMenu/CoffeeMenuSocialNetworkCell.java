package mrusta.coffeeMapAndroid.ui.cell.coffeeMenu;

import mrusta.coffeeMapAndroid.R;
import androidx.recyclerview.widget.RecyclerView;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.RecyclerAdapter;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.RecyclerAdapterClick;

import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


public class CoffeeMenuSocialNetworkCell extends RecyclerAdapterCell  {
    //------------------------------------------------------------------------------------
    @Override public int type() { return 9660; }

    @Override public void fill(RecyclerView.ViewHolder viewHolder) {
        CoffeeMenuSocialNetworkHolder holder = (CoffeeMenuSocialNetworkHolder) viewHolder;
    }

    public static class CoffeeMenuSocialNetworkHolder extends RecyclerView.ViewHolder implements CoffeeMenuClick {

        RecyclerView recyclerView;
        RecyclerAdapter adapter;
        List <RecyclerAdapterCell> items = new ArrayList<>();

        public CoffeeMenuSocialNetworkHolder(View itemView) {
            super(itemView);

            recyclerView = (RecyclerView) itemView.findViewById(R.id.recycler);

            updateItems();

            adapter = new RecyclerAdapter(items);
            recyclerView.setAdapter(adapter);
        }

        void updateItems() {
            items.add( new CoffeeMenuNetworkTagCell(this,"Почта",     R.drawable._v_k__copy) );
            items.add( new CoffeeMenuNetworkTagCell(this,"Instagram", R.drawable._instagram) );
            items.add( new CoffeeMenuNetworkTagCell(this,"Facebook",  R.drawable._v_k      ) );
            items.add( new CoffeeMenuNetworkTagCell(this,"Telegram",  R.drawable._telegram ) );
        }

        @Override public void segmentClicked(int index) { }

        @Override public void socialNetworkClick(int position) {
            clickCallback.socialNetworkClick(position);
        }
    }


    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return CoffeeMenuSocialNetworkCell.createHolder(parent);
    }

    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
        View xml = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_coffee_menu_social_network, parent,false);
        return new CoffeeMenuSocialNetworkCell.CoffeeMenuSocialNetworkHolder(xml);
    }
    //------------------------------------------------------------------------------------
    static CoffeeMenuClick clickCallback;

    public CoffeeMenuSocialNetworkCell(CoffeeMenuClick clickCallback) {
        CoffeeMenuSocialNetworkCell.clickCallback = clickCallback;
    
    }
    //------------------------------------------------------------------------------------
}