package mrusta.coffeeMapAndroid.ui.cell.coffeeList;

import mrusta.coffeeMapAndroid.R;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import mrusta.coffeeMapAndroid.ui.cell.coffeeMenu.CoffeeMenuMapSegmentCell;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class CoffeeMarketTagCell extends RecyclerAdapterCell {

    //------------------------------------------------------------------------------------
    @Override public int type() { return 33974; }

    @Override public void fill(RecyclerView.ViewHolder viewHolder) {
        CoffeeMarketTagHolder holder = (CoffeeMarketTagHolder) viewHolder;
                              holder.text1.setText(text);
                              holder.text2.setText(text);

                              holder.selected = selected;
                              holder.clickAction();
    }

    public static class CoffeeMarketTagHolder extends RecyclerView.ViewHolder {

        public boolean selected;

        public TextView text1;
        public ConstraintLayout view1;

        public TextView text2;
        public ConstraintLayout view2;

        public CardView cardSelected;

        public CoffeeMarketTagHolder(View itemView) {
            super(itemView);

            text1 = (TextView) itemView.findViewById(R.id.text1);
            view1 = (ConstraintLayout) itemView.findViewById(R.id.view1);

            text2 = (TextView) itemView.findViewById(R.id.text2);
            view2 = (ConstraintLayout) itemView.findViewById(R.id.view2);
            text2.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);

            cardSelected = (CardView) itemView.findViewById(R.id.cardSelected);
            cardSelected.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {

                    click();
                }
            });
        }

        public void click() {

            selected = !selected;


            if (!selected) {
                clickCallback.selectedTagIndex(0);
            } else {
                clickCallback.selectedTagIndex(this.getLayoutPosition() + 1);
            }

            clickAction();
        }


        public void clickAction() {

            if (selected) {
                text2.setVisibility(View.VISIBLE);
                view2.setVisibility(View.VISIBLE);
            } else {
                text2.setVisibility(View.GONE);
                view2.setVisibility(View.GONE);
            }

        }
    }
    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return CoffeeMarketTagCell.createHolder(parent);
    }

    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
        View xml = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_coffee_market_tag, parent,false);
        return new CoffeeMarketTagCell.CoffeeMarketTagHolder(xml);
    }
    //------------------------------------------------------------------------------------
    static CoffeeMarketTagClick clickCallback;

    public String text;
    public boolean selected;

    public CoffeeMarketTagCell(CoffeeMarketTagClick clickCallback, String text) {
        CoffeeMarketTagCell.clickCallback = clickCallback;
        this.selected = false;
        this.text = text;
    }

    public CoffeeMarketTagCell(CoffeeMarketTagClick clickCallback, String text, boolean selected) {
        CoffeeMarketTagCell.clickCallback = clickCallback;
        this.selected = selected;
        this.text = text;
    }
    //------------------------------------------------------------------------------------
}