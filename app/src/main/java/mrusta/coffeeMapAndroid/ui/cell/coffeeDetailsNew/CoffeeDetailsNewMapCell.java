package mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew;

import mrusta.coffeeMapAndroid.R;
import androidx.recyclerview.widget.RecyclerView;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import mrusta.coffeeMapAndroid.model.CoffeeAddress;
import mrusta.coffeeMapAndroid.ui.screen.CoffeeDetailsNewActivityCallback;

import android.app.Activity;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;



public class CoffeeDetailsNewMapCell extends RecyclerAdapterCell {
    //------------------------------------------------------------------------------------
    @Override public int type() { return 98403; }

    @Override public void fill(RecyclerView.ViewHolder viewHolder) {
        CoffeeDetailsNewMapHolder holder = (CoffeeDetailsNewMapHolder) viewHolder;
        holder.bindView(address);
    }

    public static class CoffeeDetailsNewMapHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback  {

        public ImageView image0;
        public ImageView image1;
        public MapView mapView;
        public GoogleMap map;
        public View layout;

        public CoffeeDetailsNewMapHolder(View itemView) {
            super(itemView);

            layout = itemView;

            image0 = (ImageView) itemView.findViewById(R.id.image0);
            image1 = (ImageView) itemView.findViewById(R.id.image1);

            mapView = (MapView) itemView.findViewById(R.id.mapView);

            if (mapView != null) {
                // Initialise the MapView

                layout.setTag(this);
                mapView.onCreate(null);
                // Set the map ready callback to receive the GoogleMap object
                mapView.getMapAsync(this);
            }
        }

        @Override public void onMapReady(GoogleMap googleMap) {
            MapsInitializer.initialize(activity.getApplicationContext());
            map = googleMap;
            setMapLocation();
        }

        private void setMapLocation() {
            if (map == null) return;

            if (dark) {
                MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(activity, R.raw.mapstyle_dark);
                map.setMapStyle(style);
            }

            final CoffeeAddress address = (CoffeeAddress) mapView.getTag();
            if (address == null) return;

            // Add a marker for this item and set the camera
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(address.coord, 12f));
            UiSettings settings = map.getUiSettings();
            settings.setMyLocationButtonEnabled(false);
            settings.setMapToolbarEnabled(false);

            addMarkers(address);

            map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override public void onMapClick(LatLng point) {
                    mapClickCallback.detailsClickMapPin(address);
                }
            });


            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(address.coord.latitude + 0.007, address.coord.longitude), 12f));

            // Set the map type back to normal.
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }

        private void bindView(CoffeeAddress address) {

            layout.setTag(this);

            mapView.setTag(address);
            setMapLocation();
            // title.setText(item.name);
        }


        private Marker addMarkers(CoffeeAddress address) {

            int resource = R.drawable.marker_coffee_pin;
            // resource = R.drawable.marker_coffee_black_pin;

            BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(resource);

            if (this.map != null) {
                Marker marker = map.addMarker(new MarkerOptions()
                        .position(address.coord)
                        .icon(icon)
                        .infoWindowAnchor(0.5f, 0.5f));

                // marker.setTag(coffeeAddress);

                return marker;
            } else {
                return null;
            }
        }

    }

    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return CoffeeDetailsNewMapCell.createHolder(parent);
    }

    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
        View xml = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_coffee_details_new_map, parent,false);
        return new CoffeeDetailsNewMapCell.CoffeeDetailsNewMapHolder(xml);
    }
    //------------------------------------------------------------------------------------
    public CoffeeAddress address;
    public static boolean dark;
    public static Activity activity;
    public static CoffeeDetailsNewActivityCallback mapClickCallback;

    public CoffeeDetailsNewMapCell(Activity activity, CoffeeAddress address, CoffeeDetailsNewActivityCallback mapClickCallback, boolean dark) {
        CoffeeDetailsNewMapCell.activity = activity;
        CoffeeDetailsNewMapCell.mapClickCallback = mapClickCallback;
        this.address = address;
        CoffeeDetailsNewMapCell.dark = dark;
    }
    //------------------------------------------------------------------------------------
}