package mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew;

import android.view.View;

public interface CoffeeDetailsNewClick {

    void routeClick();
    void sharedInfo();
    void clickFavorite();
    void instagramClickImage();
    void instagramOpen();
}

