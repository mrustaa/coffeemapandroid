package mrusta.coffeeMapAndroid.ui.cell.coffeeList;

import mrusta.coffeeMapAndroid.R;

import mrusta.coffeeMapAndroid.managers.CoffeeDateFormater;
import mrusta.coffeeMapAndroid.managers.CoffeeUtils;
import mrusta.coffeeMapAndroid.model.CoffeeAddress;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.RecyclerAdapterClick;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class CoffeeCell extends RecyclerAdapterCell {
    //------------------------------------------------------------------------------------
    @Override public int type() { return 16; }

    @Override public void fill(RecyclerView.ViewHolder viewHolder) {
        CoffeeHolder holder = (CoffeeHolder) viewHolder;

        if (coffeeAddress != null) {
            holder.title    .setText( coffeeAddress.coffee.title );

            boolean distanceEmpty = (coffeeAddress.distance == 0.0);

            if (distanceEmpty) {
                holder.distance.setText( "" );
//                holder.distance.setVisibility(View.GONE);
            } else {
                String strDistance = CoffeeUtils.getMeterDistance(coffeeAddress.distance);
                holder.distance.setText( strDistance );
            }

            String time = CoffeeDateFormater.todayDayWeekTime(coffeeAddress, distanceEmpty);
            holder.text.setText( time );

            String strPrice = CoffeeUtils.getStrEspressoCappuccino(coffeeAddress.coffee.espresso, coffeeAddress.coffee.cappuccino);
            holder.price.setText( strPrice );

            if (coffeeAddress.new_) {
                holder.news.setVisibility(View.VISIBLE);
            } else {
                holder.news.setVisibility(View.GONE);
            }
            if (coffeeAddress.favorite) {
                holder.fav.setVisibility(View.VISIBLE);
            } else {
                holder.fav.setVisibility(View.GONE);
            }

            if (coffeeAddress.new_ && coffeeAddress.favorite) {
                holder.fav.setVisibility(View.VISIBLE);
                holder.news.setVisibility(View.VISIBLE);

                ImageView              newsView = holder.news;
                float height = (float) newsView.getLayoutParams().height;
                float x      =         newsView.getX();

                holder.fav.setX((x + height) + 65);
            }

        } else {
            holder.title    .setText( title );
            holder.distance .setText( distance );
            holder.text     .setText( text );
            holder.price    .setText( price );
        }

    }

    public static class CoffeeHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title, distance, text, price;
        public ImageView fav, news;

        public CoffeeHolder(View itemView) {
            super(itemView);
            title       = (TextView) itemView.findViewById(R.id.title    );
            distance    = (TextView) itemView.findViewById(R.id.distance );
            text        = (TextView) itemView.findViewById(R.id.text     );
            price       = (TextView) itemView.findViewById(R.id.price    );
                                     itemView.setOnClickListener(this);

            fav  = itemView.findViewById(R.id.favorite);
            //fav.setVisibility(View.GONE);

            news = itemView.findViewById(R.id.news);
            //news.setVisibility(View.GONE);
        }

        @Override public void onClick(View v) {
            click.recyclerViewListClicked(v, this.getLayoutPosition());
        }
    }
    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return                                  CoffeeCell.createHolder(parent);
    }

    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
                                      View xml = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_coffee, parent,false);
        return new CoffeeCell.CoffeeHolder(xml);
    }
    //------------------------------------------------------------------------------------
    public CoffeeAddress coffeeAddress;
    private String title, distance, text, price;
    private static RecyclerAdapterClick click;

    public CoffeeCell(CoffeeAddress coffeeAddress, RecyclerAdapterClick click) {
        this.coffeeAddress = coffeeAddress;
        CoffeeCell.click = click;
    }

    public CoffeeCell(String title, String distance, String text, String price, RecyclerAdapterClick click) {
        this.title    = title;
        this.distance = distance;
        this.text     = text;
        this.price    = price;
        CoffeeCell.click = click;
    }
    //------------------------------------------------------------------------------------
}
