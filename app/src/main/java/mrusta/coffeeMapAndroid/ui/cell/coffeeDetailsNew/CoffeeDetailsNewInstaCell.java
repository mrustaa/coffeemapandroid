package mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew;

import mrusta.coffeeMapAndroid.R;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import mrusta.coffeeMapAndroid.managers.CoffeeDataManagerNew;
import mrusta.coffeeMapAndroid.managers.TestWebViewActivity;
import mrusta.coffeeMapAndroid.managers.WebViewHtmlCode;
import mrusta.coffeeMapAndroid.managers.instaDownload.HttpManagerCompletedClose;
import mrusta.coffeeMapAndroid.managers.utils.UtilsString;
import mrusta.coffeeMapAndroid.managers.instaDownload.HttpManager;
import mrusta.coffeeMapAndroid.managers.instaDownload.HttpManagerCompleted;
import mrusta.coffeeMapAndroid.managers.utils.ViewAlphaAnimation;
import mrusta.coffeeMapAndroid.model.Coffee;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import static mrusta.coffeeMapAndroid.managers.utils.MainThread.delay;
import static mrusta.coffeeMapAndroid.managers.utils.MainThread.main;

public class CoffeeDetailsNewInstaCell extends RecyclerAdapterCell implements HttpManagerCompleted {
    static public WebView webviewcell;
    public boolean webViewOpen = false;
    public boolean webViewLogin = false;
    public boolean webViewFirstInitOnce = false;
    public WebViewHtmlCode webViewHtmlCode;
    static public ConstraintLayout instaView;

    public List<String> imageInstaURLs = new ArrayList<>();
    //------------------------------------------------------------------------------------
    @Override public int type() { return 77472; }
    //----------------------------------------------------------------------------------------------
    @Override public void fill(RecyclerView.ViewHolder viewHolder) {



        holder = (CoffeeDetailsNewInstaHolder) viewHolder;

        Log.d("\uD83D\uDD35 \uD83D\uDD37\uD83D\uDD39️", "webViewUpdata webViewUpdata");
        if (!webViewFirstInitOnce) {
            webViewFirstInitOnce = true;
            webViewUpdata(instaURLFilter);
        }

        if ((bitmap0 != null) && (bitmap1 != null) && (bitmap2 != null)) {
            holder.image0.setImageBitmap(bitmap0);
            holder.image1.setImageBitmap(bitmap1);
            holder.image2.setImageBitmap(bitmap2);
        } else if ((image0 != 0) && (image1 != 0) && (image2 != 0)) {
            holder.image0.setImageResource(image0);
            holder.image1.setImageResource(image1);
            holder.image2.setImageResource(image2);
        }
    }
    //----------------------------------------------------------------------------------------------
    @Override public void onTaskCompleted(String httpCode) {
        //Log.d("❤️❤️️❤️", this.httpManager.urlHtmlCode);

        if (httpManager.instaURLs.size() != 0) {
            imageInstaURLs = httpManager.instaURLs;
        }

        downloadImages();
    }
    //----------------------------------------------------------------------------------------------
    public void downloadImages() {

        if (imageInstaURLs.size() != 0) {

            new Thread(){
                public void run() {

                    try {
                        if (imageInstaURLs.size() >= 3) {
                            final String imageURL3 = imageInstaURLs.get(2);
                            bitmap2 = BitmapFactory.decodeStream(new URL(imageURL3).openStream());
                        }
                        if (imageInstaURLs.size() >= 2) {
                            final String imageURL2 = imageInstaURLs.get(1);
                            bitmap1 = BitmapFactory.decodeStream(new URL(imageURL2).openStream());
                        }
                        if (imageInstaURLs.size() >= 1) {
                            final String imageURL1 = imageInstaURLs.get(0);
                            bitmap0 = BitmapFactory.decodeStream(new URL(imageURL1).openStream());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {

                            if ((holder.image0 == null) || (holder.image1 == null) || (holder.image2 == null)) {
                                return;
                            }

                            ImageView image0 = holder.image0;
                            ImageView image1 = holder.image1;
                            ImageView image2 = holder.image2;

                            image0.setVisibility(View.VISIBLE);
                            image1.setVisibility(View.VISIBLE);
                            image2.setVisibility(View.VISIBLE);

                            int duration = 500;

                            if (bitmap0 != null) {
                                image0.setImageBitmap(bitmap0);
                                image0.animate().alpha(1).setDuration(duration);
                            }
                            if (bitmap1 != null) {
                                image1.setImageBitmap(bitmap1);
                                image1.animate().alpha(1).setDuration(duration);
                            }
                            if (bitmap2 != null) {
                                image2.setImageBitmap(bitmap2);
                                image2.animate().alpha(1).setDuration(duration);
                            }
                        }
                    });
                }
            }.start();
        } else {

            if (!webViewLogin) {

                if (!webViewOpen) {

                    webViewOpen = true;


                } else {

                    Log.d("⛔️\uD83D\uDEAB❌", "webView АВТОРИЗОВАТЬСЯ");

                    CoffeeDataManagerNew.webViewInstaURL = instaURL;
//            CoffeeDataManagerNew.webViewHtmlcodeCallback = (new WebViewHtmlCode.Script.HtmlCodeCallback() {
//                @Override
//                public void htmlCodeCallback(String html) {
//                    Log.d("✅✅✅ ", "insURL");
//                    httpManager.setWebViewHtmlCode(html);
//                }
//            });

//
                    main(new Runnable() { @Override public void run() {
                        ViewAlphaAnimation.change(CoffeeDetailsNewInstaCell.instaView, true, 500);
                    }});
                }

            }
        }
    }
    //----------------------------------------------------------------------------------------------
    public static class CoffeeDetailsNewInstaHolder extends RecyclerView.ViewHolder {
        public ImageView image0;
        public ImageView image1;
        public ImageView image2;
        public ConstraintLayout layout0;
        public ConstraintLayout layout1;
        public ConstraintLayout layout2;

        public ConstraintLayout instaButton;

        public CoffeeDetailsNewInstaHolder(View itemView) {
            super(itemView);
            image0 = (ImageView) itemView.findViewById(R.id.image0);
            image1 = (ImageView) itemView.findViewById(R.id.image1);
            image2 = (ImageView) itemView.findViewById(R.id.image2);

            CoffeeDetailsNewInstaCell.instaView = (ConstraintLayout) itemView.findViewById(R.id.instaView);

            CoffeeDetailsNewInstaCell.instaView.setVisibility(View.GONE);

            instaButton = (ConstraintLayout) itemView.findViewById(R.id.instaButton);
            instaButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(activity, TestWebViewActivity.class);
                    activity.startActivity(intent);
                }
            });

            CoffeeDetailsNewInstaCell.webviewcell = (WebView)  itemView.findViewById(R.id.webView);

            image0.setVisibility(View.GONE);
            image1.setVisibility(View.GONE);
            image2.setVisibility(View.GONE);
            //webview.setVisibility(View.GONE);

            layout0 = (ConstraintLayout) itemView.findViewById(R.id.layout0);
            layout1 = (ConstraintLayout) itemView.findViewById(R.id.layout1);
            layout2 = (ConstraintLayout) itemView.findViewById(R.id.layout2);

        }
    }
    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return CoffeeDetailsNewInstaCell.createHolder(parent);
    }
    //----------------------------------------------------------------------------------------------
    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
        View xml = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_coffee_details_new_insta, parent,false);
        CoffeeDetailsNewInstaHolder holder = new CoffeeDetailsNewInstaCell.CoffeeDetailsNewInstaHolder(xml);

        return holder;
    }
    //------------------------------------------------------------------------------------
    private String instaURL;
    private String instaURLFilter;
    private HttpManager httpManager;

    private CoffeeDetailsNewInstaHolder holder;

    private Bitmap bitmap0;
    private Bitmap bitmap1;
    private Bitmap bitmap2;
    private int image0;
    private int image1;
    private int image2;
    static CoffeeDetailsNewClick detailsCallback;
    public static Activity activity;
    //----------------------------------------------------------------------------------------------
    public CoffeeDetailsNewInstaCell(Activity activity, CoffeeDetailsNewClick detailsCallback, Coffee coffee) {
        CoffeeDetailsNewInstaCell.detailsCallback = detailsCallback;
        CoffeeDetailsNewInstaCell.activity = activity;

        final String instaURL = coffee.insta;

        String insURL = instaURL.replaceAll("\\s+", "");
        this.instaURL = insURL;

        if (!UtilsString.findText( insURL,"https://www.instagram.com")) {
            if (!UtilsString.findText( insURL,"https://instagram.com/")) {
                String name = "";
                if (UtilsString.findText( insURL,"www.instagram.com/")) {
                    name = UtilsString.removingText(insURL,"www.instagram.com/");
                } else {
                    name = UtilsString.removingText(insURL,"instagram.com/");
                }
                insURL = String.format("https://www.instagram.com/%s", name);
                coffee.insta = insURL;
            }
        }


        if (instaURL.isEmpty()) return;
        if (!UtilsString.findText(instaURL, "instagram")) return;

        instaURLFilter = insURL;

        createHttpManager(instaURL);


        CoffeeDataManagerNew.testWebViewImagesURLsCallback = new TestWebViewActivity.ImagesUrlCallback() {
            @Override
            public void complection(List<String> urls) {
                imageInstaURLs =  urls;
                downloadImages();
            }
        };

        CoffeeDataManagerNew.webViewCloseCallback = new HttpManagerCompletedClose() {
            @Override public void feedbackClose() {
                webViewLogin = true;
                ViewAlphaAnimation.change(CoffeeDetailsNewInstaCell.instaView, false, 500);
                Log.d("⬜️ \uD83D\uDFE7\uD83D\uDFE5\uD83D\uDFE9\uD83D\uDFE6\uD83D\uDFEA\uD83D\uDFE8⬛️ ", "WebView Close htmlCodeURL insURL");
                Log.d("⬜️ \uD83D\uDFE7\uD83D\uDFE5\uD83D\uDFE9\uD83D\uDFE6\uD83D\uDFEA\uD83D\uDFE8⬛️ ", instaURL);
//                httpManager.htmlCodeURL(instaURL);

//                CoffeeDetailsNewInstaCell.webviewcell.loadUrl(instaURL);

//                    delay(1600, new Runnable() { @Override public void run() {
//                            ViewAlphaAnimation.change(CoffeeDetailsNewInstaCell.instaView, false, 500);

//                        webViewUpdata(instaURL);
//                        createHttpManager(instaURL);
//                        webViewUpdate();
//                    }});
            }

//            @Override public void feedbackImgURLs(List<String> instaURLs) {
//                Log.d("◽️◾️▫️▪️\uD83D\uDD32\uD83D\uDD33◻️◼️◽️◾️▫️▪️⬜️⬛️", "feedbackImgURLs");
//                if (instaURLs.size() != 0) {
//                    imageInstaURLs = instaURLs;
//                    downloadImages();
//                }
//            }
        };
    }

    public void webViewUpdata(String url) {

        final String insURL = url.replaceAll("https://", "");

        Log.d("\uD83D\uDC7A\uD83D\uDC7A ", insURL);

        if (webViewHtmlCode != null) {
            webViewHtmlCode = null;
        }

        webViewHtmlCode = new WebViewHtmlCode(webviewcell, instaURLFilter, new WebViewHtmlCode.Script.HtmlCodeCallback() {
            @Override
            public void htmlCodeCallback(String html) {
                Log.d("✅✅✅ ", String.format("insURL:%s", insURL));
                httpManager.setWebViewHtmlCode(html);
            }
        });


    }

    public void createHttpManager(String url) {

            httpManager = new HttpManager(this);
           // httpManager.htmlCodeURL( url );
        }
            //----------------------------------------------------------------------------------------------
    public CoffeeDetailsNewInstaCell(Bitmap bitmap0, Bitmap bitmap1, Bitmap bitmap2) {
        this.bitmap0 = bitmap0;
        this.bitmap1 = bitmap1;
        this.bitmap2 = bitmap2;
    }
    //----------------------------------------------------------------------------------------------
    public CoffeeDetailsNewInstaCell(int image0, int image1, int image2) {
        this.image0 = image0;
        this.image1 = image1;
        this.image2 = image2;
    }
    //------------------------------------------------------------------------------------




}