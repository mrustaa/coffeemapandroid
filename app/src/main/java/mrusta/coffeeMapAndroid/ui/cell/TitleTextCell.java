package mrusta.coffeeMapAndroid.ui.cell;

import androidx.annotation.NonNull;
import mrusta.coffeeMapAndroid.R;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.RecyclerAdapterClick;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;
import android.content.Context;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class TitleTextCell extends RecyclerAdapterCell {
    //------------------------------------------------------------------------------------
    @Override public int type() { return 99873; }

    @Override public void fill(RecyclerView.ViewHolder viewHolder) {
        TitleTextHolder holder = (TitleTextHolder) viewHolder;
                        holder.text.setText(text0);
        holder.separatorView.setVisibility( separator ? View.GONE : View.VISIBLE);
    }

    public static class TitleTextHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView text;
        public View separatorView;

        public TitleTextHolder(View itemView) {
            super(itemView);
            text =      (TextView) itemView.findViewById(R.id.title);
            separatorView = (View) itemView.findViewById(R.id.separator);
                                   itemView.setOnClickListener(this);
        }

        @Override public void onClick(View v) {
            click.recyclerViewListClicked(v, this.getLayoutPosition());
        }
    }
    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return TitleTextCell.createHolder(parent);
    }

    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
                                            View xml = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_title_text, parent,false);
        return new TitleTextCell.TitleTextHolder(xml);
    }
    //------------------------------------------------------------------------------------
    String text0;
    boolean separator;
    static RecyclerAdapterClick click;

    public TitleTextCell(RecyclerAdapterClick click, String text0, boolean hideSeparator) {
        this.text0 = text0;
        this.separator = hideSeparator;
        TitleTextCell.click = click;
    }

    public TitleTextCell(RecyclerAdapterClick click, String text0) {
        this.text0 = text0;
        TitleTextCell.click = click;
    }
    //------------------------------------------------------------------------------------
}