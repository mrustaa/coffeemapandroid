package mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew;

import androidx.annotation.NonNull;
import mrusta.coffeeMapAndroid.R;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import mrusta.coffeeMapAndroid.model.CoffeeAddress;

import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;
import android.content.Context;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CoffeeDetailsNewRouteCell extends RecyclerAdapterCell {
    //------------------------------------------------------------------------------------
    @Override public int type() { return 63648; }
    //------------------------------------------------------------------------------------
    @Override public void fill(RecyclerView.ViewHolder viewHolder) {
        CoffeeDetailsNewRouteHolder holder = (CoffeeDetailsNewRouteHolder) viewHolder;
        
    }
    //------------------------------------------------------------------------------------
    public static class CoffeeDetailsNewRouteHolder extends RecyclerView.ViewHolder {

        public TextView text2;
        public CardView buttonRoute;
        public CardView favorite;
        public CardView sharedInfo;

        public ConstraintLayout favoriteBorder;
        public ImageView        favoriteImage;

        public CoffeeDetailsNewRouteHolder(View itemView) {
            super(itemView);
            text2 = (TextView) itemView.findViewById(R.id.text2);

            buttonRoute = (CardView) itemView.findViewById(R.id.buttonRoute);
            favorite    = (CardView) itemView.findViewById(R.id.favorite);
            sharedInfo  = (CardView) itemView.findViewById(R.id.sharedInfo);

            favoriteBorder = (ConstraintLayout) itemView.findViewById(R.id.layer1border);
            favoriteImage  = (ImageView)        itemView.findViewById(R.id.image4);
            favoriteLayer();

            favorite.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    address.favorite = !address.favorite;
                    favoriteLayer();

                    detailsCallback.clickFavorite();
                }
            });

            sharedInfo.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    detailsCallback.sharedInfo();
                }
            });

            buttonRoute.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    detailsCallback.routeClick();
                }
            });
        }

        void favoriteLayer() {

            if (address.favorite) {
                favoriteBorder.setBackgroundResource(R.drawable.border_green);
                favoriteImage.setColorFilter(ContextCompat.getColor((Context) detailsCallback, R.color.coffeeGreen));

                //favoriteImage.setColorFilter(ContextCompat.getColor((Context) detailsCallback, R.color.coffeeGreen), android.graphics.PorterDuff.Mode.MULTIPLY);
            } else {
                favoriteBorder.setBackgroundResource(R.drawable.border);
                favoriteImage.setColorFilter(ContextCompat.getColor((Context) detailsCallback, (darkMode ? R.color.white : R.color.black) ));
                //favoriteImage.setColorFilter(ContextCompat.getColor((Context) detailsCallback, R.color.black), android.graphics.PorterDuff.Mode.MULTIPLY);
            }
        }
    }
    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return CoffeeDetailsNewRouteCell.createHolder(parent);
    }
    //------------------------------------------------------------------------------------
    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
        View xml = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_coffee_details_new_route, parent,false);
        return new CoffeeDetailsNewRouteCell.CoffeeDetailsNewRouteHolder(xml);
    }
    //------------------------------------------------------------------------------------
    String text2;
    static boolean darkMode;
    static CoffeeAddress address;
    static CoffeeDetailsNewClick detailsCallback;

    public CoffeeDetailsNewRouteCell(CoffeeDetailsNewClick detailsCallback, CoffeeAddress address, boolean darkMode, String text2) {
        CoffeeDetailsNewRouteCell.detailsCallback = detailsCallback;
        CoffeeDetailsNewRouteCell.address = address;
        CoffeeDetailsNewRouteCell.darkMode = darkMode;
        this.text2 = text2;
    }
    //------------------------------------------------------------------------------------
}