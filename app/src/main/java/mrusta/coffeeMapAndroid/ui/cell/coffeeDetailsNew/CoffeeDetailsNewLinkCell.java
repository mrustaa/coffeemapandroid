package mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew;

import androidx.annotation.NonNull;
import mrusta.coffeeMapAndroid.R;
import androidx.recyclerview.widget.RecyclerView;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;
import android.content.Context;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class CoffeeDetailsNewLinkCell extends RecyclerAdapterCell {
    //------------------------------------------------------------------------------------
    @Override public int type() { return 66071; }

    @Override public void fill(RecyclerView.ViewHolder viewHolder) {
        CoffeeDetailsNewLinkHolder holder = (CoffeeDetailsNewLinkHolder) viewHolder;
        holder.text1.setText(text1);
        
    }

    public static class CoffeeDetailsNewLinkHolder extends RecyclerView.ViewHolder {

         public TextView text1;

        public CoffeeDetailsNewLinkHolder(View itemView) {
            super(itemView);

            text1 = (TextView) itemView.findViewById(R.id.text1);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    detailsCallback.instagramOpen();
                }
            });
        }
    }
    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return CoffeeDetailsNewLinkCell.createHolder(parent);
    }

    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
        View xml = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_coffee_details_new_link, parent,false);
        return new CoffeeDetailsNewLinkCell.CoffeeDetailsNewLinkHolder(xml);
    }
    //------------------------------------------------------------------------------------

    private String text1;
    static CoffeeDetailsNewClick detailsCallback;

    public CoffeeDetailsNewLinkCell(CoffeeDetailsNewClick detailsCallback, String text1) {
        CoffeeDetailsNewLinkCell.detailsCallback = detailsCallback;
        this.text1 = text1;
    }
    //------------------------------------------------------------------------------------
}