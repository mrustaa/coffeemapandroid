package mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew;

import androidx.annotation.NonNull;
import mrusta.coffeeMapAndroid.R;
import androidx.recyclerview.widget.RecyclerView;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;
import android.content.Context;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class CoffeeDetailsNewSeparatorCell extends RecyclerAdapterCell {
    //------------------------------------------------------------------------------------
    @Override public int type() { return 50728; }

    @Override public void fill(RecyclerView.ViewHolder viewHolder) {
        CoffeeDetailsNewSeparatorHolder holder = (CoffeeDetailsNewSeparatorHolder) viewHolder;
        
    }

    public static class CoffeeDetailsNewSeparatorHolder extends RecyclerView.ViewHolder {

        

        public CoffeeDetailsNewSeparatorHolder(View itemView) {
            super(itemView);

            
        }
    }
    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return CoffeeDetailsNewSeparatorCell.createHolder(parent);
    }

    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
        View xml = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_coffee_details_new_separator, parent,false);
        return new CoffeeDetailsNewSeparatorCell.CoffeeDetailsNewSeparatorHolder(xml);
    }
    //------------------------------------------------------------------------------------
    

    public CoffeeDetailsNewSeparatorCell() {

        
    }
    //------------------------------------------------------------------------------------
}