package mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew;

import androidx.annotation.NonNull;
import mrusta.coffeeMapAndroid.R;
import androidx.recyclerview.widget.RecyclerView;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import mrusta.coffeeMapAndroid.managers.CoffeeDateFormater;
import mrusta.coffeeMapAndroid.managers.CoffeeUtils;
import mrusta.coffeeMapAndroid.model.CoffeeAddress;

import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;
import android.content.Context;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class CoffeeDetailsNewNameCell extends RecyclerAdapterCell {
    //------------------------------------------------------------------------------------
    @Override public int type() { return 84657; }

    @Override public void fill(RecyclerView.ViewHolder viewHolder) {
        CoffeeDetailsNewNameHolder holder = (CoffeeDetailsNewNameHolder) viewHolder;

        if (address != null) {
            holder.title    .setText( address.coffee.title );

            String strDistance = CoffeeUtils.getMeterDistance(address.distance);
            holder.distance.setText( strDistance );

            String time = CoffeeDateFormater.todayDayWeekTime(address);
            holder.time.setText( time );
        } else {
            holder.title    .setText( title );
            holder.distance .setText( distance );
            holder.time     .setText( time );
        }

        if (address.new_) {
            holder.news.setVisibility(View.VISIBLE);
        } else {
            holder.news.setVisibility(View.GONE);
        }

    }

    public static class CoffeeDetailsNewNameHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView distance;
        public TextView time;
        public ImageView image2;
        public ImageView news;
    

        public CoffeeDetailsNewNameHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.title);
            distance = (TextView) itemView.findViewById(R.id.distance);
            time = (TextView) itemView.findViewById(R.id.time);
            image2 = (ImageView) itemView.findViewById(R.id.image2);

            news = itemView.findViewById(R.id.news);
            news.setVisibility(View.GONE);
        }
    }
    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return CoffeeDetailsNewNameCell.createHolder(parent);
    }

    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
        View xml = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_coffee_details_new_name, parent,false);
        return new CoffeeDetailsNewNameCell.CoffeeDetailsNewNameHolder(xml);
    }
    //------------------------------------------------------------------------------------
    private String title, distance, time;
    CoffeeAddress address;

    public CoffeeDetailsNewNameCell(CoffeeAddress address)  {
        this.address = address;
    }

    public CoffeeDetailsNewNameCell(String title, String distance, String time)  {
        this.title = title;
        this.distance = distance;
        this.time = time;
    }
    //------------------------------------------------------------------------------------
}