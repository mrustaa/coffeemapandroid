package mrusta.coffeeMapAndroid.ui.cell.coffeeMenu;

import mrusta.coffeeMapAndroid.R;
import androidx.recyclerview.widget.RecyclerView;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class CoffeeMenuSocialTitleCell extends RecyclerAdapterCell {
    //------------------------------------------------------------------------------------
    @Override public int type() { return 39096; }

    @Override public void fill(RecyclerView.ViewHolder viewHolder) {
        CoffeeMenuSocialTitleHolder holder = (CoffeeMenuSocialTitleHolder) viewHolder;
                holder.text0.setText(text0);
        
    }

    public static class CoffeeMenuSocialTitleHolder extends RecyclerView.ViewHolder {

        public TextView text0;

        public CoffeeMenuSocialTitleHolder(View itemView) {
            super(itemView);

            text0 = (TextView) itemView.findViewById(R.id.text0);
    
        }
    }
    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return CoffeeMenuSocialTitleCell.createHolder(parent);
    }

    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
        View xml = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_coffee_menu_social_title, parent,false);
        return new CoffeeMenuSocialTitleCell.CoffeeMenuSocialTitleHolder(xml);
    }
    //------------------------------------------------------------------------------------
    private String text0;
    

    public CoffeeMenuSocialTitleCell(String text0) {

        this.text0 = text0;
    
    }
    //------------------------------------------------------------------------------------
}