package mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew;

import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;

import androidx.annotation.NonNull;
import mrusta.coffeeMapAndroid.R;
import mrusta.coffeeMapAndroid.model.Coffee;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;
import android.content.Context;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class CoffeeDetailsNewInfoCell extends RecyclerAdapterCell {
    //------------------------------------------------------------------------------------
    @Override public int type() { return 34148; }

    @Override public void fill(RecyclerView.ViewHolder viewHolder) {
        CoffeeDetailsNewInfoHolder holder = (CoffeeDetailsNewInfoHolder) viewHolder;
        holder.text0.setText(text0);
        holder.text1.setText(text1);
        holder.text2.setText(text2);
        holder.text3.setText(text3);
        holder.text4.setText(text4);
        
    }

    public static class CoffeeDetailsNewInfoHolder extends RecyclerView.ViewHolder {

        public TextView text0;
        public TextView text1;
        public TextView text2;
        public TextView text3;
        public TextView text4;
    

        public CoffeeDetailsNewInfoHolder(View itemView) {
            super(itemView);

            text0 = (TextView) itemView.findViewById(R.id.text0);
            text1 = (TextView) itemView.findViewById(R.id.text1);
            text2 = (TextView) itemView.findViewById(R.id.text2);
            text3 = (TextView) itemView.findViewById(R.id.text3);
            text4 = (TextView) itemView.findViewById(R.id.text4);
    
        }
    }
    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return CoffeeDetailsNewInfoCell.createHolder(parent);
    }

    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
        View xml = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_coffee_details_new_info, parent,false);
        return new CoffeeDetailsNewInfoCell.CoffeeDetailsNewInfoHolder(xml);
    }
    //------------------------------------------------------------------------------------
    private String text0;
    private String text1;
    private String text2;
    private String text3;
    private String text4;



    public CoffeeDetailsNewInfoCell(Coffee coffee) {
        this.text0 = "Цены";
        this.text1 = "Эспрессо";
        this.text2 = "Капучино";

        String strCap = coffee.strCappuccino + " ₽";
        String strEsp = coffee.strEspresso   + " ₽";

        this.text3 = strEsp;
        this.text4 = strCap;
    }

    public CoffeeDetailsNewInfoCell(String text0,
    String text1,
    String text2,
    String text3,
    String text4
    ) {

        this.text0 = text0;
    this.text1 = text1;
    this.text2 = text2;
    this.text3 = text3;
    this.text4 = text4;
    
    }
    //------------------------------------------------------------------------------------
}