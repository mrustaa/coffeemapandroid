package mrusta.coffeeMapAndroid.ui.cell.coffeeMenu;

import mrusta.coffeeMapAndroid.R;
import androidx.recyclerview.widget.RecyclerView;

import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.RecyclerAdapterClick;
import mrusta.coffeeMapAndroid.ui.cell.TitleTextCell;

import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class CoffeeMenuAboutCell extends RecyclerAdapterCell {
    //------------------------------------------------------------------------------------
    @Override public int type() { return 56126; }

    @Override public void fill(RecyclerView.ViewHolder viewHolder) {
        CoffeeMenuAboutHolder holder = (CoffeeMenuAboutHolder) viewHolder;
                holder.text0.setText(text0);
        holder.text1.setText(text1);
    }

    public static class CoffeeMenuAboutHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {
        public TextView text0;
        public TextView text1;

        public CoffeeMenuAboutHolder(View itemView) {
            super(itemView);

            text0 = (TextView) itemView.findViewById(R.id.text0);
            text1 = (TextView) itemView.findViewById(R.id.text1);
            itemView.setOnClickListener(this);
        }

        @Override public void onClick(View v) {
            click.recyclerViewListClicked(v, this.getLayoutPosition());
        }
    }
    //------------------------------------------------------------------------------------
    @Override public RecyclerView.ViewHolder createHolderItem(ViewGroup parent) {
        return CoffeeMenuAboutCell.createHolder(parent);
    }

    public static RecyclerView.ViewHolder createHolder(ViewGroup parent) {
        View xml = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_coffee_menu_about, parent,false);
        return new CoffeeMenuAboutCell.CoffeeMenuAboutHolder(xml);
    }
    //------------------------------------------------------------------------------------
    private String text0;
    private String text1;
    static RecyclerAdapterClick click;

    public CoffeeMenuAboutCell(RecyclerAdapterClick click, String text0, String text1) {

        this.text0 = text0;
    this.text1 = text1;
        CoffeeMenuAboutCell.click = click;
    
    }
    //------------------------------------------------------------------------------------
}