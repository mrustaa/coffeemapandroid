package mrusta.coffeeMapAndroid.ui.screen;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import mrusta.coffeeMapAndroid.R;

import androidx.recyclerview.widget.RecyclerView;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.RecyclerAdapter;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import mrusta.coffeeMapAndroid.ui.cell.coffeeEvent.CoffeeEventListCell;

import java.util.ArrayList;
import java.util.List;


public class CoffeeEventActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerAdapter adapter;
    List <RecyclerAdapterCell> items = new ArrayList<>();

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coffee_event);

        updateItems();
        updateRecyclerView();
    }

    //----------------------------------------------------------------------------------------
    private void updateRecyclerView() {

        recyclerView = (RecyclerView) findViewById(R.id.recycler);

//        DividerItemDecoration separator = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
//        recyclerView.addItemDecoration(separator);

        adapter = new RecyclerAdapter(items);
        recyclerView.setAdapter(adapter);
    }
    //----------------------------------------------------------------------------------------
    private void updateItems() {
        items.add(new CoffeeEventListCell("Каппинг от скандинавских обжарщиков",         "Cosmic Latte  · Суббота в 11:00",     "СЕН", "29", R.drawable.app_launch_new));
        items.add(new CoffeeEventListCell("Совместный обед со Мхом\n и Лёшей Морозовым", "Кооператив Чёрный · Суббота в 12:00", "СЕН", "1",  R.drawable.app_launch_new));
        items.add(new CoffeeEventListCell("AFAAAAA!!!!!!!!!!!!!!\uD83E\uDD84\uD83E\uDDA0\uD83D\uDD25\uD83E\uDDE9","dadwadw","АВГ", "15", R.drawable.app_launch_new));
        items.add(new CoffeeEventListCell("Каппинг от скандинавских обжарщиков",         "Cosmic Latte  · Суббота в 11:00",     "СЕН", "29", R.drawable.app_launch_new));
        items.add(new CoffeeEventListCell("Совместный обед со Мхом\n и Лёшей Морозовым", "Кооператив Чёрный · Суббота в 12:00", "СЕН", "1",  R.drawable.app_launch_new));
        items.add(new CoffeeEventListCell("dwadaw",                                      "dadwadw",                             "АВГ", "15", R.drawable.app_launch_new));
    }

}