package mrusta.coffeeMapAndroid.ui.screen.list;


import mrusta.coffeeMapAndroid.R;

import mrusta.coffeeMapAndroid.managers.CoffeeDataLocal;
import mrusta.coffeeMapAndroid.managers.CoffeeDataLocalArray;
import mrusta.coffeeMapAndroid.managers.CoffeeReviewManager;
import mrusta.coffeeMapAndroid.managers.CoffeeUrlScheme;
import mrusta.coffeeMapAndroid.managers.mapLocationManager.PermissionUtils;
import mrusta.coffeeMapAndroid.managers.utils.DarkMode;
import mrusta.coffeeMapAndroid.managers.mapLocationManager.CoffeeAccessLocationManager;
import mrusta.coffeeMapAndroid.managers.mapManager.CoffeeMapManager;
import mrusta.coffeeMapAndroid.managers.mapManager.CoffeeMapManagerCallback;
import mrusta.coffeeMapAndroid.managers.utils.HideStatusBar;
import mrusta.coffeeMapAndroid.managers.utils.NetworkConnected;
import mrusta.coffeeMapAndroid.managers.utils.ScreenSize;
import mrusta.coffeeMapAndroid.managers.utils.UtilsAlert;
import mrusta.coffeeMapAndroid.managers.utils.UtilsString;
import mrusta.coffeeMapAndroid.managers.utils.ViewAlphaAnimation;
import mrusta.coffeeMapAndroid.model.CoffeeAddress;
import mrusta.coffeeMapAndroid.model.CoffeeIndex;

import mrusta.coffeeMapAndroid.managers.CoffeeDataManagerNew;

import mrusta.coffeeMapAndroid.ui.alert.CustomAlert;
import mrusta.coffeeMapAndroid.ui.alert.CustomAlertActivity;
import mrusta.coffeeMapAndroid.ui.cell.CoffeeListEmplyInfoCell;
import mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew.CoffeeDetailsNewClick;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.RecyclerAdapter;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.RecyclerAdapterClick;
import mrusta.coffeeMapAndroid.ui.cell.coffeeList.CoffeeMarketTagCell;
import mrusta.coffeeMapAndroid.ui.cell.coffeeList.CoffeeMarketTagClick;
import mrusta.coffeeMapAndroid.ui.cell.coffeeMenu.CoffeeMenuClick;
import mrusta.coffeeMapAndroid.ui.launch.LaunchActivity;
import mrusta.coffeeMapAndroid.ui.modal.CoffeeMenuActivity;
import mrusta.coffeeMapAndroid.ui.modal.CoffeeMenuActivityCallback;
import mrusta.coffeeMapAndroid.ui.screen.CoffeeDetailsNewActivity;
import mrusta.coffeeMapAndroid.ui.screen.CoffeeDetailsNewActivityCallback;
//import mrusta.coffeeMapAndroid.ui.screen.OnScreenLog;
import mrusta.coffeeMapAndroid.ui.view.CoffeeSearchBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;

import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import static com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState.ANCHORED;
import static com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState.DRAGGING;
import static com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState.HIDDEN;
import static com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState.EXPANDED;
import static com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState.COLLAPSED;

import static mrusta.coffeeMapAndroid.managers.CoffeeDataLocal.DataType.FILTER;
import static mrusta.coffeeMapAndroid.managers.CoffeeDataLocal.DataType.FIRST_START_APP;
import static mrusta.coffeeMapAndroid.managers.CoffeeDataLocalArray.Type.FAVORITE;
import static mrusta.coffeeMapAndroid.managers.CoffeeDataLocalArray.Type.HISTORY;
import static mrusta.coffeeMapAndroid.managers.mapManager.CoffeeMapManager.kazan;
import static mrusta.coffeeMapAndroid.managers.mapManager.CoffeeMapManager.moscow;
import static mrusta.coffeeMapAndroid.managers.mapManager.CoffeeMapManager.spb;
import static mrusta.coffeeMapAndroid.managers.utils.MainThread.delay;
import static mrusta.coffeeMapAndroid.managers.utils.MainThread.main;
import static mrusta.coffeeMapAndroid.managers.mapLocationManager.CoffeeAccessLocationManager.accessLocationEnabled;

import static mrusta.coffeeMapAndroid.ui.screen.list.CoffeeListActivity.OpenDetailsType.Empty;
import static mrusta.coffeeMapAndroid.ui.screen.list.CoffeeListActivity.OpenDetailsType.Map;
import static mrusta.coffeeMapAndroid.ui.screen.list.CoffeeListActivity.OpenDetailsType.List;

//--------------------------------------------------------------------------------------------------
public class CoffeeListActivity extends AppCompatActivity implements
        RecyclerAdapterClick,
        CoffeeMenuClick,
        CoffeeMarketTagClick,
        CoffeeDetailsNewActivityCallback,
        CoffeeDataManagerNew.DownloadFirebaseCallback,
        CoffeeDataManagerNew.FilteredCallback,
        CoffeeDataManagerNew.DetailsActivityBackCallback,

        CoffeeDetailsNewClick {

    //----------------------------------------------------------------------------------------------
    public interface UpdateItemsCallback {
        void updatedItemsResult(List<RecyclerAdapterCell> items);
    }
    //----------------------------------------------------------------------------------------------

    // LIST COFFEE ADDRESS
    List<CoffeeAddress> filterCoffeeAddresses;

    // TAG FILTER
    static int lastSelectedFilter = 0;
    static int             filter = 0;

    CoffeeAddress addressDetails;

    RecyclerView              tagView;
    RecyclerAdapter           tagAdapter;
    TextView tv;
    List<RecyclerAdapterCell> tagItems = new ArrayList<>();

    SlidingUpPanelLayout        slidingPanelLayout;

    // LIST
    RecyclerView                recyclerView;
    SwipeRefreshLayout          recyclerViewRefresh;
    RecyclerAdapter             recyclerAdapter;
    static int                  recyclerLastScrollPosition;
    List <RecyclerAdapterCell>  recyclerDetailsItems;

            ConstraintLayout firstLoaderView;

    CoffeeMapManager mapManager;
    MapView          mapView;

    ConstraintLayout      containerItems;
    ConstraintLayout      containerMap;
    ConstraintLayout        navBar;
    ImageView           buttonClose;
    ConstraintLayout    buttonCloseLayout;


    public enum OpenDetailsType {
        Empty,
        Map,
        List,
    }


    final int               containereMoveAnimationSpeed = 380;
    final int               containereDelay = 50;
    boolean                 containereListLastPositionTop = false;
    boolean                 containereMapOpen = false;

    boolean                 openDetails = false;
    boolean                 openDetails2 = false;
    OpenDetailsType         openDetailsType = Empty;
    boolean                 openDetailsRecyclerUpdate = false;
    // BUTTONS
//    CardView                 menuButton;
//    FloatingActionButton  listMapButton;
    FloatingActionButton locationButton;
    FloatingActionButton   searchMapButton;
    FloatingActionButton     menuMapButton;

    CoffeeSearchBar      searchView;

    TextView                titleLabel;

    boolean               cityButtonShow;
    ImageView           changeCityButton;
    ConstraintLayout      changeCityView;

    // MAP BANNER
//    CardView             mapAlertView;
//    CoffeeMapBannerView  mapAlertBannerView;

    // LOADER
    ConstraintLayout    loaderScreen;
    Timer               loaderScreenTimer;
    ProgressBar      mapSpinner;
    ProgressBar     listSpinner;

    boolean openLastAddressFavorite;

    static boolean menuOpen;

    static boolean darkMode;

    static boolean updateProcess;
    static boolean updateFilter;        // если пользователь бесконечно тыкает фильты, то это сохранение последнего его нажатия)
    //static boolean updateFilterFirst;
    static boolean pauseSelectTag;


    /* Life Cycle                                                                                 */

    @Override protected void onCreate(Bundle savedInstanceState) {                                  Log.d("\uD83D\uDEA8\uD83D\uDEA8\uD83D\uDEA8️ ", "CoffeeListActivity | onCreate");

        HideStatusBar.hideStatusBar(this);

        darkMode = DarkMode.updateSetTheme(this);


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coffee_list);

        loadAllViewsById(savedInstanceState);

        CoffeeDataManagerNew.setContext(this);
        CoffeeDataManagerNew.setDownloadFirebaseCallback(this);
        CoffeeDataManagerNew.setFilteredCallback(this);
        CoffeeDataManagerNew.setDetailsActivityBackCallback(this);



        //filter = CoffeeDataLocal.loadDataKeyInt(FILTER);
        filterCoffeeAddresses = CoffeeDataManagerNew.filterCoffeesCopy();


        if (!CoffeeDataManagerNew.endDownloadFirebaseFirst) { // Если обновленние с Firebase - еще не закончилось
            loaderScreenAnimation(true);
        }


//        if (filterCoffeeAddresses.size() == 0) {
//            CoffeeDataManagerNew.loadCoffee();
//        }

        int firstStartApp = CoffeeDataLocal.loadDataKeyInt(FIRST_START_APP);
        if (firstStartApp >= 3) {
            CoffeeReviewManager.loadReview(this);
        }
        CoffeeDataLocal.saveDataKeyInt(firstStartApp + 1, FIRST_START_APP);




        setButtonsActions();

        searchView.updateCallback(new Runnable() {
            @Override
            public void run() {
                loaderScreenAnimation(true);
            }
        }, new Runnable() {
            @Override
            public void run() {
                updateSortFilter();
            }
        }, new Runnable() {
            @Override
            public void run() {

            }
        });


        updateTagsView();

//        tagView.scrollToPosition(filter - 1);
//        selectedTagCellIndex(filter);




        /// Если нет сохранненной базы кофешек
        if (CoffeeDataManagerNew.emptyLoadLocalFirst) {
            updateRecyclerView();
        } else {
            updateDataAll();
        }

        updateSwipeRefresh();

        slidingPanelLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }
            public String getPanelStateString( SlidingUpPanelLayout.PanelState previousState) {

                String previousStr = null;
                if (previousState == EXPANDED)  { previousStr = " v EXPANDED "; }
                if (previousState == COLLAPSED) { previousStr = " ^ COLLAPSED"; }
                if (previousState == ANCHORED)  { previousStr = "   ANCHORED "; }
                if (previousState == HIDDEN)    { previousStr = " - HIDDEN   "; }
                if (previousState == DRAGGING)  { previousStr = "   DRAGGING "; }
                return previousStr;
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {

                String newStr = getPanelStateString(newState);
                String previousStr = getPanelStateString(previousState);

                boolean valueInt = (newState == EXPANDED);
                int ID = panel.getId();
                String value = (valueInt ? "top" : "bottom");
                String detailsOpen = (openDetails ? "Open Details \uD83D\uDED1" : "Open List    \uD83C\uDF15");

                String text = String.format("%s|%s|%s|%s|move = %s", detailsOpen, previousStr, newStr, ID, value);
                Log.d("\uD83D\uDCA6\uD83D\uDD25️ container MOVE", text);

//                slidingPanelLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
//                containerItems     = (ConstraintLayout)     findViewById(R.id.container);
//                containerMap       = (ConstraintLayout)     findViewById(R.id.map);


                if (!openDetails2) {
                    if (previousState == COLLAPSED) {
                        containereListLastPositionTop = true;
                    } else if  (previousState == EXPANDED) {
                        containereListLastPositionTop = false;
                    }

                    Log.d("\uD83D\uDD35\uD83D\uDD35\uD83D\uDD35️ ",  String.format("SET containereListLastPositionTop %s", containereListLastPositionTop ? "TOP" : "BOTTOM") );
                }

                if (panel.getId() == R.id.sliding_layout) {
                    if (containereMapOpen != valueInt) {
                        containereMapOpen = valueInt;

                        Log.d("\uD83D\uDCA6\uD83D\uDD25️", "\uD83D\uDCA6\uD83D\uDD25");


                        int duration = 500;
                        List<View> views = new ArrayList<>();
                        views.add(menuMapButton);
                        views.add(searchMapButton);
                        views.add(locationButton);

                        ViewAlphaAnimation.changes(views, !containereMapOpen, duration);
                        if (!containereMapOpen) {
                            searchView.selectOpen(false);
                        }
                    }
                }
            }
        });

        // addLogs();

    }



//    void startLogsActivity() {
//
//        Intent intent = new Intent(this, OnScreenLog.class);
//        startActivity(intent);
//
////        finish();
//    }

    public void addLogs() {

        tv.setVisibility(View.VISIBLE);

        try {
            Process process = Runtime.getRuntime().exec("logcat -d");
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            StringBuilder log=new StringBuilder();
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                log.append(line);
            }

            tv.setText("");
            tv.setText(log.toString());
            tv.setMovementMethod(new ScrollingMovementMethod());

//            Log.d("\uD83E\uDD8B\uD83E\uDD8B\uD83E\uDD8B\uD83E\uDD8B", log.toString());

        } catch (IOException e) {
            // Handle Exception
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d("⚙️\uD83D\uDD25\uD83D\uDCA5", "onKeyDown ");
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        Log.d("⚙️\uD83D\uDD25\uD83D\uDCA5", "onKeyDown onBackPressed");

        if (openDetails) {
            closeCoffeeDetails();
        } else {
            super.onBackPressed();
        }
    }

    /* Загрузка всех ссылок UI компонентов XML                                                    */

    public void loadAllViewsById(Bundle savedInstanceState) {

//        mapAlertView        = (CardView)             findViewById(R.id.alertLayout);        // | CardView
//        addAlertView();                                                                     // |+ CoffeeMapBannerView
//        mapAlertView.setVisibility(View.GONE);

        menuMapButton      = (FloatingActionButton) findViewById(R.id.buttonMapMenu);           // FloatingActionButton
        searchMapButton      = (FloatingActionButton) findViewById(R.id.buttonMapSearch);           // FloatingActionButton
        locationButton      = (FloatingActionButton) findViewById(R.id.buttonMapLocation);           // FloatingActionButton

        slidingPanelLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);

        containerItems     = (ConstraintLayout)     findViewById(R.id.container);
        containerMap       = (ConstraintLayout)     findViewById(R.id.map);
        navBar             = (ConstraintLayout)     findViewById(R.id.navBar);
        buttonClose        = (ImageView)            findViewById(R.id.buttonClose);
        buttonCloseLayout  = (ConstraintLayout)     findViewById(R.id.buttonCloseLayout);
        ViewAlphaAnimation.change(buttonClose, false, 0);
        ViewAlphaAnimation.change(buttonCloseLayout, false, 0);

        // listMapButton      = (FloatingActionButton) findViewById(R.id.fab);                // FloatingActionButton
//        alertViewMoveBottom(true, false, 0);

        mapView             = (MapView)              findViewById(R.id.mapView);            // MapView
        updateMapView(savedInstanceState);

        recyclerViewRefresh = (SwipeRefreshLayout)   findViewById(R.id.swipe_layout);       // | SwipeRefreshLayout
        recyclerView        = (RecyclerView)         findViewById(R.id.recycler);           // |- RecyclerView

        firstLoaderView     = (ConstraintLayout)     findViewById(R.id.firstLoaderView);    // ConstraintLayout
        // firstLoaderView.setVisibility(View.GONE);

        loaderScreen        = (ConstraintLayout)     findViewById(R.id.loaderScreen);       // ConstraintLayout
        //loaderScreen.setVisibility(View.GONE);
        listSpinner         = (ProgressBar)          findViewById(R.id.listSpinner);        // ProgressBar
        mapSpinner          = (ProgressBar)          findViewById(R.id.mapSpinner);         // ProgressBar
        mapSpinner.setVisibility(View.GONE);

        searchView          = (CoffeeSearchBar)      findViewById(R.id.searchView);         // CoffeeSearchBar
        searchView.selectOpen(false);
        tagView             = (RecyclerView)         findViewById(R.id.tags);               // RecyclerView
//        menuButton          = (CardView)             findViewById(R.id.buttonMenu);         // CardView/

        titleLabel          = (TextView)             findViewById(R.id.text5);              // CardView

        changeCityButton    = (ImageView)            findViewById(R.id.buttonCity);         // ImageView
        changeCityView      = (ConstraintLayout)     findViewById(R.id.buttonCity2);        // ConstraintLayout
        tv = (TextView)findViewById(R.id.textView1);
        ObjectAnimator.ofFloat(changeCityButton, View.ROTATION, 0f, 180f).setDuration(0).start();


    }


    /* Дата-Манагер Callbacks                                                                     */

    @Override public void downloadFirebaseCallback(final List<CoffeeAddress> result) {              Log.d("✅", "CoffeeListActivity | downloadFirebase.Callback ");
        runOnUiThread(new Runnable() { @Override public void run() {
                dataManagerLoadingFinish(result);
        }});
    }

    //--------------------------------------------------------
    @Override public void filteredCallback(final List<CoffeeAddress> result) {                      Log.d("✅", "CoffeeListActivity | filtered.Callback ");
        runOnUiThread(new Runnable() { @Override public void run() {
                dataManagerLoadingFinish(result);
        }});
    }
    //--------------------------------------------------------
    void dataManagerLoadingFinish(List<CoffeeAddress> result) {

        filterCoffeeAddresses = result;

        recyclerViewRefresh.setRefreshing(false);

        if (CoffeeDataManagerNew.updateRefresh) {                                                   Log.d("✅", "CoffeeListActivity | update Refresh | задержка 1 секунда "); // 🌀
            delay(600, new Runnable() { @Override public void run() {
                updateDataAll();
            }});
        } else {
            updateDataAll();
        }

    }
    //--------------------------------------------------------
    @Override public void detailsActivityBackCallback(final  CoffeeAddress address) {               Log.d("✅", "CoffeeListActivity | detailsActivity.Click -> Back.Callback");

        if (openLastAddressFavorite == address.favorite) return;
        updateDataAll();
    }

    /* Спинеры загрузки                                                                           */

    void loaderScreenAnimation(boolean show) {

        if (show) {
            Log.d("\uD83C\uDF00", "Спинеры загрузки | loaderScreenAnimation ✅️ показать "); // 🌀
        }
        else {
            Log.d("\uD83C\uDF00", "Cпинеры загрузки | loaderScreenAnimation ❌ скрыть");
        }

        View spinnerView;
//        if (visibleMap) {
//            spinnerView = mapSpinner;
//            loaderScreen.setVisibility(View.GONE);
//        } else {
            spinnerView = loaderScreen;
            mapSpinner.setVisibility(View.GONE);
//        }

        ViewAlphaAnimation.change(spinnerView, show, 300);
    }


    /* Обновление данных на Списке и Карте                                                        */

    void updateDataAll() {
        updateDataAll(null);
    }                                      // 📝️

    void updateDataAll(Runnable completion) {                                                       Log.d("\uD83D\uDCDD️ ", String.format("updateDataAll | filter %s", updateFilter ? "✅" : "❌")); // 📝️

        if (firstLoaderView.getVisibility() == View.VISIBLE) {                                      Log.d("✅", "firstLoaderView | анимация скрывания 0.6 секунд");
            delay(600, new Runnable() { @Override public void run() {
                ViewAlphaAnimation.change(firstLoaderView, false, 600);
            }});
        }

        if (updateProcess) return;
            updateProcess = true;

        if (CoffeeDataManagerNew.endDownloadFirebaseFirst) {  // Обновление данных через Firebase - закончилось
            loaderScreenAnimation(false);
        }

        updateRecyclerView();

        if (openDetailsType != Map) {
            mapManager.addAllCoffeeMarkers(filterCoffeeAddresses);                                      // 🗺
        }
        updateProcess = false;
        if (completion != null) completion.run();

        if (updateFilter) updateSortFilter();
    }

    /* Сортировка Фильтрация                                                                      */

    void updateSortFilter() {                                                                       Log.d("\uD83D\uDCDD", "updateSortFilter"); // 📝

        int index = filter;

        if (updateProcess) {
            updateFilter = true;
            return;
        }

        if (CoffeeMapManager.myLocation != null)                                                    Log.d("\uD83D\uDCDD️\uD83D\uDCCD ", String.format("updateSortFilter | myCoord %s", UtilsString.strCoordinate(CoffeeMapManager.myLocation) )); // 📝️📍


        CoffeeDataManagerNew.sortCoffeeAddressDistance(index);

        updateFilter = false;
    }

    /* Установка действий для Кнопок                                                              */

    private void setButtonsActions() {

        buttonCloseLayout.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                closeCoffeeDetails();
            }
        });
        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                closeCoffeeDetails();
            }
        });

        changeCityButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                buttonChangeCityAction();
            }
        });
        changeCityView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                buttonChangeCityAction();
            }
        });

        menuMapButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                openMenuActivity();
            }
        });

        final AppCompatActivity activity = this;

        locationButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {

//                LogcatActivity.launch(CoffeeListActivity.this);
//                 addLogs();

                if (!accessLocationEnabled()) {
                    PermissionUtils.requestPermissionOnceFirst = false;
                    CoffeeAccessLocationManager.requestPermission(activity);                        Log.d("❤️ ", "CoffeeAccessLocationManager request access Location");
                }

                mapManager.moveMyLocation();
            }
        });
        searchMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchMapButtonAction();
            }
        });
    }

    public void searchMapButtonAction() {

        if (searchView.open) {
            searchView.selectOpen(false);
//            HideKeyboard.hideSoftKeyboard(this);
        } else {
            searchView.selectOpen(true);
            slidingPanelLayout.setPanelState(EXPANDED);
        }
    }
        /* Обновление Карты                                                                           */

    public void updateMapView(Bundle bundle) {                                                     Log.d("\uD83D\uDDFA ", "Обновление Карты | updateMapView "); // 🗺

//        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
//        if (resultCode == ConnectionResult.SUCCESS) {
//            // Load Map as Native
//            // https://developers.google.com/maps/documentation/android-sdk/start
//        }else{
//            // Load Map in Webview/Customtabs
//            // https://developers.google.com/maps/documentation/javascript/overview
//        }

        CoffeeAccessLocationManager.activity = this;

        mapManager = new CoffeeMapManager(bundle, this, mapView, darkMode, new CoffeeMapManagerCallback() {
            //------------------------------------------------
            @Override public void onMapReady(GoogleMap map) {                                       Log.d("\uD83D\uDDFA ", "получение GoogleMap map <= | onMapReady"); // 🗺

                //if (!accessLocationEnabled()) {
                    mapManager.addAllCoffeeMarkers(filterCoffeeAddresses);
                //}
            }
            //------------------------------------------------
            @Override public void mapUpdateMyLocation(boolean timer) {                              Log.d("\uD83D\uDDFA ", "обновление локации | mapUpdateMyLocation"); // 🗺

                if (timer) {
                    loaderScreenAnimation(true);
                    saveRecyclerScrollPosition();
                }

                updateSortFilter();
            }
            //------------------------------------------------
            @Override public void mapSelectMarker(Marker marker) {                                  Log.d("\uD83D\uDDFA ", "нажатие маркера | mapSelectMarker"); // 🗺

                if (openDetailsType == List) {
                    openDetailsType = Empty;
                    return;
                }

                CoffeeAddress address = (CoffeeAddress) marker.getTag();

                mapManager.selectedMarker = marker;
                addressDetails = address;
                openDetailsType = Map;
                openCoffeeDetailsActivity();

            }
            //------------------------------------------------
            @Override public void mapUnSelectMarker() {                                             Log.d("\uD83D\uDDFA ", "нажатие на карту - отцепить маркер | mapUnSelectMarker"); // 🗺

            }
        });
    }

    /* Life Cycle                                                                                 */

    @Override protected void onResume() {
        super.onResume();
        mapView.onResume();
        //CoffeeAccessLocationManager.accessLocationEnabled();
    }
    //-------------------------------------------------------
    @Override protected void onStart() { // Один из стартовых методов экрана
        super.onStart();
        mapView.onStart();
    }
    //------------------------------------------------------
    @Override protected void onStop() { // Остановил экран - потому что перешел на следующий
        super.onStop();
        mapView.onStop();
    }
    //------------------------------------------------------
    @Override protected void onPause() {
        mapView.onPause();
        super.onPause();
    }
    //------------------------------------------------------
    @Override protected void onDestroy() { // Вышел из экрана
        mapView.onDestroy();
        super.onDestroy();
    }
    //------------------------------------------------------
    @Override public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    /* Результат разрешения доступа геолокации                                                    */

    @SuppressLint("MissingSuperCall")
    @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] results) {

        boolean result = CoffeeAccessLocationManager.onRequestPermissionsResult(requestCode, permissions, results);
        if (result) {

            if (CoffeeAccessLocationManager.gpsEnabled(this)) {
                if (NetworkConnected.check(this)) {

                    runOnUiThread(new Runnable() { @Override public void run() {
                        loaderScreenAnimation(true);
                        CoffeeDataManagerNew.trueRequestPermissionsResult = true;
                    }});

                }
            } else {
                String message = getString(R.string.permission_location_enable);
                UtilsAlert.alertMessage(this, message);
            }

            mapManager.updateMyLocation();
            CoffeeMapManager.firstAccessMyLocation = true;

        }
    }
    //------------------------------------------------------
    @Override protected void onResumeFragments() {
        super.onResumeFragments();

        CoffeeAccessLocationManager.onResumeFragments();
    }

    /* Навигатор-бар Теги                                                                         */

    private void updateTagsView() {

        updateTagsItems();
        tagAdapter = new RecyclerAdapter(tagItems);

        tagView.setAdapter(tagAdapter);
    }
    //----------------------------------------------------------------------------------------------
    private void updateTagsItems() {

        for ( String filterName : CoffeeDataManagerNew.filterString() ) {
            tagItems.add(new CoffeeMarketTagCell(this, filterName));
        }
    }
    //----------------------------------------------------------------------------------------------
    @Override public void selectedTagIndex(final int index) {
        openDetailsType = Empty;
        selectedTagCellIndex(index);

        lastSelectedFilter = index;

        if (pauseSelectTag) {
            return;
        } else {
            pauseSelectTag = true;

            delay(1000, new Runnable() { @Override public void run() {
                pauseSelectTag = false;

                if (lastSelectedFilter != filter) updateDataFilterIndex(lastSelectedFilter);
            }});
        }

        updateDataFilterIndex(index);
    }
    //----------------------------------------------------------------------------------------------
    void updateDataFilterIndex(int index) {
        updateDataFilterIndex(index, true);
    }

    void updateDataFilterIndex(int index, boolean spinner) {                                        Log.d("❤️ ", "updateDataFilterIndex");

        openDetailsType = Empty;

        if (spinner) loaderScreenAnimation(true);

        filter = index;

        mapManager.hideSelectedMarker();

        CoffeeDataLocal.saveDataKeyInt(filter, FILTER);

        updateSortFilter();
    }

    //----------------------------------------------------------------------------------------------
    void selectedTagCellIndex(int index) {                                                          Log.d("❤️ ",  String.format("selectedTagCellIndex %d", index) );

        for (int i = 0; i < tagAdapter.items.size(); i++) {

            CoffeeMarketTagCell item = (CoffeeMarketTagCell) tagAdapter.items.get(i);

            item.selected = (i == (index - 1));
            tagAdapter.notifyItemChanged(i);
        }
    }

    /* Список свайп обновление | Swipe-Refresh                                                    */

    void updateSwipeRefresh() {

        recyclerViewRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override public void onRefresh() {

                CoffeeDataManagerNew.updateRefresh = true;
                CoffeeDataManagerNew.updateDownloadCoffeeFirebaseTask();
            }
        });

        recyclerViewRefresh.setColorSchemeResources( R.color.coffeeGreen);

//        /* Отображение анимации Swipe Refresh при создании действия
//          * Поскольку анимация не запускается на onCreate, используется post runnable */
        recyclerViewRefresh.post(new Runnable() { @Override public void run() {

            if (recyclerViewRefresh != null) {
                recyclerViewRefresh.setRefreshing(true);
                recyclerViewRefresh.setRefreshing(false);
            }
        }});
    }

    /* Список                                                                                     */

    private void updateRecyclerView() {

        saveRecyclerScrollPosition();

        updateItemsCallback(new UpdateItemsCallback() {
            @Override public void updatedItemsResult(List<RecyclerAdapterCell> items) {
                updateRecyclerAdapterItems(items);
            }
        });
    }

    void updateRecyclerAdapterItems(List<RecyclerAdapterCell> items) {

        if (openDetails) {
            return;
        }

        if ((recyclerAdapter == null) || (openDetailsRecyclerUpdate)) {
            openDetailsRecyclerUpdate = false;
            recyclerAdapter = new RecyclerAdapter(items);
            recyclerView.setAdapter(recyclerAdapter);
        } else {
            recyclerAdapter.items = items;
            recyclerAdapter.notifyDataSetChanged();
//            recyclerView.invalidate();
        }

        recyclerView.scrollToPosition(recyclerLastScrollPosition);
    }

    //----------------------------------------------------------------------------------------------
    // сохранение проскроллированной-позиции в списке
    void saveRecyclerScrollPosition() {

        LinearLayoutManager recyclerLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        if (recyclerLayoutManager != null)
            recyclerLastScrollPosition = recyclerLayoutManager.findFirstVisibleItemPosition();
    }
    //----------------------------------------------------------------------------------------------


    private void updateItemsCallback(final UpdateItemsCallback callback) {

        final Context context = this;

        if (filterCoffeeAddresses.isEmpty() || !(NetworkConnected.check(context))) {

            if (   (filter == 0)
                || (filter == 2)
                || (filter == 3)
                || (filter == 10)) {

                if (callback != null) {

                    List<RecyclerAdapterCell> emptyCells = updateItemsEmpty();
                    callback.updatedItemsResult(emptyCells);
                }
            }

        } else {
            CoffeeDataManagerNew.updateItemsCell(this, new UpdateItemsCallback() {
                @Override public void updatedItemsResult(List<RecyclerAdapterCell> items) {
                    if (callback != null) callback.updatedItemsResult(items);
                }
            });
        }
    }


    //----------------------------------------------------------------------------------------------
    private List<RecyclerAdapterCell> updateItemsEmpty() {

        final List<RecyclerAdapterCell> items = new ArrayList<>();

        String title = "", message = "", buttonText = "";

        final int curFilter = CoffeeDataManagerNew.coffees.isEmpty() ? 0 : filter;

        final Context context = this;

        if (!(NetworkConnected.check(context))) {
                        title = "Подключитесь к Интернету"; message = "Проверьте подключение к сети \n" +
                                                                      "и повторите попытку";                                              buttonText = "Повторить";
        } else {
            switch (curFilter) {
                case 0: title = "Нет результатов";          message = "По такому запросу кофейни не найдены \n" +
                                                                      "и повторите попытку";                                              buttonText = "Повторить"; break;
                case 2: title = "Новых кофеен не найдено";  message = "По этому городу пока нет новых кофеен.\n" +
                                                                      "Мы стараемся поддерживать актуальные данные о кофейных точках по 3-ем городам.\n" +
                                                                      "Поэтому возможно скоро они появятся в новом обновлении\uD83D\uDE0A";     buttonText = "Вернуться к списку"; break;
                case 3: title = "Добавляйте в Избранные";   message = "Добавляйте любимые места в избранные нажав на сердечко в кофейне"; buttonText = "Выбрать кофейню"; break;
                case 8: title = "История просмотров";       message = "В этом списке пока нет просмотренных кофеен";                      buttonText = "Вернуться к списку"; break;
                default: break;
            }
        }


        int height = recyclerView.getLayoutParams().height;

        items.add(new CoffeeListEmplyInfoCell(title, message, buttonText, height, new View.OnClickListener() {
            @Override public void onClick(View view) {

                if (!(NetworkConnected.check(context))) {

                    loaderScreenAnimation(true);

                    delay(500, new Runnable() { @Override public void run() {
                        CoffeeDataManagerNew.updateDownloadCoffeeFirebase();
                    }});
                } else {
                    selectedTagCellIndex(0);
                    updateDataFilterIndex(0);
                }

            }
        }));

        return items;
    }

    //----------------------------------------------------------------------------------------------
    @Override public void recyclerViewListClicked(View v, int position) {

        CoffeeAddress address = filterCoffeeAddresses.get(position);
        openLastAddressFavorite = address.favorite;

        addressDetails = address;

        CoffeeIndex coffeeIndex = CoffeeDataManagerNew.getIndexToAddress(address);
        openDetailsType = List;
        openCoffeeDetailsActivity( coffeeIndex );
    }

    /* Закрыть экран Деталей                                                                      */

    public void closeCoffeeDetails() {

        openDetailsType = Empty;

        openCoffeeDetailsMapMarker(false);

        openDetails = false;
        addressDetails = null;

        loaderScreenAnimation(true);
        slidingPanelLayout.setPanelState( HIDDEN);

         delay(containereMoveAnimationSpeed, new Runnable() { @Override public void run() {

             float d = getResources().getDisplayMetrics().density;
            slidingPanelLayout.setPanelHeight((int) (168 * d));

            closeCoffeeDetailsUpdate();
            updateRecyclerView();

            delay(containereDelay, new Runnable() { @Override public void run() {

                Log.d("\uD83C\uDF15\uD83C\uDF15\uD83C\uDF15️ ",  String.format("containereListLastPositionTop %s", containereListLastPositionTop ? "TOP" : "BOTTOM") );

                openDetails2 = false;
                slidingPanelLayout.setPanelState( containereListLastPositionTop ? EXPANDED : COLLAPSED );

                loaderScreenAnimation(false);
            }});
        }});
    }

    public void closeCoffeeDetailsUpdate() {


        ViewAlphaAnimation.change(navBar, true, 500);
        ViewAlphaAnimation.change(buttonClose, false, 0);
        ViewAlphaAnimation.change(buttonCloseLayout, false, 0);

        recyclerView.setVerticalScrollBarEnabled(true);



        ScreenSize.setMargins(this, recyclerView, 0,104,0,0);
    }


    /* Открыть экран Деталей                                                                      */

    public void openCoffeeDetailsActivity() {

        Marker selectedMarker = mapManager.selectedMarker;

        if ((selectedMarker == null) || (selectedMarker.getTag() == null)) { return; }

        CoffeeAddress sel_address = (CoffeeAddress)selectedMarker.getTag();
        CoffeeIndex coffeeIndex = CoffeeDataManagerNew.getIndexToAddress(sel_address);
        openCoffeeDetailsActivity( coffeeIndex );
    }

    //-----------------------------------------------------
    public void openCoffeeDetailsActivity(final CoffeeIndex coffeeIndex) {


        openCoffeeDetailsMapMarker(true);

        openDetails = true;
        openDetails2 = true;

        loaderScreenAnimation(true);

        slidingPanelLayout.setPanelState(HIDDEN);


        delay(containereMoveAnimationSpeed, new Runnable() { @Override public void run() {

            float d = getResources().getDisplayMetrics().density;
            slidingPanelLayout.setPanelHeight((int) (300 * d));
            openCoffeeDetailsActivityContainerOpen(coffeeIndex);

            main(containereDelay, new Runnable() { @Override public void run() {
                slidingPanelLayout.setPanelState(COLLAPSED);
                loaderScreenAnimation(false);
            }});
        }});
    }
    public void openCoffeeDetailsMapMarker(boolean selected) {

        if (openDetailsType == Map) {
            mapManager.markerChangeIcon(mapManager.selectedMarker, true);
        }

        if (selected) {

            if (openDetailsType == Map) {
                return;
            }

            Marker marker = mapManager.mapSearchMarker(addressDetails);
            if (marker != null) {


                LatLng markerLocation = marker.getPosition();
                mapManager.moveAnimationLocation(markerLocation);
                mapManager.mapSelectMarker(marker);
            }

        } else {
            mapManager.hideSelectedMarker();
        }
    }
    //------------------------------------------------------
    public void openCoffeeDetailsActivityContainerOpen(CoffeeIndex coffeeIndex) {

        addressDetails = CoffeeDataManagerNew.getAddressToIndex(coffeeIndex);

        CoffeeDataManagerNew.detailsActivityBackCallback.detailsActivityBackCallback(addressDetails);

        addressDetails.history = true;
        CoffeeDataLocalArray.addAddressKey(addressDetails, HISTORY);

        openDetailsRecyclerUpdate = true;

        recyclerDetailsItems = CoffeeDetailsNewActivity.updateGetRecyclerView(addressDetails, addressDetails.coffee, darkMode, this, this, this);

        CoffeeDetailsNewActivity.updateItemsGlobal(recyclerView, recyclerDetailsItems);

        ViewAlphaAnimation.change(navBar, false, 500);
        ViewAlphaAnimation.change(buttonClose, true, 0);
        ViewAlphaAnimation.change(buttonCloseLayout, true, 0);

        ScreenSize.setMargins(this, recyclerView, 0,0,0,0);
    }


    /* Экран Деталей Button Actions                                                               */

    public void routeClick() {

        LatLng myLocation = CoffeeMapManager.myLocation;
        if (myLocation != null) {
            CoffeeUrlScheme.mapRouteAlertMessage(this, addressDetails.coord, myLocation);
        }
    }

    @Override
    public void sharedInfo() {
        CoffeeUrlScheme.shareInfo(this, addressDetails);
    }

    @Override
    public void clickFavorite() {
        CoffeeDataLocalArray.addAddressKey(addressDetails, FAVORITE);
    }

    @Override
    public void instagramClickImage() {

    }

    @Override
    public void instagramOpen() {
        CoffeeUrlScheme.instagram(this, addressDetails.coffee.insta);
    }

    //----------------------------------------------------------
    @Override public void detailsClickMapPin(CoffeeAddress address) {
        mapManager.mapClickMovePin(address);
    }

    /* Открыть экран Меню                                                                         */

    void buttonChangeCityAction() {

//        if (!cityButtonShow) {
            buttonCityAnimationRotate(true);
//        } else {
//            buttonCityAnimationRotate(false);
//        }

        CustomAlert dialog = new CustomAlert(this, new CustomAlertActivity() {
            @Override public void customAlertCallback(int index) {

                String title = "";

                switch (index) {
                    case 0: title = "Москва"; break;
                    case 1: title = "Санкт-Петербург"; break;
                    case 2: title = "Казань"; break;
                }

                if (!title.isEmpty()) {

                    if (CoffeeDataManagerNew.city == index) return;

                    loaderScreenAnimation(true);
                    CoffeeDataManagerNew.city = index;
                    //CoffeeDataManagerNew.updateRefresh = true;
                    CoffeeDataManagerNew.updateDownloadCoffeeFirebaseTask();

                    LatLng latLng = moscow;
                    if (index == 1) {
                        latLng = spb;
                    } else if (index == 2) {
                        latLng = kazan;
                    }

                    mapManager.moveLocation( latLng, 10 ,true);

                    titleLabel.setText(title);
                }

                buttonCityAnimationRotate(false);
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();


//        float d = getResources().getDisplayMetrics().density;

        if (containereListLastPositionTop) {
            wmlp.gravity = Gravity.TOP | Gravity.LEFT;
            wmlp.y = 100;   //y position
        } else {
            wmlp.gravity = Gravity.BOTTOM | Gravity.LEFT;
            wmlp.y = 200;   //y position
        }

        wmlp.x = 2;     //x position

        dialog.show();
    }
    //-----------------------------------------
    void buttonCityAnimationRotate(boolean rotate) {

        cityButtonShow = rotate;

        if (rotate) {
            ObjectAnimator.ofFloat(changeCityButton, View.ROTATION, 180f , 0f).setDuration(250).start();
        } else {
            ObjectAnimator.ofFloat(changeCityButton, View.ROTATION, 0f, 180f).setDuration(250).start();
        }
    }

    //----------------------------------------------------------------------------------------------
    void openMenuActivity() {

        if (menuOpen) { return; }
            menuOpen = true;

        CoffeeMenuActivity modalMenu = new CoffeeMenuActivity(this);
        modalMenu.show(getSupportFragmentManager(), "modalMenu");
        modalMenu.setUpListener(new CoffeeMenuActivityCallback() {
            @Override public void menuClose() {
                menuOpen = false;
            }
        });
    }

    //----------------------------------------------------------------------------------------------
    @Override public void segmentClicked(int index) {
        mapManager.setMapType(index);
    }
    //----------------------------------------------------------------------------------------------
    @Override public void socialNetworkClick(int index) {

    }

    //----------------------------------------------------------------------------------------------
}