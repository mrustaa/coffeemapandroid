package mrusta.coffeeMapAndroid.ui.screen;

import android.view.View;

import mrusta.coffeeMapAndroid.model.CoffeeAddress;

public interface CoffeeDetailsNewActivityCallback {

    void detailsClickMapPin(CoffeeAddress address);
}

