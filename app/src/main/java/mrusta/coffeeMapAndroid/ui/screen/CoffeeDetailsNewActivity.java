package mrusta.coffeeMapAndroid.ui.screen;

import mrusta.coffeeMapAndroid.R;

import mrusta.coffeeMapAndroid.framework.recyclerAdapter.RecyclerAdapter;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;

import mrusta.coffeeMapAndroid.managers.CoffeeDataLocal;
import mrusta.coffeeMapAndroid.managers.CoffeeDataLocalArray;
import mrusta.coffeeMapAndroid.managers.CoffeeDataManagerNew;

import mrusta.coffeeMapAndroid.managers.CoffeeUrlScheme;
import mrusta.coffeeMapAndroid.managers.CoffeeWebView;
import mrusta.coffeeMapAndroid.managers.mapManager.CoffeeMapManager;
import mrusta.coffeeMapAndroid.managers.utils.HideStatusBar;
import mrusta.coffeeMapAndroid.model.Coffee;
import mrusta.coffeeMapAndroid.model.CoffeeAddress;
import mrusta.coffeeMapAndroid.model.CoffeeIndex;


import mrusta.coffeeMapAndroid.model.CoffeeNew;
import mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew.CoffeeDetailsNewClick;
import mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew.CoffeeDetailsNewLinkCell;
import mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew.CoffeeDetailsNewMapCell;
import mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew.CoffeeDetailsNewNameCell;
import mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew.CoffeeDetailsNewRouteCell;
import mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew.CoffeeDetailsNewAboutCell;
import mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew.CoffeeDetailsNewInstaCell;
import mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew.CoffeeDetailsNewSectionCell;
import mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew.CoffeeDetailsNewInfoMiniCell;
import mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew.CoffeeDetailsNewInfoCell;
import mrusta.coffeeMapAndroid.ui.cell.coffeeDetailsNew.CoffeeDetailsNewSeparatorCell;


import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import androidx.recyclerview.widget.RecyclerView;


import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import static mrusta.coffeeMapAndroid.managers.CoffeeDataLocalArray.Type.FAVORITE;
import static mrusta.coffeeMapAndroid.managers.CoffeeDataLocalArray.Type.HISTORY;
import static mrusta.coffeeMapAndroid.managers.CoffeeDataLocalArray.Type.NEWS;


public class CoffeeDetailsNewActivity extends AppCompatActivity implements CoffeeDetailsNewClick, CoffeeDetailsNewActivityCallback {
    //----------------------------------------------------------------------------------------------
    CoffeeAddress address;
    Coffee coffee;

    static CoffeeDetailsNewActivityCallback callback;

    RecyclerView recyclerView;
    RecyclerAdapter adapter;
    List <RecyclerAdapterCell> items = new ArrayList<>();

    boolean darkMode;
    //----------------------------------------------------------------------------------------------
    @Override protected void onCreate(Bundle savedInstanceState) {


//

//        CoffeeDetailsNewActivity.setUpListener(this);
//
//        Intent intent = new Intent(CoffeeListActivity.this, CoffeeDetailsNewActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//
//        intent.putExtra("coffee",  coffeeIndex.coffee  );
//        intent.putExtra("address", coffeeIndex.address );
//        startActivity(intent);

        loadTheme();
        super.onCreate(savedInstanceState);

        loadArgCoffee();

        HideStatusBar.hideStatusBar(this);

        address.history = true;
        CoffeeDataLocalArray.addAddressKey(address, HISTORY);
        // CoffeeDataManagerNew.historys = CoffeeDataLocal.loadType(HISTORY);

        setContentView(R.layout.activity_coffee_details_new);

        updateItems();
        updateRecyclerView();
    }

    @Override protected void onDestroy() {
        super.onDestroy();

        CoffeeDataManagerNew.detailsActivityBackCallback.detailsActivityBackCallback(address);

        Log.d("\uD83D\uDEA8\uD83D\uDEA8\uD83D\uDEA8️ ", "CoffeeDetailsNew Activity  onDestroy");
    }
    //----------------------------------------------------------------------------------------------
    void loadTheme() {

        int currentNightMode = getResources().getConfiguration().uiMode
                & Configuration.UI_MODE_NIGHT_MASK;

        if (currentNightMode == Configuration.UI_MODE_NIGHT_NO) {
            darkMode = false;
            setTheme(R.style.Light);
        } else if (currentNightMode == Configuration.UI_MODE_NIGHT_YES) {
            darkMode = true;
            setTheme(R.style.DarkDetails);
        }
    }
    //----------------------------------------------------------------------------------------------
    public static void setUpListener(CoffeeDetailsNewActivityCallback callback) {
        CoffeeDetailsNewActivity.callback = callback;
    }
    //----------------------------------------------------------------------------------------------
    @Override public void detailsClickMapPin(CoffeeAddress address) {
        callback.detailsClickMapPin(address);
        finish();
    }
    //----------------------------------------------------------------------------------------------
    void loadArgCoffee() {

        Bundle arguments = getIntent().getExtras();
        if (arguments != null) {
            int  coffeeIndex = arguments.getInt("coffee" );
            int addressIndex = arguments.getInt("address");
            address = CoffeeDataManagerNew.getAddressToIndex(new CoffeeIndex(coffeeIndex, addressIndex));
        }
        coffee = address.coffee;
    }
    //----------------------------------------------------------------------------------------------
    private void updateItems() {

        items.clear();

        items.add( new CoffeeDetailsNewNameCell(address) );
        items.add( new CoffeeDetailsNewRouteCell(this, address, darkMode, "МАРШРУТ") );
        items.add( new CoffeeDetailsNewAboutCell(coffee.content) ); // кофеином и вафельными трубочками

        items.add( new CoffeeDetailsNewInstaCell(this, this, coffee) );
//        items.add( new CoffeeDetailsNewInstaCell(R.drawable._rectangle_11, R.drawable.recycler_img_2, R.drawable.izobrazhenie_oreh_ti_vf ) );

        items.add( new CoffeeDetailsNewLinkCell(this, "Перейти в Instagram") );

         items.add( new CoffeeDetailsNewSectionCell("Кофе") );
        items.add( new CoffeeDetailsNewInfoCell(coffee));
        items.add( new CoffeeDetailsNewInfoMiniCell("Обжарщик",  coffee.stuff) );
        items.add( new CoffeeDetailsNewInfoMiniCell("Типы кофе", coffee.strTags()) );
        items.add( new CoffeeDetailsNewSeparatorCell() );

         items.add( new CoffeeDetailsNewSectionCell("Кофейня") );
        items.add( new CoffeeDetailsNewInfoMiniCell(address) );
        items.add( new CoffeeDetailsNewInfoMiniCell("Адрес",       address.street) );
        items.add( new CoffeeDetailsNewMapCell(this, address, this, darkMode) );

    }
    //----------------------------------------------------------------------------------------------
    public static RecyclerAdapter updateItemsGlobal(
            RecyclerView rec,
            List <RecyclerAdapterCell> rItems) {

        RecyclerAdapter rAdapter = new RecyclerAdapter(rItems);
        rec.setAdapter(rAdapter);
        rec.setVerticalScrollBarEnabled(false);
        return rAdapter;
    }

    public static List <RecyclerAdapterCell> updateGetRecyclerView(
            CoffeeAddress rAddress,
            Coffee rCoffee ,
            boolean rDarkMode,
            Activity rActivity,
            CoffeeDetailsNewClick rDetailsCallback,
            CoffeeDetailsNewActivityCallback rMapClickCallback) {

        List <RecyclerAdapterCell> rItems = new ArrayList<>();

        rItems.add( new CoffeeDetailsNewNameCell(rAddress) );
        rItems.add( new CoffeeDetailsNewRouteCell(rDetailsCallback, rAddress, rDarkMode, "МАРШРУТ") );
        rItems.add( new CoffeeDetailsNewAboutCell(rCoffee.content) ); // кофеином и вафельными трубочками

        rItems.add( new CoffeeDetailsNewInstaCell(rActivity, rDetailsCallback, rCoffee) );
//        items.add( new CoffeeDetailsNewInstaCell(R.drawable._rectangle_11, R.drawable.recycler_img_2, R.drawable.izobrazhenie_oreh_ti_vf ) );

        rItems.add( new CoffeeDetailsNewLinkCell(rDetailsCallback, "Перейти в Instagram") );

        rItems.add( new CoffeeDetailsNewSectionCell("Кофе") );
        rItems.add( new CoffeeDetailsNewInfoCell(rCoffee));
        rItems.add( new CoffeeDetailsNewInfoMiniCell("Обжарщик",  rCoffee.stuff) );
        rItems.add( new CoffeeDetailsNewInfoMiniCell("Типы кофе", rCoffee.strTags()) );
        rItems.add( new CoffeeDetailsNewSeparatorCell() );

        rItems.add( new CoffeeDetailsNewSectionCell("Кофейня") );
        rItems.add( new CoffeeDetailsNewInfoMiniCell(rAddress) );
        rItems.add( new CoffeeDetailsNewInfoMiniCell("Адрес",       rAddress.street) );
        rItems.add( new CoffeeDetailsNewMapCell(rActivity, rAddress, rMapClickCallback, rDarkMode) );

        return rItems;
    }

    //----------------------------------------------------------------------------------------------
    private void updateRecyclerView() {

        recyclerView = findViewById(R.id.recycler);
        adapter = new RecyclerAdapter(items);
        recyclerView.setAdapter(adapter);
    }
    //-------------------------------------------------------------------------------routeClick---------------
    @Override public void instagramClickImage() {

    }
    //----------------------------------------------------------------------------------------------
    @Override public void sharedInfo() {
        CoffeeUrlScheme.shareInfo(this, address);
    }
    //----------------------------------------------------------------------------------------------
    @Override public void clickFavorite() {
        CoffeeDataLocalArray.addAddressKey(address, FAVORITE);
        // CoffeeDataManagerNew.favorites = CoffeeDataLocal.loadType(FAVORITE);
    }
    //----------------------------------------------------------------------------------------------
    @Override public void routeClick() {

        LatLng myLocation = CoffeeMapManager.myLocation;
        if (myLocation != null) {
            CoffeeUrlScheme.mapRouteAlertMessage(this, address.coord, myLocation);
        }

        //CoffeeWebView.mapYandexRoute(this, address.coord, myLocation);
    }
    //----------------------------------------------------------------------------------------------
    @Override public void instagramOpen() {
        CoffeeUrlScheme.instagram(this, coffee.insta);
    }
    //----------------------------------------------------------------------------------------------
}















