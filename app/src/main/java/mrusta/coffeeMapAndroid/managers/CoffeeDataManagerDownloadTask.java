package mrusta.coffeeMapAndroid.managers;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mrusta.coffeeMapAndroid.managers.instaDownload.HttpManagerCompleted;
import mrusta.coffeeMapAndroid.managers.mapManager.CoffeeMapManager;
import mrusta.coffeeMapAndroid.model.Coffee;
import mrusta.coffeeMapAndroid.model.CoffeeAddress;
import mrusta.coffeeMapAndroid.model.CoffeeNew;
import mrusta.coffeeMapAndroid.managers.CoffeeDataManagerNew;

import static com.microsoft.appcenter.utils.HandlerUtils.runOnUiThread;
import static mrusta.coffeeMapAndroid.managers.CoffeeDataLocalArray.Type.FIREBASE;
import static mrusta.coffeeMapAndroid.managers.CoffeeDataLocalArray.Type.NEWS;


//----------------------------------------------------------------------------------------------


public class CoffeeDataManagerDownloadTask extends AsyncTask<ArrayList<ArrayList<HashMap<String, String>>>, Void, List<CoffeeAddress>> {

    public interface Callback {
        void callback(List<CoffeeAddress> result);
    }

    public static Callback callback;
    public static int city;

    public CoffeeDataManagerDownloadTask(int city, Callback callback) {
        CoffeeDataManagerDownloadTask.callback = callback;
        CoffeeDataManagerDownloadTask.city = city;
    }

    @Override protected List<CoffeeAddress> doInBackground(ArrayList<ArrayList<HashMap<String, String>>>... lists) {

        ArrayList<ArrayList<HashMap<String, String>>> a = (ArrayList<ArrayList<HashMap<String, String>>>) lists[0];

        List<HashMap<String, String>> new_     = (List<HashMap<String, String>>) a.get(0);
        List<HashMap<String, String>> response = (List<HashMap<String, String>>) a.get(1);
        List<HashMap<String, String>> spb      = (List<HashMap<String, String>>) a.get(2);
        List<HashMap<String, String>> kazan      = (List<HashMap<String, String>>) a.get(3);

        CoffeeDataLocalArray.saveFirebaseResponse(new_, response, spb, kazan);

        CoffeeDataManagerNew.coffees = Coffee.   convertArray(response);
        CoffeeDataManagerNew.coffeeAddresses = CoffeeDataManagerNew.createListAddresses(CoffeeDataManagerNew.coffees);

        CoffeeDataManagerNew.coffeeSpb = Coffee.   convertArray(spb);
        CoffeeDataManagerNew.coffeeSpbAddress = CoffeeDataManagerNew.createListAddresses(CoffeeDataManagerNew.coffeeSpb);

        CoffeeDataManagerNew.coffeeKazan = Coffee.   convertArray(kazan);
        CoffeeDataManagerNew.coffeeKazanAddress = CoffeeDataManagerNew.createListAddresses(CoffeeDataManagerNew.coffeeKazan);

        List<CoffeeAddress> addresses = CoffeeDataManagerNew.coffeeAddresses;
        if (city == 1) {
            addresses = CoffeeDataManagerNew.coffeeSpbAddress;
        } else if (city == 2) {
            addresses = CoffeeDataManagerNew.coffeeKazanAddress;
        }

        final List<CoffeeAddress> filteredResult = CoffeeDataManagerFilter.sortCoffeeAddressDistance(
                addresses,
                 CoffeeDataLocalArray.news,
                 CoffeeDataLocalArray.favorites,
                 CoffeeDataLocalArray.historys,
                CoffeeDataManagerNew.searchText,
                CoffeeMapManager.myLocation,
                CoffeeDataManagerNew.filterSelected
        );

        return filteredResult;
    }

//    @Override protected void onProgressUpdate(Integer... values) {
//        super.onProgressUpdate(values);
//    }

        @Override protected void onPostExecute(List<CoffeeAddress> result) {
            callback.callback(result);
        }
    }

