package mrusta.coffeeMapAndroid.managers;

import androidx.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;

public class CoffeeUtils {


    //----------------------------------------------------------------------------------------------
    public static float distanceFromLocation(@NonNull LatLng coordA, @NonNull LatLng coordB) {

        float lat_a = (float) coordA.latitude;
        float lng_a = (float) coordA.longitude;

        float lat_b = (float) coordB.latitude;
        float lng_b = (float) coordB.longitude;

        double earthRadius = 3958.75;
        double latDiff = Math.toRadians(lat_b-lat_a);
        double lngDiff = Math.toRadians(lng_b-lng_a);
        double a = Math.sin(latDiff /2) * Math.sin(latDiff /2) +
                Math.cos(Math.toRadians(lat_a)) * Math.cos(Math.toRadians(lat_b)) *
                        Math.sin(lngDiff /2) * Math.sin(lngDiff /2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double distance = earthRadius * c;

        int meterConversion = 1609;

        return new Float(distance * meterConversion).floatValue();
    }
    //----------------------------------------------------------------------------------------------
    public static String getStrEspressoCappuccino(int espresso, int cappuccino) {

        if (espresso != 0) {
            return String.format(" · %s-%s ₽", espresso, cappuccino);
        }
        return "";

    }
    //----------------------------------------------------------------------------------------------
    public static String getMeterDistance(float distance) {

        String meter = "";

        if (distance < 1000.0) {

            meter = String.format("%.0f м", distance);

        } else {
            String kmStr = "";

            int km = ((int)distance % 1000);
            if ( km < 100 ) {

            } else {
                kmStr = String.format("%d", (km / 100));
            }

            if (kmStr.isEmpty()) {
                meter = String.format("%.0f км", (distance / 1000));
            } else {
                meter = String.format("%.0f,%s км", (distance / 1000), kmStr);
            }
        }
        return meter;
    }
    //----------------------------------------------------------------------------------------------

}
