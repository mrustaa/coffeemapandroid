package mrusta.coffeeMapAndroid.managers;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigValue;

import java.util.Map;

public class CoffeeRemoteConfig {
    //----------------------------------------------------------------------------------------------
    static FirebaseRemoteConfig conf;
    static Activity act;
    static Runnable complete;
    //----------------------------------------------------------------------------------------------
    public static Map <String, FirebaseRemoteConfigValue> allValues;

    public static String url_facebook;      // https://www.facebook.com/coffeemap.ru
    public static String url_telegram;      // http://t.me/coffeemaps
    public static String url_instagram;     // http://instagram.com/coffeemap.ru

    public static String url_site;          // http://coffeemap.ru
    public static String url_send_forms;    // http://docs.google.com/forms/d/e/1FAIpQLSdCHjRxiGV1YEZTj9hIEOincI3hyLaq6u7LhIK6oMmXvq8jKA/viewform

    public static String email_feedback;    // feedback@coffeemap.ru
    public static String email_ios;         // motionrustam@gmail.com

//    public static String maps_api_key;      // ...

    //----------------------------------------------------------------------------------------------
    public static void load(Activity activity, Runnable completion) {

        act = activity;
        complete = completion;

        conf = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(3600)
                .build();
        conf.setConfigSettingsAsync(configSettings);

        loadAllValues();

        conf.fetchAndActivate().addOnCompleteListener(act, new OnCompleteListener<Boolean>() {
            @Override public void onComplete(@NonNull com.google.android.gms.tasks.Task<Boolean> task) {
                loadAllValues();
                complete.run();
            }
        });
    }

//    if (task.isSuccessful()) {
//        boolean updated = task.getResult();
//        Log.d("❤️ ", "");
//    }
    //----------------------------------------------------------------------------------------------

    public static String httpUrl(String text) {
        return String.format("http://%s", text);
    }
    public static String httpsUrl(String text) {
        return String.format("https://%s", text);
    }

    public static void loadAllValues() {

        allValues = conf.getAll();

        if (allValues.isEmpty()) {
            defaultValues();
        } else {
            loadRemoteValues();
        }
    }
    //----------------------------------------------------------------------------------------------
    public static void loadRemoteValues() {

        url_facebook    = conf.getString("url_facebook");
        url_telegram    = conf.getString("url_telegram");
        url_instagram   = conf.getString("url_instagram");

        url_site        = conf.getString("ulr_site");
        url_send_forms  = conf.getString("url_send_google_forms");

        email_feedback  = conf.getString("email");
        email_ios       = conf.getString("send_email"); // motionrustam@gmail.com

        // maps_api_key    = conf.getString("google_ maps_api_key_android");
    }
    //----------------------------------------------------------------------------------------------
    public static void defaultValues() {

        url_facebook    = "www.facebook.com/coffeemap.ru";
        url_telegram    = "t.me/coffeemaps";
        url_instagram   = "instagram.com/coffeemap.ru";

        url_site        = "coffeemap.ru";
        url_send_forms  = "docs.google.com/forms/d/e/1FAIpQLSdCHjRxiGV1YEZTj9hIEOincI3hyLaq6u7LhIK6oMmXvq8jKA/viewform";

        email_feedback  = "feedback@coffeemap.ru";
        email_ios       = "motionrustam@gmail.com";

        // maps_api_key    = "@string/google _maps_key";
    }
    //----------------------------------------------------------------------------------------------

}
