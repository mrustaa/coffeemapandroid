package mrusta.coffeeMapAndroid.managers;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

public class CoffeeWebView {

    //----------------------------------------------------------------------------------------------
    public static void url(Activity a, String url) {
        //Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));

        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
        browserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        browserIntent.setDataAndType(Uri.parse(url), "text/html");

        a.startActivity(browserIntent);
    }

}