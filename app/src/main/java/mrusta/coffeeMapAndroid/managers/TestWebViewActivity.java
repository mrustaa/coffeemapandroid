package mrusta.coffeeMapAndroid.managers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import mrusta.coffeeMapAndroid.R;
import mrusta.coffeeMapAndroid.managers.instaDownload.HttpManager;
import mrusta.coffeeMapAndroid.managers.instaDownload.HttpManagerCompleted;
import mrusta.coffeeMapAndroid.managers.instaDownload.HttpManagerCompletedClose;



public class TestWebViewActivity extends Activity implements HttpManagerCompleted, HttpManagerCompletedClose {

    public interface ImagesUrlCallback {
        void complection(List<String> urls);
    }

    private HttpManager httpManager;
    private WebView webview;

    @SuppressLint("JavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_web_view);



        httpManager = new HttpManager(this);
        httpManager.delegateClose = this;

        webview = (WebView) findViewById(R.id.webView);
        webview.getSettings().setJavaScriptEnabled(true);

//        webview.setVisibility(View.GONE);

//        webview.clearHistory();
//        webview.clearCache(true);
//        webview.getSettings().setBuiltInZoomControls(true);
//        webview.getSettings().setCacheMode(2);
        webview.getSettings().setSupportZoom(true);
        webview.getSettings().setUseWideViewPort(false);
        webview.getSettings().setLoadWithOverviewMode(false);

        webview.addJavascriptInterface(new MyJavaScriptInterface(new MyJavaScriptInterfaceCallback() {
            @Override
            public void htmlCodeCallback(String html) {
                Log.d("❤️❤️️❤❤️❤️️❤️", "htmlCodeCallback");
                httpManager.setWebViewHtmlCode(html);
            }
        }, new MyJavaScriptInterfaceCloseCallback() {
            @Override
            public void close() {
//                closeActivity();
            }
        }), "HTMLOUT");

        webview.loadUrl(CoffeeDataManagerNew.webViewInstaURL);

        webview.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                Log.d("❤️❤️️❤❤️❤️️❤️", url);

                if (url.startsWith("source://")) {
                    try {
                        String html = URLDecoder.decode(url, "UTF-8").substring(9);
                        //sourceReceived(html);
                        //Log.d("❤️❤️️❤❤️❤️️❤️", html);
                    } catch (UnsupportedEncodingException e) {
                        Log.e("example", "failed to decode source", e);
                    }
                    view.getSettings().setJavaScriptEnabled(false);
                    return true;
                }
                // For all other links, let the WebView do it's normal thing
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                webview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
                //webview.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
            }
        });

        webview.evaluateJavascript("(function(){return window.document.body.outerHTML})();",
                new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String html) {
                        //Log.d("❤️❤️️❤❤️❤️️❤️", html);
                    }
                });
    }

    @Override
    public void onTaskCompleted(String htmlCode) {
        Log.d("❤️❤️️❤❤️❤️️❤️", "onTaskCompleted");

        if (httpManager.instaURLs.size() != 0) {
            Log.d("❤️❤️️❤❤️❤️️❤️", "onTaskCompleted НАШЕЛ фотки");

            CoffeeDataManagerNew.testWebViewImagesURLsCallback.complection(httpManager.instaURLs);

            this.finish();
            //super.onBackPressed();
        }
    }

    @Override
    public void feedbackClose() {
        Log.d("❤️❤️️❤❤️❤️️❤❤️❤️️❤️❤️❤️️❤️️", "feedbackClose");

        CoffeeDataManagerNew.webViewCloseCallback.feedbackClose();

        webview.loadUrl(CoffeeDataManagerNew.webViewInstaURL);

//        this.finish();
    }

//    @Override
//    public void feedbackImgURLs(List<String> instaURLs) {
//        CoffeeDataManagerNew.webViewCloseCallback.feedbackImgURLs(instaURLs);
//    }

//    private void closeActivity() {
//        super.onBackPressed();
//    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            // your code
//            return true;
//        }
//fcloseCallback
//        return super.onKeyDown(keyCode, event);
//    }

    public interface MyJavaScriptInterfaceCallback {
        void htmlCodeCallback(String html);
    }

    public interface MyJavaScriptInterfaceCloseCallback {
        void close();
    }


    static class MyJavaScriptInterface
    {
        static MyJavaScriptInterfaceCallback callback;
        static MyJavaScriptInterfaceCloseCallback closeCallback;

        public MyJavaScriptInterface(MyJavaScriptInterfaceCallback callback, MyJavaScriptInterfaceCloseCallback closeCallback) {
            MyJavaScriptInterface.callback = callback;
            MyJavaScriptInterface.closeCallback = closeCallback;
        }

        @JavascriptInterface
        @SuppressWarnings("unused")
        public void processHTML(String html)
        {
            MyJavaScriptInterface.callback.htmlCodeCallback(html);

            //Log.d("❤️❤️️❤️", html);
            MyJavaScriptInterface.closeCallback.close();
            // process the html as needed by the app
        }
    }
}

