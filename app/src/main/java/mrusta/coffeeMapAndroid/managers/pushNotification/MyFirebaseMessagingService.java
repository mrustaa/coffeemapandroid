
package mrusta.coffeeMapAndroid.managers.pushNotification;

import mrusta.coffeeMapAndroid.R;
import mrusta.coffeeMapAndroid.managers.CoffeeDataManagerNew;
import mrusta.coffeeMapAndroid.ui.launch.LaunchActivity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;

import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;



public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override public void onMessageReceived(RemoteMessage remoteMessage) {

        CoffeeDataManagerNew.updateDownloadCoffeeFirebase();

        sendNotification(remoteMessage);
    }
    //----------------------------------------------------------------------------------------
    @Override public void onNewToken(String token) {
        Log.d("TAG", "Refreshed token: " + token);
    }
    //----------------------------------------------------------------------------------------

    private void sendNotification(RemoteMessage remoteMessage) {

        String title = remoteMessage.getNotification().getTitle();
        String body  = remoteMessage.getNotification().getBody();

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent intent = new Intent(getApplicationContext(), LaunchActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("pushnotification", "yes");
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.notification_channel_Id);

        NotificationCompat.Builder
            mBuilder = new NotificationCompat.Builder(this, channelId);
            mBuilder
                .setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_round))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setColor(Color.parseColor("#FFD600"))
                .setContentIntent(pendingIntent)
                .setChannelId(channelId)
                .setPriority(NotificationCompat.PRIORITY_LOW);

        NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            mNotifyManager.notify(1, mBuilder.build());

    }

//        OneTimeWorkRequest work = new OneTimeWorkRequest.Builder(MyWorker.class) .build();
//        WorkManager.getInstance().beginWith(work).enqueue();

//
////        Log.d(TAG, "From: " + remoteMessage.getFrom());
//
//         Intent intent = new Intent(this, CoffeeListActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent, PendingIntent.FLAG_ONE_SHOT);
//
//        String channelId = "myID";// getString(R.string.default_notification_channel_id);
//
//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//        NotificationCompat.Builder notificationBuilder =
//                new NotificationCompat.Builder(this, channelId)
//                        .setCategory(NotificationCompat.CATEGORY_TRANSPORT)
//                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
//                        .setShowWhen(true)
//                        .setSmallIcon(R.drawable.ic_stat_ic_notification)
//                        .setContentTitle(title)
//                        .setContentText(body)
//                        .setVibrate(new long[] { 1000, 1000 })
//                        .setLights(Color.BLUE, 700, 500)
//                        .setAutoCancel(true)
//                        .setSound(defaultSoundUri)
//                        .setContentIntent(pendingIntent)
//                ;
//                        //
//
//        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        // Since android Oreo notification channel is needed.
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationChannel channel = new NotificationChannel(channelId,"Channel human readable title", NotificationManager.IMPORTANCE_DEFAULT);
//            notificationManager.createNotificationChannel(channel);
//        }
//
//    }
    //----------------------------------------------------------------------------------------
}
