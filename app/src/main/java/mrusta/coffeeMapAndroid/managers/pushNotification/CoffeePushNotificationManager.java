package mrusta.coffeeMapAndroid.managers.pushNotification;


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import mrusta.coffeeMapAndroid.R;
import mrusta.coffeeMapAndroid.managers.CoffeeDataManagerNew;

public class CoffeePushNotificationManager {

    public static Context context;

    //----------------------------------------------------------------------------------------
    public static void loadContext(final Context context) {
        CoffeePushNotificationManager.context = context;

        clearAllBadge();
        createNotificationChannel();
        getToken();
        subscribeToTopic();
    }
    //----------------------------------------------------------------------------------------
    public static void createNotificationChannel() {

        // NotificationManager ntfManager = context.getSystemService(NotificationManager.class);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            String channelId = context.getString(R.string.notification_channel_Id);

            // String channelId = "fcm_fallback_notification_channel";
            NotificationChannel channel = new NotificationChannel(channelId,"Другие", NotificationManager.IMPORTANCE_HIGH);
            channel.enableVibration(true);
            channel.setShowBadge(true);
            notificationManager.createNotificationChannel(channel);
        }
    }

    //----------------------------------------------------------------------------------------
    public static void clearAllBadge() {

        NotificationManager ntfManager = context.getSystemService(NotificationManager.class);

        StatusBarNotification[] statusBarNotifications = ntfManager.getActiveNotifications();
        if (statusBarNotifications.length != 0) {
            CoffeeDataManagerNew.pushNotification = true;
        }

        ntfManager.cancelAll();

//        String channelId = "fcm_fallback_notification_channel";
//        NotificationChannel channel = ntfManager.getNotificationChannel(channelId);

        Log.d("❤️ ", "");
    }
    //----------------------------------------------------------------------------------------
    public static void getToken() {

//FirebaseInstanceId.getInstance().getInstanceId()
//                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() { @Override public void onComplete(@NonNull Task<InstanceIdResult> task) {

        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {

                        if (!task.isSuccessful()) {
                            Log.w("FirebaseMessaging", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult(); // String token = task.getResult().getToken();
                        String msg = String.format("\uD83C\uDF2B FCM registration token: %s", token);
                        Log.d("FirebaseMessaging", msg);
                        CoffeeDataManagerNew.pushToken = token;
                    }
                });
    }
    //----------------------------------------------------------------------------------------
    public static void subscribeToTopic() {

        FirebaseMessaging.getInstance().subscribeToTopic("android")
                .addOnSuccessListener(new OnSuccessListener<Void>() { @Override public void onSuccess(Void aVoid) {
                    Log.d("FirebaseMessaging", "\uD83C\uDF2B Subscribed to Android topic");
                }
                });

        FirebaseMessaging.getInstance().subscribeToTopic("all")
                .addOnSuccessListener(new OnSuccessListener<Void>() { @Override public void onSuccess(Void aVoid) {
                    Log.d("FirebaseMessaging", "\uD83C\uDF2B Subscribed to all topic");
                }
                });
    }
    //----------------------------------------------------------------------------------------
}
