package mrusta.coffeeMapAndroid.managers;

import mrusta.coffeeMapAndroid.R;

import mrusta.coffeeMapAndroid.model.Coffee;
import mrusta.coffeeMapAndroid.model.CoffeeAddress;
import mrusta.coffeeMapAndroid.model.CoffeeIndex;


import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CoffeeJSON {
    //----------------------------------------------------------------------------------------------
    public static List<Coffee> loadContext(Context context) {
        try {
            return CoffeeJSON.load(context);
        } catch (IOException | JSONException e) { }
        return null;
    }
    //----------------------------------------------------------------------------------------------
    public static List<Coffee> load(Context context) throws IOException, JSONException {

        // Read content of company.json
        String jsonText = readText(context, R.raw.coffeemap_export);
        JSONObject jsonRoot = new JSONObject(jsonText);

        JSONArray jsonArray = jsonRoot.getJSONArray("response");

        List <Coffee> result = new ArrayList<>();
        for (int i=0; i < jsonArray.length(); i++ ) {

            JSONObject jsonObj = jsonArray.getJSONObject(i);
            Coffee coffee = new Coffee(jsonObj);
            result.add(coffee);
        }

        return result;
    }

//    public static convertJsonToString(String text) {
//        JSONObject jsonRoot = new JSONObject(jsonText);
//    }
    //----------------------------------------------------------------------------------------------
    private static String readText(Context context, int resId) throws IOException {
        InputStream is = context.getResources().openRawResource(resId);
        BufferedReader br= new BufferedReader(new InputStreamReader(is));
        StringBuilder sb= new StringBuilder();
        String s= null;
        while((  s = br.readLine())!=null) {
            sb.append(s);
            sb.append("\n");
        }
        return sb.toString();
    }



    //----------------------------------------------------------------------------------------------
    public static String jsonString(JSONObject json, String key) {
        String result = "";
        if (json.has(key)) {
            try {
                result = json.getString(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    //----------------------------------------------------------------------------------------------
    public static int jsonInt(JSONObject json, String key) {
        int result = 0;
        if (json.has(key)) {
            try {
                result = json.getInt(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    //----------------------------------------------------------------------------------------------
    public static double jsonDouble(JSONObject json, String key) {
        double result = 0;
        if (json.has(key)) {
            try {
                result = json.getDouble(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    //----------------------------------------------------------------------------------------------
    public static JSONArray jsonArray(JSONObject json, String key) {
        JSONArray result = new JSONArray();
        try {
            result = json.getJSONArray(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }
    //----------------------------------------------------------------------------------------------
    public static String jsonArrayString(JSONArray array, int index) {
        String result = "";
        if (array.length() >= (index + 1)) {
            try {
                result = array.getString(index);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    //----------------------------------------------------------------------------------------------
    public static JSONObject jsonArrayObject(JSONArray array, int index) {
        JSONObject result = new JSONObject();
        if (array.length() >= (index + 1)) {
            try {
                result = array.getJSONObject(index);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    //----------------------------------------------------------------------------------------------
}
