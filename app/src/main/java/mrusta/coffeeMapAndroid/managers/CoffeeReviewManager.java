package mrusta.coffeeMapAndroid.managers;

import android.app.Activity;
import android.util.Log;

import mrusta.coffeeMapAndroid.R;
import mrusta.coffeeMapAndroid.managers.utils.UtilsAlert;

import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.android.play.core.tasks.Task;
import com.google.android.play.core.tasks.OnCompleteListener;

public class CoffeeReviewManager {

    public static ReviewManager reviewManager;
    public static Activity activity;

    //----------------------------------------------------------------------------------------------
    public static void loadReview(final Activity activity) {

        CoffeeReviewManager.activity = activity;
        CoffeeReviewManager.reviewManager = ReviewManagerFactory.create(activity);

        Task <ReviewInfo> request = reviewManager.requestReviewFlow();

        request.addOnCompleteListener(new OnCompleteListener<ReviewInfo>() {
            @Override public void onComplete(Task<ReviewInfo> task) {

                if (task.isSuccessful()) {

                    // Мы можем получить объект ReviewInfo
                    //UtilsAlert.alertToastLong(activity, "Мы можем получить объект ReviewInfo");

                    ReviewInfo reviewInfo = task.getResult();

                    launchReview(reviewInfo);
                } else {
                    // Возникла проблема, продолжить независимо от результата.
                    //UtilsAlert.alertToastLong(activity, "Возникла проблема, ❌ продолжить независимо от результата.");
                }
            }
        });
    }
    //----------------------------------------------------------------------------------------------
    public static void launchReview(ReviewInfo reviewInfo) {

        Task<Void> flow = reviewManager.launchReviewFlow(activity, reviewInfo);

        flow.addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override public void onComplete(Task<Void> task) {
                // Поток завершен. API не указывает, просмотрел ли пользователь
                // или нет, и даже был ли показан диалог обзора. Таким образом,
                // независимо от результата, мы продолжаем поток приложения.

                //UtilsAlert.alertToastLong(activity, "LaunchReview, ✅ Поток завершен. API не указывает, " +
                //        "просмотрел ли пользователь или нет, и даже был ли показан диалог обзора. " +
                //        "Таким образом, независимо от результата, мы продолжаем поток приложения.");

                Log.d("✅", "launchReview");
            }
        });
    }
    //----------------------------------------------------------------------------------------------

}
