package mrusta.coffeeMapAndroid.managers;

import androidx.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import mrusta.coffeeMapAndroid.managers.utils.UtilsString;
import mrusta.coffeeMapAndroid.model.Coffee;
import mrusta.coffeeMapAndroid.model.CoffeeAddress;
import mrusta.coffeeMapAndroid.model.CoffeeNew;

public class CoffeeDataManagerFilter {

    //----------------------------------------------------------------------------------------------
    public static List<CoffeeAddress> sortCoffeeAddressDistance(List<CoffeeAddress> coffeeAddresses,
                                                                String searchText,
                                                                LatLng myLocation,
                                                                int filterIndex) {

        return sortCoffeeAddressDistance(coffeeAddresses, null, null, null, searchText, myLocation, filterIndex);
    }
    //----------------------------------------------------------------------------------------------
    public static List<CoffeeAddress> sortCoffeeAddressDistance(List<CoffeeAddress> coffeeAddresses,
                                                                List<CoffeeNew> news,
                                                                List<CoffeeNew> favorites,
                                                                List<CoffeeNew> historys,
                                                                String searchText,
                                                                LatLng myLocation,
                                                                int filterIndex) {
        //---------------------------------------
        // Фильтрафия (открыто сейчас)
        for (CoffeeAddress address : coffeeAddresses) {
            openAddress(address);
        }
        //---------------------------------------

        // поиск Новых
        if (news != null) {
            searchAddressCoffeeNew(coffeeAddresses, news, new FindOneAddressNew() {
                @Override public void findOneAddress(CoffeeAddress findOneAddress) {
                    findOneAddress.new_ = true;
                }
            });
        }

        // поиск Избранных
        if (favorites != null) {
            searchAddressCoffeeNew(coffeeAddresses, favorites, new FindOneAddressNew() {
                @Override public void findOneAddress(CoffeeAddress findOneAddress) {
                    findOneAddress.favorite = true;
                }
            });
        }

        // поиск Просмотренных
        if (historys != null) {
            searchAddressCoffeeNew(coffeeAddresses, historys, new FindOneAddressNew() {
                @Override
                public void findOneAddress(CoffeeAddress findOneAddress) {
                    findOneAddress.history = true;
                }
            });
        }

        //---------------------------------------
        // сепарация по фильтру
        List<CoffeeAddress> filteredResult = new ArrayList<>();

        for (CoffeeAddress addr : coffeeAddresses) {

            if     (    (filterIndex == 0)                          // All
                    || ((filterIndex == 1) && (addr.open))          // Open
                    || ((filterIndex == 2) && (addr.new_))          // NEWS
                    || ((filterIndex == 3) && (addr.favorite))      // Favorite
                    || ((filterIndex == 4) && (addr.coffee.v60))    // V60
                    || ((filterIndex == 5) && (addr.coffee.veg))    // Veg
                    || ((filterIndex == 6) && (addr.coffee.decaf))  // Decaf
                    || ((filterIndex == 7) && (addr.coffee.sale))   // Sale
                    || ((filterIndex == 8) && (addr.coffee.dog))        // Dog friendly
                    || ((filterIndex == 9) && (addr.coffee.specialty))   // specialty
                    || ((filterIndex == 10) && (addr.history))       // History
            ){
                filteredResult.add(addr);
            }
        }

        //---------------------------------------
        // если есть локация - расчитать дистанцию
        if (myLocation != null) {

            for (CoffeeAddress address : filteredResult) {
                address.distance = distanceFromLocation(address.coord, myLocation);
            }

            // сортировка массива по дистанциям
            Collections.sort(filteredResult, new Comparator<CoffeeAddress>() {
                @Override public int compare(CoffeeAddress o1, CoffeeAddress o2) {
                    return Float.compare(o1.distance, o2.distance);
                }
            });
        }
        //---------------------------------------
        // поиск кофеен
        if (!searchText.isEmpty()) {

            List <CoffeeAddress> resultSort = new ArrayList<>();

            for (CoffeeAddress addr : filteredResult) {

                boolean findTitle   = (UtilsString.findText(addr.coffee.title,   searchText));
                boolean findStreet  = (UtilsString.findText(addr.street,         searchText));
                boolean findContent = (UtilsString.findText(addr.coffee.content, searchText));

                if (findTitle || findStreet || findContent) {
                    resultSort.add(addr);
                }
            }
            filteredResult = resultSort;
        }
        //---------------------------------------
        return filteredResult;

    }
    //----------------------------------------------------------------------------------------------
    private interface FindOneAddressNew {
        void findOneAddress(CoffeeAddress findOneAddress);
    }

    public static void searchAddressCoffeeNew(List<CoffeeAddress> a, List<CoffeeNew> b, FindOneAddressNew callback) {
        for (CoffeeAddress address : a) {                                 // инициализация НОВЫх
            for (CoffeeNew c : b) {
                if (address.street.equals(c.street) &&
                        address.coffee.title.equals(c.title)) {
                    callback.findOneAddress(address);
                }
            }
        }
    }
    //----------------------------------------------------------------------------------------------
    public static void filterCoffee(Coffee coffee) {

        if (!coffee.tags.isEmpty()) {
            for (String str : coffee.tags) {
                if (str.equals("V60"))           { coffee.v60   = true; }
                if (str.equals("Veg"))           { coffee.veg   = true; }
                if (str.equals("Декаф"))         { coffee.decaf = true; }
                if (str.equals("Продажа зерна")) { coffee.sale  = true; }
                if (str.equals("Dog friendly"))    { coffee.dog = true; }
                if (str.equals("Specialty cacao")) { coffee.specialty  = true; }

            }
        }
    }
    //----------------------------------------------------------------------------------------------
    public static void openAddress(CoffeeAddress address) {

        String street = address.street;
        String title  = address.coffee.title;

        String time = CoffeeDateFormater.todayDayWeekTime(address);

        address.open = ( UtilsString.findText(time, "Круглосуточно") ||
                UtilsString.findText(time, "Работает до") );

        // String strOpen = address.open ? "✅" : "\uD83D\uDEA8";
        // Log.d("\uD83E\uDDE9", String.format("|%s|%s|\uD83D\uDD25%s| %s %s", address.coffee.title, address.street, address.time, strOpen, time) );
    }

    //----------------------------------------------------------------------------------------------
    public static float distanceFromLocation(@NonNull LatLng coordA, @NonNull LatLng coordB) {

        float lat_a = (float) coordA.latitude;
        float lng_a = (float) coordA.longitude;

        float lat_b = (float) coordB.latitude;
        float lng_b = (float) coordB.longitude;

        double earthRadius = 3958.75;
        double latDiff = Math.toRadians(lat_b-lat_a);
        double lngDiff = Math.toRadians(lng_b-lng_a);
        double a = Math.sin(latDiff /2) * Math.sin(latDiff /2) +
                Math.cos(Math.toRadians(lat_a)) * Math.cos(Math.toRadians(lat_b)) *
                        Math.sin(lngDiff /2) * Math.sin(lngDiff /2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double distance = earthRadius * c;

        int meterConversion = 1609;

        return new Float(distance * meterConversion).floatValue();
    }
    //----------------------------------------------------------------------------------------------
}
