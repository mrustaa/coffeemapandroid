package mrusta.coffeeMapAndroid.managers;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import mrusta.coffeeMapAndroid.model.Coffee;
import mrusta.coffeeMapAndroid.model.CoffeeAddress;
import mrusta.coffeeMapAndroid.model.CoffeeNew;

import static mrusta.coffeeMapAndroid.managers.CoffeeDataLocalArray.Type.FAVORITE;
import static mrusta.coffeeMapAndroid.managers.CoffeeDataLocalArray.Type.FIREBASE;
import static mrusta.coffeeMapAndroid.managers.CoffeeDataLocalArray.Type.FIREBASE_SPB;
import static mrusta.coffeeMapAndroid.managers.CoffeeDataLocalArray.Type.FIREBASE_KAZAN;
import static mrusta.coffeeMapAndroid.managers.CoffeeDataLocalArray.Type.HISTORY;
import static mrusta.coffeeMapAndroid.managers.CoffeeDataLocalArray.Type.NEWS;

public class CoffeeDataLocalArray {

    public static final String MY_VARIABLES_KEY = "MyVariables";
    //----------------------------------------------------------------------------------------
    public static Context context;

    public static List<CoffeeNew> news      = new ArrayList<>();
    public static List<CoffeeNew> favorites = new ArrayList<>();
    public static List<CoffeeNew> historys  = new ArrayList<>();

    //----------------------------------------------------------------------------------------
    public static void setContext(Context context) {
        CoffeeDataLocalArray.context = context;

//        saveEmptyKey(FIREBASE);
//        saveEmptyKey(NEWS);
//        saveEmptyKey(FAVORITE);
//        saveEmptyKey(HISTORY);

        news      = loadType(NEWS);
        favorites = loadType(FAVORITE);
        historys  = loadType(HISTORY);
    }

    //----------------------------------------------------------------------------------------
    public enum Type {
        FIREBASE_KAZAN,
        FIREBASE_SPB,
        FIREBASE,
        NEWS,
        FAVORITE,
        HISTORY,
    }

    /* Search Address                                                                            */

    public static SearchAddress searchDataKey(CoffeeAddress address, Type type) {

        List<CoffeeNew> array = arrayAddressNewsDataType(type);

        int i = 0;
        for (CoffeeNew coffeeNew : array) {
            if (    coffeeNew.title. equals(address.coffee.title)) {
                if (coffeeNew.street.equals(address.street)) {
                    return new SearchAddress(true, i, address);
                }
            }
            i++;
        }
        return new SearchAddress(false, -1, address);
    }

    //----------------------------------------------------------------------------------------
    static class SearchAddress {
        boolean find;
        int index;
        CoffeeAddress address;

        public SearchAddress(boolean find, int index, CoffeeAddress address) {
            this.find = find; this.index = index; this.address = address;
        }
    }


    /* Save Array Empty Type                                                                      */

    public static void saveEmptyKey(Type type) {
        List<CoffeeNew> array = arrayAddressNewsDataType(type);
        array.clear();
        saveType(array, type);
    }


    /* Save Firebase Response                                                                     */

    public static void saveFirebaseResponse(final List<HashMap<String, String>> new_,
                                            final List<HashMap<String, String>> response,
                                            final List<HashMap<String, String>> spb,
                                            final List<HashMap<String, String>> kazan) {

        news = CoffeeNew.convertArray(new_);

        saveHashType(new_,     NEWS);
        saveHashType(response, FIREBASE);
        saveHashType(spb,      FIREBASE_SPB);
        saveHashType(kazan,    FIREBASE_KAZAN);

    }


    /* Save Array Type                                                                            */

    // предпологается что CoffeeAddress уже определено свойство
    public static void addAddressKey(CoffeeAddress address, Type type) {

        boolean                    selected = false;
        if      (type == FAVORITE) selected = address.favorite;
        else if (type == HISTORY)  selected = address.history;
        else if (type == NEWS)     selected = address.new_;

        SearchAddress search = searchDataKey(address, type);

        List<CoffeeNew> array = arrayAddressNewsDataType(type);

        if (selected) {             // если он выбран
            if (!search.find) {         // но не найден в списке
                array.add(new CoffeeNew(address)); // значит добавить
            }
        } else {                    // если он НЕ выбран
            if (search.find) {          // но найден в списке
                array.remove(search.index);         // значит удалить
            }
        }

        saveType(array, type);
    }


    //----------------------------------------------------------------------------------------
    public static List<CoffeeNew> arrayAddressNewsDataType(Type type) {

        if      (type == FAVORITE) return favorites;
        else if (type == HISTORY)  return historys;
        else if (type == NEWS)     return news;

        return new ArrayList<>();
    }

    //----------------------------------------------------------------------------------------
    public static void saveType(List<CoffeeNew> coffeeNews, Type type) {
        List<HashMap<String, String>> array = hashMapsToNewsType(coffeeNews);
        saveHashType(array, type);
    }

    //----------------------------------------------------------------------------------------
    public static void saveHashType(List<HashMap<String, String>> array, Type type) {

        String key = getKeyType(type);

        SharedPreferences pSharedPref = getSharedPreferences();
        if (pSharedPref != null) {
            JSONArray jsonArray  = new JSONArray(array);
            String jsonString = jsonArray.toString();
            SharedPreferences.Editor editor = pSharedPref.edit();
            editor.remove(key).apply();
            editor.putString(key, jsonString);
            editor.commit();
        }
    }
    //----------------------------------------------------------------------------------------
    public static List<HashMap<String, String>> hashMapsToNewsType(List<CoffeeNew> coffeeNews) {
        return CoffeeNew.saveArray(coffeeNews);
    }


    /* Load Array Type                                                                            */

    public static SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(MY_VARIABLES_KEY, Context.MODE_PRIVATE);
    }

    //----------------------------------------------------------------------------------------
    public static List<CoffeeNew> loadType(Type type) {
        ArrayList<HashMap<String, String>> resultNews = loadDataKey(type);
        return CoffeeNew.convertArray(resultNews);
    }

    //----------------------------------------------------------------------------------------
    public static ArrayList<HashMap<String, String>> loadDataKey(Type type){

        String key = getKeyType(type);

        ArrayList <HashMap<String, String>> outputMap = new ArrayList<>();

        SharedPreferences pSharedPref = getSharedPreferences();
        try {
            if (pSharedPref != null) {
                String jsonString = pSharedPref.getString(key, (new JSONArray()).toString());
                JSONArray jsonArray = new JSONArray(jsonString);

                for( int i=0; i < jsonArray.length(); i++ ) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    HashMap <String,String> hashMap = new HashMap<String,String>();

                    Iterator<String> keysItr = jsonObject.keys();
                    while (keysItr.hasNext()) {
                        String key_  = keysItr.next();
                        String value = (String) jsonObject.get(key_);
                        hashMap.put(key_, value);
                    }
                    outputMap.add(hashMap);
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return outputMap;
    }


    /* Load Coffees                                                                               */

    public static List<Coffee> loadCoffeesFirebaseLocal() {

        String key = getKeyType(Type.FIREBASE);

        List <Coffee> result = new ArrayList<>();

        SharedPreferences pSharedPref = getSharedPreferences();
        try {
            if (pSharedPref != null) {
                String jsonString = pSharedPref.getString(key, (new JSONArray()).toString());
                JSONArray jsonArray = new JSONArray(jsonString);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    Coffee coffee = new Coffee(jsonObject);
                    result.add(coffee);
                }
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        return result;
    }

    //----------------------------------------------------------------------------------------
    public static String getKeyType(Type type) {
        if      (type == FAVORITE) return "SaveJsonFavorites";
        else if (type == HISTORY ) return "SaveJsonHistory";
        else if (type == NEWS    ) return "SaveJsonNEWS";
        else if (type == FIREBASE) return "SaveJsonCoffees";
        else if (type == FIREBASE_SPB) return "SaveJsonCoffeesSpb";
        else if (type == FIREBASE_KAZAN) return "SaveJsonCoffeesKazan";
        return "";
    }
    //----------------------------------------------------------------------------------------

//    public static final String FIREBASE_KEY         = "SaveJsonCoffees";
//    public static final String NEWS_KEY             = "SaveJsonNEWS";
//    public static final String FAVORITE_KEY         = "SaveJsonFavorites";
//    public static final String HISTORY_KEY          = "SaveJsonHistory";
}
