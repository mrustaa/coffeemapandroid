package mrusta.coffeeMapAndroid.managers.mapLocationManager;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.UiSettings;

import mrusta.coffeeMapAndroid.managers.mapManager.CoffeeMapManagerCallback;

import static android.content.Context.LOCATION_SERVICE;

public class CoffeeAccessLocationManager {
    //----------------------------------------------------------------------------------------------
    // MAP
    public static AppCompatActivity activity;
    public static boolean permissionDenied; // доступ запрещен
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    //----------------------------------------------------------------------------------------------
    public static boolean gpsEnabled(AppCompatActivity activity) {
        LocationManager locationManager = (LocationManager) activity.getSystemService(LOCATION_SERVICE);
        return (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER));
    }

    //----------------------------------------------------------------------------------------------
    public static boolean accessLocationEnabled() {
        return (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }
    //----------------------------------------------------------------------------------------------
    // Разрешение на доступ к местоположению.
    public static void requestPermission(AppCompatActivity activity) {

        PermissionUtils.requestPermission(activity, LOCATION_PERMISSION_REQUEST_CODE,
                Manifest.permission.ACCESS_FINE_LOCATION, false);
    }
    //----------------------------------------------------------------------------------------------
    // результат разрешения доступа
    public static boolean onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] results) {

        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) return false;

        if (PermissionUtils.isPermissionGranted(permissions, results, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Включаем слой "Мое местоположение", если разрешение было предоставлено.
            return true;
        } else {
            // В разрешении было отказано. Показать сообщение об ошибке
            permissionDenied = true;
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    public static void onResumeFragments() {
        if (permissionDenied) {
            // Разрешение не предоставлено, отображается диалоговое окно с ошибкой.
            // showMissingPermissionError();
            permissionDenied = false;
        }
    }
    //----------------------------------------------------------------------------------------------
    // окно с ошибкой.
    private static void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(activity.getSupportFragmentManager(), "dialog");
    }
    //----------------------------------------------------------------------------------------------
}
