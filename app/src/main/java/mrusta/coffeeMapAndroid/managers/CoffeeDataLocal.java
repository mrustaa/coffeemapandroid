package mrusta.coffeeMapAndroid.managers;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import mrusta.coffeeMapAndroid.model.Coffee;
import mrusta.coffeeMapAndroid.model.CoffeeAddress;
import mrusta.coffeeMapAndroid.model.CoffeeNew;


public class CoffeeDataLocal {

    //public static List<Coffee>                       coffees   = new ArrayList<>();
    //public static ArrayList<HashMap<String, String>> news      = new ArrayList<>();

    public static final String MAPTYPE_KEY          = "SaveJsonMap";
    public static final String FILTER_KEY           = "SaveJsonFilter";
    public static final String FIRST_START_APP_KEY  = "SaveJsonFirstStartApp";
    public static final String COORD_LATITUDE_KEY   = "SaveJsonLatitude";
    public static final String COORD_LONGITUDE_KEY  = "SaveJsonLongitude";

    public static final String MY_VARIABLES_KEY = "MyVariablesNumber";

    public static Context context;

    public static void setContext(Context context) {
        CoffeeDataLocal.context = context;
    }

    //----------------------------------------------------------------------------------------------
    public enum DataType {
        FIRST_START_APP,
        MAPTYPE,
        FILTER,
    }

    //----------------------------------------------------------------------------------------
    public static void saveDataKeyMyLocation(LatLng location) {
        SharedPreferences sp = sharedPreferencesSaved();
        SharedPreferences.Editor editor = sp.edit();
        editor.putFloat(COORD_LATITUDE_KEY,  (float) location.latitude);
        editor.putFloat(COORD_LONGITUDE_KEY, (float) location.longitude);
        editor.commit();
    }
    //----------------------------------------------------------------------------------------
    public static LatLng loadDataKeyMyLocation(){
        SharedPreferences sp = sharedPreferencesSaved();
        float latitude  = sp.getFloat(COORD_LATITUDE_KEY, 0);
        float longitude = sp.getFloat(COORD_LONGITUDE_KEY, 0);
        if ((latitude == 0) && (longitude == 0)) { return null; }
        return new LatLng(latitude, longitude);
    }

    //----------------------------------------------------------------------------------------
    public static void saveDataKeyInt(int value, DataType type) {
        SharedPreferences sp = sharedPreferencesSaved();
        DataKeyClass dkc = new DataKeyClass(type);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(dkc.key, value);
        editor.commit();
    }
    //----------------------------------------------------------------------------------------
    public static int loadDataKeyInt(DataType type){
        SharedPreferences sp = sharedPreferencesSaved();
        DataKeyClass dkc = new DataKeyClass(type);
        int myIntValue = sp.getInt(dkc.key, 0);
        return myIntValue;
    }
    //----------------------------------------------------------------------------------------
    public static SharedPreferences sharedPreferencesSaved() {
        return context.getSharedPreferences(MY_VARIABLES_KEY, Context.MODE_PRIVATE);
    }

    //----------------------------------------------------------------------------------------
    private static class DataKeyClass {
        DataType type;
        String key;

        public DataKeyClass(DataType type) {
            this.type    = type;

            if      (type == DataType.MAPTYPE)         { key = MAPTYPE_KEY;         }
            else if (type == DataType.FILTER)          { key = FILTER_KEY;          }
            else if (type == DataType.FIRST_START_APP) { key = FIRST_START_APP_KEY; }
        }
    }
    //----------------------------------------------------------------------------------------
}
