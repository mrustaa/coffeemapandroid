package mrusta.coffeeMapAndroid.managers;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import static com.microsoft.appcenter.utils.HandlerUtils.runOnUiThread;

public class CoffeeDataFirebase {

    public interface Callback {

        //void compJson(final HashMap<String,ArrayList> value);

        void completion(final ArrayList<HashMap<String,String>> new_,
                        final ArrayList<HashMap<String,String>> response,
                        final ArrayList<HashMap<String,String>> spb,
                        final ArrayList<HashMap<String, String>> kazan);
    }

    //----------------------------------------------------------------------------------------------
    public enum DownloadType {
        New,
        Response,
    }
    //----------------------------------------------------------------------------------------------
    public static void downloadJSONFirebaseType(final Callback callback) {

        final boolean[] gotResult = new boolean[1];
        gotResult[0] = false;

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference reference = database.getReference();

//        String childName = "";
//        if        (type == DownloadType.New) {
//            childName = "new";
//        } else if (type == DownloadType.Response) {
//            childName = "response";
//        }
//
//        DatabaseReference ref = reference.child(childName);
        final ValueEventListener dataFetchEventListener = new ValueEventListener() {

            @Override public void onDataChange(DataSnapshot dataSnapshot) {
                gotResult[0] = true;

                if (dataSnapshot.exists()) {
//                    final ArrayList<HashMap<String,String>> value = (ArrayList <HashMap<String,String>>) dataSnapshot.getValue();

                    final HashMap<String,ArrayList> value = (HashMap<String, ArrayList>) dataSnapshot.getValue();

                    final ArrayList<HashMap<String,String>> new_     = (ArrayList <HashMap<String,String>>) value.get("new");
                    final ArrayList<HashMap<String,String>> response = (ArrayList <HashMap<String,String>>) value.get("response");
                    final ArrayList<HashMap<String,String>> spb      = (ArrayList <HashMap<String,String>>) value.get("response_spb");
                    final ArrayList<HashMap<String,String>> kazan      = (ArrayList <HashMap<String,String>>) value.get("response_kazan");


                    //runOnUiThread(new Runnable() { @Override public void run() {
                        callback.completion(new_, response, spb, kazan);
                        //callback.compJson(value);
                    //}});
                }
            }

            @Override public void onCancelled(DatabaseError error) {
                gotResult[0] = true;

                //callback.compJson(null);
                callback.completion(
                        new ArrayList<HashMap<String,String>>(),
                        new ArrayList<HashMap<String,String>>(),
                        new ArrayList<HashMap<String,String>>(),
                        new ArrayList<HashMap<String,String>>()
                );

                Log.w("downloadJSONFirebaseType", "Failed to read value.", error.toException());
            }
        };



            reference.addListenerForSingleValueEvent(dataFetchEventListener);

            final Timer timer = new Timer();
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    timer.cancel();

                    if (gotResult[0] == false) { //  Timeout
                        reference.removeEventListener(dataFetchEventListener);
                        // Your timeout code goes here

                        //callback.compJson(null);
                        callback.completion(
                                new ArrayList<HashMap<String,String>>(),
                                new ArrayList<HashMap<String,String>>(),
                                new ArrayList<HashMap<String,String>>(),
                                new ArrayList<HashMap<String,String>>()
                        );
                    }


                }
            };
            // Setting timeout of 10 sec to the request
            timer.schedule(timerTask, 10000L);

    }

    //----------------------------------------------------------------------------------------------
}
