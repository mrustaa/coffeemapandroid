package mrusta.coffeeMapAndroid.managers.mapManager;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import mrusta.coffeeMapAndroid.R;
import mrusta.coffeeMapAndroid.managers.CoffeeDataLocal;
import mrusta.coffeeMapAndroid.managers.CoffeeDataManagerNew;
import mrusta.coffeeMapAndroid.managers.mapLocationManager.CoffeeAccessLocationManager;
import mrusta.coffeeMapAndroid.managers.mapLocationManager.PermissionUtils;
import mrusta.coffeeMapAndroid.managers.utils.UtilsString;
import mrusta.coffeeMapAndroid.model.CoffeeAddress;

import static com.microsoft.appcenter.utils.HandlerUtils.runOnUiThread;
import static mrusta.coffeeMapAndroid.managers.CoffeeDataLocal.DataType.MAPTYPE;
import static mrusta.coffeeMapAndroid.managers.mapLocationManager.CoffeeAccessLocationManager.accessLocationEnabled;

public class CoffeeMapManager implements
        OnMapReadyCallback,
        GoogleMap.OnMyLocationChangeListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMapClickListener {
    //----------------------------------------------------------------------------------------------
    // MAP
    GoogleMap mMap;
    MapView mapView;

    static public LatLng myLocation;
    static public LatLng saveLastUpdatedMyLocation;

    static public LatLng moscow;
    static public LatLng spb;
    static public LatLng kazan;

    static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";

    ArrayList<Marker> allMarkers = new ArrayList<>();

    static boolean selectedMarkerMove;
    public Marker selectedMarker;
    ArrayList<Marker> selectedMarkers = new ArrayList<>();

    AppCompatActivity activity;
    boolean darkMode;

    public static boolean firstAccessMyLocation;
    //----------------------------------------------------------------------------------------------
    public static Context context;

    public static void setContext(Context context) {
        CoffeeMapManager.context = context;
    }
    //----------------------------------------------------------------------------------------------
    CoffeeMapManagerCallback callback;
    //----------------------------------------------------------------------------------------------
    public enum MarkerTypes {
        GREEN,
        GREEN_PIN,
        BLACK,
        BLACK_PIN,
        FAVORITE,
        NEWS,
        NEWS_PIN,
        FAVORITE_NEWS,
    }
    //----------------------------------------------------------------------------------------------
    public CoffeeMapManager(Bundle bundle, AppCompatActivity activity, MapView mapView, boolean darkMode, CoffeeMapManagerCallback callback) {
        this.activity = activity;
        this.mapView = mapView;
        this.darkMode = darkMode;
        this.callback = callback;

        loadLocations();

        if (bundle != null) {
            bundle.getBundle(MAPVIEW_BUNDLE_KEY);
        }

        mapView.onCreate(bundle);
        mapView.getMapAsync(this);
    }
    //----------------------------------------------------------------------------------------------
    // Загрузка кордиинат Москвы - и локации пользователя
    public static void loadLocations() {
        moscow = new LatLng(55.750572, 37.619534);
        spb    = new LatLng(59.925662, 30.355260);
        kazan  = new LatLng(55.794264, 49.119990);

        CoffeeAccessLocationManager.activity = (AppCompatActivity) context;
        if (accessLocationEnabled()) {
            myLocation = CoffeeDataLocal.loadDataKeyMyLocation();
            if (myLocation != null) {
                saveLastUpdatedMyLocation = new LatLng(myLocation.latitude, myLocation.longitude);
            }
        } else {
            myLocation = null;
        }
    }
    //----------------------------------------------------------------------------------------------
    // Получение GoogleMap
    @Override public void onMapReady(GoogleMap map) {
        mMap = map;

        callback.onMapReady(map);

        int mapType = CoffeeDataLocal.loadDataKeyInt(MAPTYPE);
        setMapType(mapType);

        if (darkMode) {
            MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(activity, R.raw.mapstyle_dark);
            mMap.setMapStyle(style);
        }

        mMap.setOnMyLocationChangeListener(this);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(this);

        updateMyLocation();

        startMapCameraPosition();
    }

    //----------------------------------------------------------------------------------------------
    // Включает "Мое местоположение", если предоставлено разрешение на точное определение местоположения.
    public void updateMyLocation() {

        // [START карты проверяют разрешение местоположения]
        int permission = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permission == PackageManager.PERMISSION_GRANTED) {

            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
            }
        } else {
            // Запрос на доступ геолокации пользователя
            CoffeeAccessLocationManager.requestPermission(activity);
        }

        if (mMap != null) {
            UiSettings settings = mMap.getUiSettings();
            settings.setMyLocationButtonEnabled(false);
            settings.setMapToolbarEnabled(false);
        }
    }
    //----------------------------------------------------------------------------------------------
    // Карта. Стартовая позиция камеры
    void startMapCameraPosition() {

        int city = CoffeeDataManagerNew.city;

        float zoomLevel = 10.0f;
        LatLng latLng = moscow;
        if (city == 1) {
            latLng = spb;
        } else if (city == 2) {
            latLng = kazan;
        }

        if (myLocation != null) {
            zoomLevel = 13.0f;
            latLng = myLocation;
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom( latLng, zoomLevel));
    }
    //----------------------------------------------------------------------------------------------
    // Карта. Обновление локации
    @Override public void onMyLocationChange(Location location) {                                   // Log.d("❤️ ", "GMap.MyLocation.Callback");

        saveLocalMyLocation(location);

        if (checkingNewLocationHasMoved(location)) {
            mapUpdateMyLocationCallback(true);
        }

        if (CoffeeDataManagerNew.trueRequestPermissionsResult) {        // (Одноразовая) Результат разрешения доступа геолокации
            CoffeeDataManagerNew.trueRequestPermissionsResult = false;
            mapUpdateMyLocationCallback(true);
        }

        if (firstAccessMyLocation) {
            firstAccessMyLocation = false;
            startMapCameraPosition();
        }
    }
    //----------------------------------------------------------------------------------------------
    // (Мое местоположение) Проверка отдаления"координаты пользователя" на 0.001000 по широте и долготе
    public boolean checkingNewLocationHasMoved(Location newLocation) {

        if (newLocation == null) return false;
        if (saveLastUpdatedMyLocation == null) return false;

        double myL1 = saveLastUpdatedMyLocation.latitude;
        double myL2 = saveLastUpdatedMyLocation.longitude;

        double dL1 = newLocation.getLatitude();
        double dL2 = newLocation.getLongitude();

        double r1 = checkingLocations(myL1, dL1);
        double r2 = checkingLocations(myL2, dL2);

        boolean result = ((0.001 < r1) || (0.001 < r2));

                                                                                                    // Log.d("\uD83C\uDD7F️️ ", String.format("checkingNewLocationHasMoved %s %f %f", result ? "✅" : "❌" , r1, r2 ) );

        return result;
    }
    //----------------------------------------------------------------------------------------------
    public double checkingLocations(double m, double l)  {
        double r = 0;
        if (m < l) {
            r = l - m;
        } else {
            r = m - l;
        }
        return r;
    }
    //----------------------------------------------------------------------------------------------
    // Сохранить в бд, последнюю локацию пользователя
    public void saveLocalMyLocation(Location location) {

        double dLatitude  = location.getLatitude();
        double dLongitude = location.getLongitude();
        myLocation = new LatLng(dLatitude, dLongitude);

                                                                                                    // Log.d("\uD83C\uDD7F️️ ", String.format("onMyLocationChange %s", UtilsString.strCoordinate(myLocation) ) );

        CoffeeDataLocal.saveDataKeyMyLocation(myLocation);
    }
    //----------------------------------------------------------------------------------------------
    // Таймер 100 секунд обновления локации
//    void startTimerUpdateMyLocation() {
//
//        int period = 100000;
//
//        if (timerUpdateLocation != null) timerUpdateLocation.cancel();
//
//        timerUpdateLocation = new Timer();
//        timerUpdateLocation.scheduleAtFixedRate(new TimerTask() {
//            @Override public void run() {
//
//                runOnUiThread(new Runnable() { @Override public void run() {                      Log.d("❤️ ", "mMap.setOnMyLocationChangeListener TIMER");
//
//                    mapUpdateMyLocationCallback(true);
//                }});
//            }
//        },period,period);
//    }
    //----------------------------------------------------------------------------------------------
    void mapUpdateMyLocationCallback(boolean timer) {

        if (!selectedMarkerMove && (callback != null)) {
            callback.mapUpdateMyLocation(timer);                                                    Log.d("\uD83C\uDD7F️️ ", "mapUpdateMyLocation" );

            if (myLocation != null) {
                saveLastUpdatedMyLocation = new LatLng(myLocation.latitude, myLocation.longitude);
            }
        }
    }
    //----------------------------------------------------------------------------------------------
    // Карта. Нажал на маркер
    @Override public boolean onMarkerClick(Marker marker) {
        mapSelectMarker(marker);
        return false;
    }
    //----------------------------------------------------------------------------------------------
    // Карта. Нажал на карту
    @Override public void onMapClick(LatLng latLng) {

        //hideSelectedMarker();
    }

    //----------------------------------------------------------------------------------------------
    // Карта. Типы
    public void setMapType(int index) {

        if (index == 0) {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        } else if (index == 1) {
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        } else if (index == 2) {
            mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        }
    }
    //----------------------------------------------------------------------------------------------
    // Кнопка. Переместить к локация пользователя
    public void moveMyLocation() {
        moveMyLocation(false);
    }

    public void moveMyLocation(boolean animation) {

        if (myLocation != null) {
            moveLocation(myLocation, 10.0f, animation);
            //mMap.animateCamera(CameraUpdateFactory.newLatLng(myLocation));
        }
    }
    //------------------------------------------------------------------
    public void moveLocation(LatLng location) { // 10.0f
        float zoom = mMap.getCameraPosition().zoom;
            moveLocation(location, zoom, false);
    }
    public void moveAnimationLocation(LatLng location) { // 10.0f
        float zoom = mMap.getCameraPosition().zoom;
        moveLocation(location, zoom, true);
    }

    public void moveLocation(LatLng location, float zoomLevel, boolean animation) { // 10.0f
        CameraUpdate camera = CameraUpdateFactory.newLatLngZoom( location, zoomLevel);
        if (!animation)  {
            mMap.moveCamera(camera);
        } else {
            mMap.animateCamera(camera);
        }

    }

    //----------------------------------------------------------------------------------------------
    public void mapSelectMarker(Marker marker) {

        selectedMarkerMove = true;

        mapDeselectMarkers();

        if (callback != null)
            callback.mapSelectMarker(marker);

        selectedMarkers = mapGetAllMarkersCoffeeAddress(marker);
        for (Marker oneMarker : selectedMarkers) {

            if (oneMarker != null) {
                if (oneMarker != marker) {
                    MarkerTypes type = MarkerTypes.GREEN;
                    oneMarker.setIcon( getMarkerIcon(type) );
                    oneMarker.setZIndex( getZIndexAddress(type) );
                }
            }
        }

        markerChangeIcon(marker, true);
        selectedMarker = marker;
    }

    //----------------------------------------------------------------------------------------------
    public void hideSelectedMarker() {

        selectedMarkerMove = false;

        callback.mapUnSelectMarker();
        mapDeselectMarkers();
    }
    //----------------------------------------------------------------------------------------------
    public void mapDeselectMarkers() {

        if (selectedMarker != null) {
            for (Marker marker : selectedMarkers) {
                if (marker != null) {
                    markerChangeIcon(marker, false);
                }
            }
        }
    }
    //----------------------------------------------------------------------------------------------
    public ArrayList<Marker> mapGetAllMarkersCoffeeAddress(Marker marker) {

        ArrayList<Marker> result = new ArrayList<>();

        CoffeeAddress address = (CoffeeAddress) marker.getTag();
        if (address == null) return result;

        for (CoffeeAddress oneAddress : address.coffee.address) {
            Marker oneMarker = mapSearchMarker(oneAddress);
            result.add(oneMarker);
        }
        return result;
    }
    //----------------------------------------------------------------------------------------------
    public Marker mapSearchMarker(CoffeeAddress address) {

        for (Marker marker : allMarkers) {
            CoffeeAddress addr = (CoffeeAddress) marker.getTag();
            if (addr != null) {
                if (addr.coffee.title.equals(address.coffee.title)) {
                    if (addr.street.equals(address.street)) {
                        return marker;
                    }
                }
            }
        }
        return null;
    }
    //----------------------------------------------------------------------------------------------
    public void mapClickMovePin(CoffeeAddress address) {

        Marker marker = mapSearchMarker(address);
        if (marker != null) mapSelectMarker(marker);

        mMap.animateCamera(CameraUpdateFactory.newLatLng(address.coord));
    }
    //----------------------------------------------------------------------------------------------
    public void addAllCoffeeMarkers(List<CoffeeAddress> filterCoffeeAddresses) {                    // 📍🗺
        if (filterCoffeeAddresses == null) return;
                                                                                                    Log.d("\uD83D\uDDFA️", String.format(" добавить пины на карту | addAllCoffeeMarkers %s", String.valueOf( filterCoffeeAddresses.size()))); //📍🗺
        hideSelectedMarker();

        for (Marker marker : allMarkers) {
            marker.remove();
        }
        allMarkers.clear();

        for (CoffeeAddress address : filterCoffeeAddresses) {
            LatLng cord = address.coord;

            if (address.new_ && address.favorite) {
                addMarkers(cord, MarkerTypes.FAVORITE_NEWS, address);
            } else if (address.new_) {
                addMarkers(cord, MarkerTypes.NEWS, address);
            } else if (address.favorite) {
                addMarkers(cord, MarkerTypes.FAVORITE, address);
            } else {
                addMarkers(cord, MarkerTypes.BLACK, address);
            }
        }
    }
    //----------------------------------------------------------------------------------------------
    public void addMarkers(LatLng coord, MarkerTypes type, CoffeeAddress coffeeAddress) {

        int zIndex = getZIndexAddress(type);

        if (mMap != null) {

            BitmapDescriptor icon = getMarkerIcon(type);

            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(coord)
                    .zIndex(zIndex)
                    .icon(icon)
                    .infoWindowAnchor(0.5f, 0.5f)
            );


            marker.setTag(coffeeAddress);

            allMarkers.add(marker);
        }
    }
    //----------------------------------------------------------------------------------------------
    public int getZIndexAddress(MarkerTypes type) {

        int zIndex = 0;
        if ((type == MarkerTypes.BLACK_PIN) || (type == MarkerTypes.GREEN_PIN) || (type == MarkerTypes.NEWS_PIN)) {
            zIndex = 3;
        } else if ((type == MarkerTypes.FAVORITE) || (type == MarkerTypes.FAVORITE_NEWS)) {
            zIndex = 1;
        } else if (type == MarkerTypes.GREEN) {
            zIndex = 2;
        }

        return zIndex;
    }
    //----------------------------------------------------------------------------------------------
    public BitmapDescriptor getMarkerIcon(MarkerTypes type) {

        int resource = 0;
        if        (type == MarkerTypes.GREEN) {
            resource = R.drawable.marker_coffee;
        } else if (type == MarkerTypes.GREEN_PIN) {
            resource = R.drawable.marker_coffee_pin;
        } else if (type == MarkerTypes.BLACK) {
            resource = R.drawable.marker_coffee_black;
        } else if (type == MarkerTypes.BLACK_PIN) {
            resource = R.drawable.marker_coffee_black_pin;
        } else if (type == MarkerTypes.FAVORITE) {
            resource = R.drawable.marker_coffee_favorite;
        } else if (type == MarkerTypes.NEWS) {
            resource = R.drawable.marker_coffee_new;
        } else if (type == MarkerTypes.NEWS_PIN) {
            resource = R.drawable.marker_coffee_new_pin;
        } else if (type == MarkerTypes.FAVORITE_NEWS) {
            resource = R.drawable.marker_coffee_favorite_new;
        }

//        int size = 50;
//        BitmapDrawable bitmapdraw = (BitmapDrawable) context.getResources().getDrawable(R.drawable.marker_coffee_black);
//        Bitmap b     = bitmapdraw.getBitmap();
//        Bitmap smallMarker = Bitmap.createScaledBitmap(b, size, size, false);
//        return BitmapDescriptorFactory.fromBitmap(smallMarker);

         return BitmapDescriptorFactory.fromResource(resource);
    }
    //----------------------------------------------------------------------------------------------
    public void markerChangeIcon(@NonNull Marker marker, boolean pin) {

        if (marker.getTag() == null) {

            if (selectedMarker != null) selectedMarker.remove();
            selectedMarker = null;
            return;
        }

        CoffeeAddress address = (CoffeeAddress) marker.getTag();

        MarkerTypes type = MarkerTypes.BLACK;

        if (pin) {

            if (address.new_ && address.favorite) {
                type = MarkerTypes.NEWS_PIN;
            } else if (address.new_) {
                type = MarkerTypes.NEWS_PIN;
            } else if (address.favorite) {
                type = MarkerTypes.BLACK_PIN;
            } else {
                type = MarkerTypes.BLACK_PIN;
            }

        } else {

            if (address.new_ && address.favorite) {
                type = MarkerTypes.FAVORITE_NEWS;
            } else if (address.new_) {
                type = MarkerTypes.NEWS;
            } else if (address.favorite) {
                type = MarkerTypes.FAVORITE;
            }
        }
        marker.setIcon( getMarkerIcon(type) );
        marker.setZIndex( getZIndexAddress(type) );
    }
    //----------------------------------------------------------------------------------------------
}
