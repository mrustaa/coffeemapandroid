package mrusta.coffeeMapAndroid.managers.mapManager;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;


public interface CoffeeMapManagerCallback {

    void onMapReady(GoogleMap map);

    void mapUpdateMyLocation(boolean timer);

    void mapSelectMarker(Marker marker);
    void mapUnSelectMarker();

}
