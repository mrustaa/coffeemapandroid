package mrusta.coffeeMapAndroid.managers;


import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import mrusta.coffeeMapAndroid.model.CoffeeAddress;

import static mrusta.coffeeMapAndroid.managers.utils.UtilsString.findReplaceText;
import static mrusta.coffeeMapAndroid.managers.utils.UtilsString.findText;
import static mrusta.coffeeMapAndroid.managers.utils.UtilsString.removingSpaces;

public class CoffeeDateFormater {

    //----------------------------------------------------------------------------------------
    public static String timeSeparatedWeekDays(String text,
                                               String d1, boolean s1,
                                               String d2, boolean s2,
                                               String d3, boolean s3,
                                               String d4, boolean s4) {
        return timeSeparatedWeekDays(text,
                d1, s1,
                d2, s2,
                d3, s3,
                d4, s4,
                "",false,
                "",false,
                "",false,
                "",false);
    }
    public static String timeSeparatedWeekDays(String text,
                                               String d1, boolean s1,
                                               String d2, boolean s2,
                                               String d3, boolean s3,
                                               String d4, boolean s4,
                                               String d5, boolean s5,
                                               String d6, boolean s6,
                                               String d7, boolean s7) {
        return timeSeparatedWeekDays(text,
                d1, s1,
                d2, s2,
                d3, s3,
                d4, s4,
                d5, s5,
                d6, s6,
                d7, s7,
                "", false);
    }

    public static String timeSeparatedWeekDays(String text,
                                               String d1, boolean s1,
                                               String d2, boolean s2,
                                               String d3, boolean s3,
                                               String d4, boolean s4,
                                               String d5, boolean s5,
                                               String d6, boolean s6,
                                               String d7, boolean s7,
                                               String d8, boolean s8) {
        return timeSeparatedWeekDays(text,
                d1, s1,
                d2, s2,
                d3, s3,
                d4, s4,
                d5, s5,
                d6, s6,
                d7, s7,
                d8, s8,
                "", false);
    }

    public static String timeSeparatedWeekDays(String text,
                                               String d1, boolean s1,
                                               String d2, boolean s2,
                                               String d3, boolean s3,
                                               String d4, boolean s4,
                                               String d5, boolean s5,
                                               String d6, boolean s6,
                                               String d7, boolean s7,
                                               String d8, boolean s8,
                                               String d9, boolean s9) {
        return timeSeparatedWeekDays(text,
                d1, s1,
                d2, s2,
                d3, s3,
                d4, s4,
                d5, s5,
                d6, s6,
                d7, s7,
                d8, s8,
                d9, s9,
                "", false);
    }

    public static String timeSeparatedWeekDays(String text,
                                               String d1, boolean s1,
                                               String d2, boolean s2,
                                               String d3, boolean s3,
                                               String d4, boolean s4,
                                               String d5, boolean s5,
                                               String d6, boolean s6,
                                               String d7, boolean s7,
                                               String d8, boolean s8,
                                               String d9, boolean s9,
                                               String d10, boolean s10) {
        return timeSeparatedWeekDays(text,
                d1, s1,
                d2, s2,
                d3, s3,
                d4, s4,
                d5, s5,
                d6, s6,
                d7, s7,
                d8, s8,
                d9, s9,
                d10, s10,
                "", false);
    }

    public static String timeSeparatedWeekDays(String text,
                                               String d1, boolean s1,
                                               String d2, boolean s2,
                                               String d3, boolean s3,
                                               String d4, boolean s4,
                                               String d5, boolean s5,
                                               String d6, boolean s6,
                                               String d7, boolean s7,
                                               String d8, boolean s8,
                                               String d9, boolean s9,
                                               String d10, boolean s10,
                                               String d11, boolean s11) {
        return timeSeparatedWeekDays(text,
                d1, s1,
                d2, s2,
                d3, s3,
                d4, s4,
                d5, s5,
                d6, s6,
                d7, s7,
                d8, s8,
                d9, s9,
                d10, s10,
                d11, s11,
                "", false);
    }

    public static String timeSeparatedWeekDays(String text,
                                               String d1, boolean s1,
                                               String d2, boolean s2,
                                               String d3, boolean s3,
                                               String d4, boolean s4,
                                               String d5, boolean s5,
                                               String d6, boolean s6,
                                               String d7, boolean s7,
                                               String d8, boolean s8,
                                               String d9, boolean s9,
                                               String d10, boolean s10,
                                               String d11, boolean s11,
                                               String d12, boolean s12) {
        return timeSeparatedWeekDays(text,
                d1, s1,
                d2, s2,
                d3, s3,
                d4, s4,
                d5, s5 ,
                d6, s6,
                d7, s7,
                d8, s8,
                d9, s9,
                d10, s10,
                d11, s11,
                d12, s12,
                "", false);
    }

    public static String timeSeparatedWeekDays(String text,
                                               String d1, boolean s1,
                                               String d2, boolean s2,
                                               String d3, boolean s3,
                                               String d4, boolean s4,
                                               String d5, boolean s5,
                                               String d6, boolean s6,
                                               String d7, boolean s7,
                                               String d8, boolean s8,
                                               String d9, boolean s9,
                                               String d10, boolean s10,
                                               String d11, boolean s11,
                                               String d12, boolean s12,
                                               String d13, boolean s13) {
        return timeSeparatedWeekDays(text,
                d1, s1,
                d2, s2,
                d3, s3,
                d4, s4,
                d5, s5,
                d6, s6,
                d7, s7,
                d8, s8,
                d9, s9,
                d10, s10,
                d11, s11,
                d12, s12,
                d13, s13,
                "", false);
    }

    public static String timeSeparatedWeekDays(String text,
                                               String d1, boolean s1,
                                               String d2, boolean s2,
                                               String d3, boolean s3,
                                               String d4, boolean s4,
                                               String d5, boolean s5,
                                               String d6, boolean s6,
                                               String d7, boolean s7,
                                               String d8, boolean s8,
                                               String d9, boolean s9,
                                               String d10, boolean s10,
                                               String d11, boolean s11,
                                               String d12, boolean s12,
                                               String d13, boolean s13,
                                               String d14, boolean s14) {
        if (! d1.isEmpty()) { text = timeSeparated(text,  d1, s1); }
        if (! d2.isEmpty()) { text = timeSeparated(text,  d2, s2); }
        if (! d3.isEmpty()) { text = timeSeparated(text,  d3, s3); }
        if (! d4.isEmpty()) { text = timeSeparated(text,  d4, s4); }
        if (! d5.isEmpty()) { text = timeSeparated(text,  d5, s5); }
        if (! d6.isEmpty()) { text = timeSeparated(text,  d6, s6); }
        if (! d7.isEmpty()) { text = timeSeparated(text,  d7, s7); }
        if (! d8.isEmpty()) { text = timeSeparated(text,  d8, s8); }
        if (! d9.isEmpty()) { text = timeSeparated(text,  d9, s9); }
        if (!d10.isEmpty()) { text = timeSeparated(text, d10, s10); }
        if (!d11.isEmpty()) { text = timeSeparated(text, d11, s11); }
        if (!d12.isEmpty()) { text = timeSeparated(text, d12, s12); }
        if (!d13.isEmpty()) { text = timeSeparated(text, d13, s13); }
        if (!d14.isEmpty()) { text = timeSeparated(text, d14, s14); }
        return text;
    }
    //----------------------------------------------------------------------------------------
    public static String timeSeparated(String text, String date, boolean separated) {

        if (text.split(date).length == 2) {

            String result = (text.split(date)[1]);
            if (separated) {
                result = (result.split(",")[0]);
            }
            return result;
        }
        return text;
    }

    //----------------------------------------------------------------------------------------
    public static Date dateParseStr(String strDate) {

        // Locale RUSSIAN = new Locale("ru","RU");
        SimpleDateFormat dateFormatterStr3 = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.ENGLISH);

        Date date = new Date();
        try {
            date = dateFormatterStr3.parse(strDate);;
        } catch (ParseException e) {
            e.printStackTrace();
        }

//        Calendar cal = Calendar.getInstance();
//        cal.setTime(date);
//        cal.add(Calendar.DATE, -2);
//        Date dateResult = cal.getTime();

        return date;
    }
    //----------------------------------------------------------------------------------------------
    public static Date curDate() {
//        int day = 3;
//        final long millisToAdd = ((60 * 60 * 1000) * 24 * day); // 24 hours
        Date curDate = new Date();
//        curDate.setTime(curDate.getTime() + millisToAdd);
        return curDate;
    }

    //----------------------------------------------------------------------------------------------
    public static String todayDayWeekTime(CoffeeAddress address) {
        return todayDayWeekTime(address, false);
    }

    public static String todayDayWeekTime(CoffeeAddress address, boolean clear) {

        String time   = address.time;

        String street = address.street;
        String title  = address.coffee.title;

//        Calendar cal = Calendar.getInstance();
//        cal.setTime(new Date());
//        cal.add(Calendar.DATE, -1);
//        Date dateBefore30Days = cal.getTime();


        if (address.street.equals("Ул. Новодмитровская, д. 2, корп. 1"))  {
            Log.d("❤️ ", "");
        }

        Calendar calendar = Calendar.getInstance();
        TimeZone tz = TimeZone.getDefault();

        calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE", Locale.ENGLISH); // Locale.getDefault()

        Date currentDate = curDate();
        String todayDayWeek = sdf.format(currentDate);


        if (findText( time,"-")) {
            time = findReplaceText(time, "-", "–");
        }

        /// ПН
        /// КОСТЫЛИ =3 - ГОВНОКОД - ХАРДКОД
        if (findText( time,"10–")) { /// PROSVET ✅
            time = findReplaceText(time, "10–", "10:00–");
        }
        if (findText( time,".00")) { /// Wake Up Cafe  ✅       Ул. Покровка, 40, стр.1
            time = findReplaceText(time, ".00", ":00"); //  .
        }
        if (street.equals("Ул. Земляной вал, 38-40/15с9"))  { // Молния ✅
            time = "ПН–ПТ: 8:00–21:00, СБ–ВС: 9:00–21:00"; //  ;
        }
        if (street.equals("Ул. Земляной вал, 14/16"))  { // Молния ✅
            time = "ПН–ПТ: 8:00–21:00, СБ–ВС: 9:00–21:00";  //  ;
        }
        if (street.equals("пр. Аэропорта, 8, стр. 9 (этаж 1)"))  { // Surf Coffee ✅
            time = "ПН–ПТ: 7:30–22:00, СБ–ВС: 9:00–22:00";  //  ;
        }

        if (street.equals("Краснобогатырская улица, 90 строение 2"))  { // Profitrolly ✅
            //time = "ПН–ПТ: 8:00–23:00, СБ–ВС: 9:00–23:00";
            time = time.toUpperCase();
        }
        if (street.equals("Якиманская набережная, 2"))  { // Блэк by Даблби ✅
            // Пн–Ср: 8:00–23:00, Чт: 8:00–01:00, Пт: 8:00–6:00, Сб: 10:00–6:00, Вс: 10:00–23:00
            time = time.toUpperCase();
        }


        if (street.equals("Ул. Новодмитровская, д. 2, корп. 1"))  { // 8/25 кофейня ✅
            time = "ПН–ПТ: закрыто, СБ–ВС: 10:00–19:00";         //   нет ПН–ПТ:      СБ–ВС: 10:00–19:00
        }


        if (street.equals("Проспект Вернадского, 86В, фуд-корт Eat Market"))  { // Pa Pa Power ✅
            time = "ПН–ЧТ: 10:00–22:00, ПТ–СБ: 10:00–01:00, ВС: 10:00–22:00"; // ПН–ЧТ, ВС              ПН–ЧТ, ВС: 10:00–22:00, ПТ–СБ: 10:00–01:00
        }
        if (street.equals("Новодмитровская улица 1 строение 9, на территории городского пространства Хлебозавода N9")) { // Melt me ✅
            time = "ПН–ВТ: 12:00–20:00, СР: 12:00–21:00, ЧТ–СБ: 12:00–22:00, ВС: 12:00–20:00"; // ПН–ВТ, ВС         ВС–ВТ: 12:00–20:00, СР: 12:00–21:00, ЧТ–СБ: 12:00–22:00
        }
        if (street.equals("Ул. Лесная, 20, фудмолл \"Депо\", место 78"))  { // Raw to go  ✅
            time = "ПН–ЧТ: 10:00–23:00, ПТ–СБ: 10:00–23:59, ВС: 10:00–23:00"; // ПН–ЧТ, ВС          ПН–ЧТ, ВС: 10:00 – 23:00, ПТ–СБ: 10:00–23:59
        }



        if (street.equals("Ул. Варварка, 3, Гостиный Двор, вход между 16-17 подъездами"))  { // LOBBY
            time = "ПН–СР: 10:00–21:00, ЧТ–СБ: 10:00–23:00, ВС: 10:00–21:00"; // ВС–СР: 10:00 – 21:00, ЧТ–СБ: 10:00–23:00
        }
        if (street.equals("Малый Сухаревский пер., 9с1"))  { // Блэк Милк
            time = "ПН–ЧТ: 10:00–23:00, ПТ–СБ: 10:00–23:59, ВС: 10:00–23:00"; // ВС–ЧТ:
        }
        if (street.equals("Малый Гнездниковский переулок, 9с1"))  { // Ровесник
            time = "ПН–ЧТ: 9:00–01:00, ПТ–СБ: 9:00–06:00, ВС: 9:00–01:00"; // ВС–ЧТ:
        }
        if (street.equals("Ул. Палиха, 14/33 строение 2"))  { // Сверстник
            time = "ПН–ЧТ: 9:00–23:59, ПТ–СБ: 9:00–02:00, ВС: 9:00–23:59"; // ВС–ЧТ:
        }

        // ВТ
        if (street.equals("Чистопрудный бульвар, 23, библиотека Ф.М. Достоевского")) { // Грамотный кофе
            time = "ПН: закрыто, ВТ–СБ: 8:00–22:00, ВС: 10:00–20:00"; // ВТ–СБ:       ВТ–СБ: 8:00–22:00, ВС: 10:00–20:00, ПН: закрыто
        }
        if (street.equals("Страстной бульвар, 8, Библиотека им. А.П. Чехова")) { // Грамотный кофе
            time = "ПН: закрыто, ВТ–СБ: 8:00–22:00, ВС: 10:00–20:00"; // ВТ–СБ:            ВТ–СБ: 8:00–22:00, ВС: 10:00–20:00, ПН: закрыто
        }

        if (street.equals("Ул. Петровка 25, строение 1")) { // Книгикофекниги
            time = "ПН: 12:00–20:00, ВТ–ВС: 12:00–21:00"; // ВТ–ВС:      ВТ–ВС: 12:00–21:00, ПН: 12:00–20:00
        }

//        // ЧТ
//        if (street.equals("Ул. Новодмитровская, д. 2, корп. 7")) {
//            time = "ПН–ПТ 8:00–21:00, СБ–ВС 9:00–21:00";
//        }

        // ПТ
        if (street.equals("Ул. Варварка, 3, Гостиный Двор, вход между 16-17 подъездами")) { // LOBBY
            time = "ПН: закрыто, ВС–СР: 10:00 – 21:00, ЧТ–СБ: 10:00–23:00, ВС: закрыто"; // ВС–СР: 10:00 – 21:00, ЧТ–СБ: 10:00–23:00
        }
        if (street.equals("Новодмитровская улица 1 строение 9, на территории городского пространства Хлебозавода N9")) { // Melt me
            time = "ПН: закрыто, ВС–ВТ: 12:00–20:00, СР: 12:00–21:00, ЧТ–СБ: 12:00–22:00, ВС: закрыто"; // ВС–ВТ: 12:00–20:00, СР: 12:00–21:00, ЧТ–СБ: 12:00–22:00
        }

        if (street.equals("Большой Сухаревский переулок, 4"))  {
            time = "ПН–ПТ: 8:00 – 21:00, СБ–ВС: 10:00 – 21:00";
        }


        String time_ = "";

        if (todayDayWeek.equals("Monday")) { // ПН

            time_ = timeSeparatedWeekDays(time,
                    "ПН: "   , true,
                    "ПН–ВТ: ", true,
                    "ПН–СР: ", true,
                    "ПН–ЧТ: ", true,
                    "ПН–ПТ: ", true,
                    "ПН–СБ: ", true,
                    "ПН–ВС: ",      false
            );

        } else if (todayDayWeek.equals("Tuesday")) { // ВТ

            time_ = timeSeparatedWeekDays(time,
                    "ПН: "   , true,
                    "ПН–ВТ: ", true,
                    "ПН–СР: ", true,
                    "ПН–ЧТ: ", true,
                    "ПН–ПТ: ", true,
                    "ПН–СБ: ", true,
                    "ПН–ВС: ",      false,
                    ", ВТ–ПТ: ", true,
                    ", ВТ–СБ: ", true,
                   ", ВТ–ВС: ",   false
            );

        } else if (todayDayWeek.equals("Wednesday")) { // СР

            time_ = timeSeparatedWeekDays(time,
                    "ПН: "   , true,
                    "ПН–ВТ: ", true,
                    "ПН–СР: ", true,
                    "ПН–ЧТ: ", true,
                    "ПН–ПТ: ", true,
                    "ПН–СБ: ", true,
                    "ПН–ВС: ",         false,
                    ", ВТ–ПТ: ", true,
                    ", ВТ–СБ: ", true,
                   ", СР–ЧТ: ", true );

        } else if (todayDayWeek.equals("Thursday")) { // ЧТ

            time_ = timeSeparatedWeekDays(time,
                    "ПН: "   , true,
                    "ПН–ВТ: ", true,
                    "ПН–СР: ", true,
                    "ПН–ЧТ: ", true,
                    "ПН–ПТ: ", true,
                    "ПН–СБ: ", true,
                    "ПН–ВС: ",      false,
                    ", ВТ–ПТ: ", true,
                    ", ВТ–СБ: ", true,
                   ", СР–ЧТ: ",   false );

        } else if (todayDayWeek.equals("Friday")) { // ПТ

            time_ = timeSeparatedWeekDays(time,
                    "ПН: "   , true,
                    "ПН–ВТ: ", true,
                    "ПН–СР: ", true,
                    "ПН–ЧТ: ", true,
                    "ПН–ПТ: ", true,
                    "ПН–СБ: ", true,
                    "ПН–ВС: ",      false,
                    ", ВТ–ПТ: ", true,
                    ", ВТ–СБ: ", true,
                    ", ПТ–СБ: ", true,
                    ", ПТ–ВС: ", true,
                    ", ПТ: ", true);

        } else if (todayDayWeek.equals("Saturday")) { // СБ

            time_ = timeSeparatedWeekDays(time,
                    "ПН: "   , true,
                    "ПН–ПТ: ", true,
                    "ПН–СБ: ", true,
                    "ПН–ВС: ",      false,
                    ", ВТ–СБ: ", true,
                    ", ПТ–СБ: ", true,
                    ", ПТ–ВС: ", true,
                    ", СБ–ВС: ",     false,
                    ", СБ: ", true);

        } else if (todayDayWeek.equals("Sunday")) { // ВС

            time_ = timeSeparatedWeekDays(time,
                    "ПН: "   , true,
                    "ПН–ПТ: ", true,
                    "ПН–СБ: ", true,
                    "ПН–ВС: ",       false,
                    ", ПТ–ВС: ", true,
                    ", СБ–ВС: ",        false,
                    ", ВС: ",       false);
        }
        //--------------------------------------------

        if (time_.equals("24 часа")) {
            return " · Круглосуточно";
        } else if (time_.equals("закрыто")) {
            return " · Выходной";
        }

        //--------------------------------------------
//        String text = String.format("%s|%s|%s|", title, street, time_);
//        Log.d("❤️ ", text);

        String[] timeArr = time_.split("–");

        if (timeArr.length < 2) {
            return "";
        }

        String strTimeStart = timeArr[0];
        String strTimeEnd   = timeArr[1];

//        SimpleDateFormat dateFormatterStr2 = new SimpleDateFormat("dd.MM.yyyy",          Locale.ENGLISH);
//        SimpleDateFormat dateFormatterStr3 = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.ENGLISH);

        SimpleDateFormat dateFormatterStr2 = new SimpleDateFormat("dd.MM.yyyy",          Locale.ENGLISH);
        SimpleDateFormat dateFormatterStr3 = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.ENGLISH);

        strTimeStart = removingSpaces(strTimeStart);
        strTimeEnd   = removingSpaces(strTimeEnd);

        String strTimeStartCreateDate = String.format("%s %s:00", dateFormatterStr2.format(curDate()), strTimeStart );

        String strTimeEndCreateDate   = String.format("%s %s:00", dateFormatterStr2.format(curDate()), strTimeEnd );

        // final long millisToAdd = ((60 * 60 * 1000) * 3); // 3 hours

//        String tag  = String.format("\uD83E\uDD4F %s", address.coffee.title);
//        String text = String.format("%s %s", strTimeStartCreateDate, strTimeEndCreateDate);
//        Log.d(tag, text);


//        if (Utils.findText(strTimeStartCreateDate, "00 :00")) {
//            Log.d("❤️ ", "");
//        }
//        if (Utils.findText(strTimeEndCreateDate, "00 :00")) {
//            Log.d("❤️ ", "");
//        }


        Date  startDate = dateParseStr(strTimeStartCreateDate);
        // startDate.setTime(startDate.getTime() + millisToAdd);

        Date  endDate = dateParseStr(strTimeEndCreateDate);
        // endDate.setTime(endDate.getTime() + millisToAdd);

        final long millisToAdd = -((60 * 60 * 1000) * 24); // 3 hours

        Date curDate = curDate();
        // curDate.setTime(curDate.getTime() + millisToAdd);

        long strN = startDate.getTime();
        long endN =   endDate.getTime();
        long curN =   curDate.getTime();

        String format = "";

        if (strN < curN) {
            if (endN < curN) {

                format = String.format("%s %s", "Откроется в", strTimeStart);

            } else {

                if (strTimeEnd.equals("23:59")) {
                    strTimeEnd = "12 часов ночи";
                }
                format = String.format("%s %s", "Работает до", strTimeEnd);
            }
        } else {
            format = String.format("%s %s", "Откроется в", strTimeStart);
        }

        String result = (clear) ? format : String.format(" · %s", format);


//        if (address.coffee.title.equals("8/25 кофейня"))  {
//            Log.d("❤️ ", "");
//        }
//        if (address.street.equals("Ул. Новодмитровская, д. 2, корп. 1"))  {
//            Log.d("❤️ ", "");
//        }



//        if (address.coffee.title.equals("6 am bread kitchen")) {
//
//
//            Log.d("0❤️ ", time);
//            Log.d("1❤️ ", time_);
//
//            Log.d("2\uD83E\uDDE9️ ", todayDayWeek);
//
//            Log.d("3❤️ ", strTimeStart);
//            Log.d("4❤️ ", strTimeEnd);
//
//            Log.d("5❤️ ", strTimeStartCreateDate);
//            Log.d("6❤️ ", strTimeEndCreateDate);
//
//
//            String curDat = dateFormatterStr3.format(currentDate);
//            Log.d("-❤️ ", curDat);
//            String startDat = dateFormatterStr3.format(startDate);
//            Log.d("7\uD83E\uDDA0️ ", startDat);
//            String endDat = dateFormatterStr3.format(endDate);
//            Log.d("8\uD83E\uDDA0️ ", endDat);
//
//            Log.d("9\uD83E\uDDA0️ ", result);
//
//            Log.d("️ ", "");
//        }

        return result;
        // java.util.Date currenTimeZone = new java.util.Date((long)1379487711 * 1000);
    }
    //----------------------------------------------------------------------------------------


}
