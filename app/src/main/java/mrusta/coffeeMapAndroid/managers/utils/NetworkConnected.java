package mrusta.coffeeMapAndroid.managers.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import static android.net.ConnectivityManager.TYPE_WIFI;
import static android.net.NetworkInfo.State.CONNECTED;
import static android.provider.ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE;

public class NetworkConnected {

    //----------------------------------------------------------------------------------------------
    public static boolean check(Context context) {

        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (activeNetwork != null) { // connected to the internet
            switch (activeNetwork.getType()) {
                case ConnectivityManager.TYPE_WIFI:   break; // connected to wifi
                case ConnectivityManager.TYPE_MOBILE: break; // connected to mobile data
                default:                              break;
            }
            return true;
        } else {                     // not connected to the internet
            return false;
        }
    }

}
