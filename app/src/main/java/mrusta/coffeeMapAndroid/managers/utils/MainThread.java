package mrusta.coffeeMapAndroid.managers.utils;

import static com.microsoft.appcenter.utils.HandlerUtils.runOnUiThread;

public class MainThread {
    //----------------------------------------------------------------------------------------------
    public static void main(final Runnable callback) {
        main(0, callback);
    }
    //----------------------------------------------------------------------------------------------
    public static void main(final int delay, final Runnable callback) {
        new Thread(new Runnable() { @Override public void run() {
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            runOnUiThread(callback);
            // synchronized (this) { ... }
            // getMainLooper
        }}).start();
    }
    //----------------------------------------------------------------------------------------------
    public static void delay(final int delay, final Runnable runnable) {

        new Thread(new Runnable() { @Override public void run() {
            sleep(delay);
            main(runnable);
        }}).start();
    }
    //----------------------------------------------------------------------------------------------
    public static void sleep(final int delay) {
        try { Thread.sleep(delay); } catch (InterruptedException e) { e.printStackTrace(); }
    }
    //----------------------------------------------------------------------------------------------
}
