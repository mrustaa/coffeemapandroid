package mrusta.coffeeMapAndroid.managers.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.Toast;

import mrusta.coffeeMapAndroid.R;

public class UtilsAlert {

    //----------------------------------------------------------------------------------------------

    public static void alertToastLong(Activity activity, String text) {
        Toast.makeText(activity, text, Toast.LENGTH_LONG).show();
    }
    //----------------------------------------------------------------------------------------------
    public static void alertToastShort(Activity activity, String text) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show();
    }

    //----------------------------------------------------------------------------------------------
    public static void alertMessage(Activity activity, String message) {
        alertTitleMessage(activity, null, message, 0, null);
    }
    //----------------------------------------------------------------------------------------------
    public static void alertTitleMessage(Activity activity, String title, String message, int iconId, DialogInterface.OnClickListener okCallback) {

        AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
        if (title   != null) alertDialog.setTitle(title);
        if (message != null) alertDialog.setMessage(message);
        if (iconId  != 0)    alertDialog.setIcon(iconId);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", okCallback);

        alertDialog.show();
    }
    //----------------------------------------------------------------------------------------------
}
