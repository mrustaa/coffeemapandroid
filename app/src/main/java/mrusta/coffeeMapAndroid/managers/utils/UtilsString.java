package mrusta.coffeeMapAndroid.managers.utils;

import android.app.Activity;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class UtilsString {

    //----------------------------------------------------------------------------------------------
    // Удалить все пробелы
    public static String removingSpaces(String text) {
        //return text.replace(" ", "");
        return  text.replaceAll("\\s+", "");
    }

    // Удалить текст
    public static String removingText(String text, String remove) {
        return  text.replaceAll(remove, "");
    }
    //----------------------------------------------------------------------------------------------
    // Найти текст
    public static boolean findText(String text, String find) {
        return (text.toLowerCase().contains(find.toLowerCase()));
        // return (text.toLowerCase().indexOf(find.toLowerCase()) > -1);
        // return (text.indexOf(find) != -1);
    }
    //----------------------------------------------------------------------------------------------
    // Найти заменит
    public static String findReplaceText(String text, String find, String replace) {
        return text.replace(find, replace);
    }
    //----------------------------------------------------------------------------------------------
    public static String decimalFormat(double num) {
        DecimalFormatSymbols decimalSymbols = DecimalFormatSymbols.getInstance();
        decimalSymbols.setDecimalSeparator('.');
        DecimalFormat decForm = new DecimalFormat("0.000000", decimalSymbols);
        return decForm.format(num);
    }
    //----------------------------------------------------------------------------------------------
    public static String strCoordinate(LatLng coordinate) {
        return String.format("%f %f", coordinate.latitude, coordinate.longitude);
    }
}
