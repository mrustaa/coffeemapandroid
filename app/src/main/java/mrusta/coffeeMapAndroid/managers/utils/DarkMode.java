package mrusta.coffeeMapAndroid.managers.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;

import mrusta.coffeeMapAndroid.R;

import static android.content.res.Configuration.UI_MODE_NIGHT_NO;
import static android.content.res.Configuration.UI_MODE_NIGHT_YES;

public class DarkMode {
    //----------------------------------------------------------------------------------------------
    public static boolean context(Context context) {

        int currentNightMode = context.getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
        if (currentNightMode == UI_MODE_NIGHT_NO) {
            return false;
        } else if (currentNightMode == UI_MODE_NIGHT_YES) {
            return true;
        }
        return false;
    }

    /* Загрузка темы                                                                              */

    public static boolean updateSetTheme(Activity activity) {
        boolean dark = DarkMode.context(activity);
        if (dark) {
            activity.setTheme(R.style.Dark);
        } else {
            activity.setTheme(R.style.Light);
        }
        return dark;
    }
}
