package mrusta.coffeeMapAndroid.managers.utils;

import android.view.View;
import android.view.animation.AlphaAnimation;

import java.util.ArrayList;
import java.util.List;

import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import mrusta.coffeeMapAndroid.ui.cell.coffeeList.CoffeeMarketTagCell;

public class ViewAlphaAnimation {
    //----------------------------------------------------------------------------------------------
    public static void change(View view, boolean show, int duration) {

        if ((view.getVisibility() == View.VISIBLE) && show) {
            return;
        } else if ((view.getVisibility() == View.GONE) && !show) {
            return;
        }

        AlphaAnimation animation = new AlphaAnimation((show ? 0 : 1), (show ? 1 : 0));
        animation.setDuration(duration);
        view.startAnimation(animation);

        if (show) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }
    }

    public static void changes(List<View> views, boolean show, int duration) {

        for (int i = 0; i < views.size(); i++) {
            View item = (View) views.get(i);
            change(item, show, duration);
        }
    }

    //----------------------------------------------------------------------------------------------

}
