package mrusta.coffeeMapAndroid.managers.utils;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.View;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.dynamicanimation.animation.SpringAnimation;
import androidx.dynamicanimation.animation.SpringForce;

public class ScreenSize {

    public static void setMargins(Context context, View view, int left, int top, int right, int bottom) {

        ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT
        );

        if ((left == 0) && (top == 0) && (right == 0) && (bottom == 0)) {

            params = new ConstraintLayout.LayoutParams(
                    ConstraintLayout.LayoutParams.WRAP_CONTENT,
                    ConstraintLayout.LayoutParams.WRAP_CONTENT
            );
        }

        float d = context.getResources().getDisplayMetrics().density;

        params.setMargins((int) (left * d), (int) (top * d), (int) (right * d), (int) (bottom * d));
        view.setLayoutParams(params);
    }

    public static void animationFrameY(View target, int duration, boolean springAnimations, float movePosition) {

        if (springAnimations) {

            SpringAnimation springAnim = new SpringAnimation(target, SpringAnimation.TRANSLATION_Y);
            SpringForce springForce = new SpringForce();
            springForce.setFinalPosition(movePosition);
            springForce.setStiffness(350f);     // SpringForce.STIFFNESS_LOW
            springForce.setDampingRatio(0.55f); // SpringForce.DAMPING_RATIO_MEDIUM_BOUNCY
            springAnim.setSpring(springForce);
            springAnim.start();
        } else {

            String propertyName = "translationY";
            ObjectAnimator animation = ObjectAnimator.ofFloat(target, propertyName, movePosition);
            animation.setDuration(duration);
            animation.start();
        }
    }
}


//    float d = getResources().getDisplayMetrics().density;
//    float gribPadding = 16;
//    float nav = 104;
//    float y = (nav * d);
////        animationFrameY(recyclerView, 0, false, (y * -1));


//
//        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
//        Display display = wm.getDefaultDisplay();
//        float dpWidth   = (float) display.getWidth();
//        float dpHeight  = (float) display.getHeight();
//        float pxWidth     = dpWidth * d;
//        float pxHeight    = dpHeight * d;
//
//        ViewGroup.LayoutParams prm = recyclerView.getLayoutParams();
//        //layoutParams.height =  height - (int) px;
//
//        float pxX  = recyclerView.getX();
//        float pxY  = recyclerView.getY();
//
//
//        float dpX  = pxX / d;
//        float dpY  = pxY / d;
//
//        String text = String.format(" dp (x:%s|y:%s) (width:%s|height:%s) (width:%s|height:%s)  density:%s", dpX, dpY, dpWidth, dpHeight, prm.width, prm.height, d );
//        Log.d("\uD83E\uDD8B\uD83E\uDD84\uD83E\uDDE9️ ", text);
//
//
//        String textd = String.format(" px (x:%s|y:%s) (width:%s|height:%s)", pxX , pxY , pxWidth , pxHeight );
//        Log.d("\uD83E\uDD8B\uD83E\uDD84\uD83E\uDDE9️ ", textd);
//
//
//        ViewGroup.LayoutParams prmam = recyclerView.getLayoutParams();
//        prmam.height = (int) (dpHeight - (y / d));
//        recyclerView.setLayoutParams(prmam);
