package mrusta.coffeeMapAndroid.managers;

import android.os.Build;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class WebViewHtmlCode {

    private WebView webview;
    private String instaURL;
    private WebViewHtmlCode.Script.HtmlCodeCallback htmlCodeCallback;

    //----------------------------------------------------------------------------------------------
    public WebViewHtmlCode(WebView web, String insta, WebViewHtmlCode.Script.HtmlCodeCallback callback) {
        webview = web;
        instaURL = insta;
        htmlCodeCallback = callback;



        webview.getSettings().setJavaScriptEnabled(true);

        webview.setVisibility(View.GONE);

//        webview.clearHistory();
//        webview.clearCache(true);
//        webview.getSettings().setBuiltInZoomControls(true);
//        webview.getSettings().setCacheMode(2);

        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setSupportZoom(true);
        webview.getSettings().setUseWideViewPort(false);
        webview.getSettings().setLoadWithOverviewMode(false);

        webview.addJavascriptInterface(new WebViewHtmlCode.Script(htmlCodeCallback), "HTMLOUT");

        webview.loadUrl(instaURL);

        webview.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("source://")) {
                    try {
                        String html = URLDecoder.decode(url, "UTF-8").substring(9);
                        //sourceReceived(html);
                        //Log.d("❤️❤️️❤❤️❤️️❤️", html);
                    } catch (UnsupportedEncodingException e) {
                        Log.e("example", "failed to decode source", e);
                    }
                    view.getSettings().setJavaScriptEnabled(false);
                    return true;
                }
                // For all other links, let the WebView do it's normal thing
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url)
            {
                webview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
                //webview.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
            }
        });

        webview.evaluateJavascript("(function(){return window.document.body.outerHTML})();",
                new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String html) {
                        //Log.d("❤️❤️️❤❤️❤️️❤️", html);
                    }
                });
    }

    public static class Script
    {
        public interface HtmlCodeCallback {
            void htmlCodeCallback(String html);
        }

        public interface CloseCallback {
            void close();
        }

        static HtmlCodeCallback callback;
        static CloseCallback closeCallback;

        public Script(HtmlCodeCallback callback) {
            Script.callback = callback;
        }

        @JavascriptInterface
        @SuppressWarnings("unused")
        public void processHTML(String html)
        {
            Script.callback.htmlCodeCallback(html);

            //Log.d("❤️❤️️❤️", html);
            // process the html as needed by the app
        }
    }
}
