package mrusta.coffeeMapAndroid.managers;

import android.app.Activity;
import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mrusta.coffeeMapAndroid.framework.recyclerAdapter.Interface.RecyclerAdapterCell;
import mrusta.coffeeMapAndroid.framework.recyclerAdapter.RecyclerAdapterClick;
import mrusta.coffeeMapAndroid.managers.instaDownload.HttpManagerCompletedClose;
import mrusta.coffeeMapAndroid.managers.mapManager.CoffeeMapManager;
import mrusta.coffeeMapAndroid.managers.utils.NetworkConnected;
import mrusta.coffeeMapAndroid.model.Coffee;
import mrusta.coffeeMapAndroid.model.CoffeeAddress;
import mrusta.coffeeMapAndroid.model.CoffeeIndex;

import mrusta.coffeeMapAndroid.ui.cell.coffeeList.CoffeeCell;
import mrusta.coffeeMapAndroid.ui.screen.list.CoffeeListActivity;

import static com.microsoft.appcenter.utils.HandlerUtils.runOnUiThread;

public class CoffeeDataManagerNew {


    public static void webViewCloseCallback() {
    }

    //----------------------------------------------------------------------------------------------
    public interface LoadingLocalCallback {
        void loadingLocalCallback(List<CoffeeAddress> result);
    }

    public interface DownloadFirebaseCallback {
        void downloadFirebaseCallback(List<CoffeeAddress> result);
    }

    public interface FilteredCallback {
        void filteredCallback(List<CoffeeAddress> result);
    }

    public interface DetailsActivityBackCallback {
        void detailsActivityBackCallback(CoffeeAddress address);
    }

    //----------------------------------------------------------------------------------------------
    public static int city = 0;

    public static String searchText = "";

    public static String pushToken = "";

    public static boolean pushNotification;
    public static boolean updateRefresh;

    // public static List<CoffeeNew>     news                  = new ArrayList<>();
    // public static List<CoffeeNew>     favorites             = new ArrayList<>();
    // public static List<CoffeeNew>     historys              = new ArrayList<>();

    public static List<Coffee>              coffees = new ArrayList<>();
    public static List<CoffeeAddress>       coffeeAddresses = new ArrayList<>();
    public static List<CoffeeAddress> filterCoffeeAddresses = new ArrayList<>();

    public static List<Coffee>              coffeeSpb = new ArrayList<>();
    public static List<CoffeeAddress>       coffeeSpbAddress = new ArrayList<>();

    public static List<Coffee>              coffeeKazan = new ArrayList<>();
    public static List<CoffeeAddress>       coffeeKazanAddress = new ArrayList<>();

    public static        LoadingLocalCallback          loadingLocalCallback;
    public static    DownloadFirebaseCallback      downloadFirebaseCallback;
    public static            FilteredCallback              filteredCallback;
    public static DetailsActivityBackCallback   detailsActivityBackCallback;

    public static String webViewInstaURL;
    public static TestWebViewActivity.ImagesUrlCallback testWebViewImagesURLsCallback;
    public static HttpManagerCompletedClose webViewCloseCallback;

    public static int filterSelected; // USER SELECTED FILTER
    public static Context context;

    public static boolean        loadLocalProcess;
    public static boolean downloadFirebaseProcess;
    public static boolean           filterProcess;

    public static boolean      emptyLoadLocalFirst;     // Если нет сохранненной-локально базы кофешек
    public static boolean endDownloadFirebaseFirst;     // Обновление данных через Firebase - закончилось
    public static boolean trueRequestPermissionsResult; // (Одноразовая) Результат разрешения доступа геолокации

    //----------------------------------------------------------------------------------------------
    public static String[] filterString() {
        return new String[]{
                "Открыто сейчас",
                "New",
                "Избранные",
                "Альтернатива",
                "Veg",
                "Декаф",
                "Продажа зерна",
                "Dog friendly",            /// 8
                "Specialty cacao",         /// 9
                "Просмотрено"
        };
    }

    //----------------------------------------------------------------------------------------------
    public static void setContext(Context context) {
        CoffeeDataManagerNew.context  = context;
        CoffeeDataLocal     .setContext(context);
        CoffeeDataLocalArray.setContext(context);
        CoffeeMapManager    .setContext(context);
    }
    public static void setLoadingLocalCallback(LoadingLocalCallback callback) {
        CoffeeDataManagerNew.loadingLocalCallback = callback;
    }
    public static void setDownloadFirebaseCallback(DownloadFirebaseCallback callback) {
        CoffeeDataManagerNew.downloadFirebaseCallback = callback;
    }
    public static void setFilteredCallback(FilteredCallback callback) {
        CoffeeDataManagerNew.filteredCallback = callback;
    }
    public static void setDetailsActivityBackCallback(DetailsActivityBackCallback callback) {
        CoffeeDataManagerNew.detailsActivityBackCallback = callback;
    }
    //----------------------------------------------------------------------------------------------
    public static void loadingCallback(List<CoffeeAddress> result) {
        if (CoffeeDataManagerNew.loadingLocalCallback != null)
            CoffeeDataManagerNew.loadingLocalCallback.loadingLocalCallback(result);
    }
    public static void downloadCallback(List<CoffeeAddress> result) {
        if (CoffeeDataManagerNew.downloadFirebaseCallback != null)
            CoffeeDataManagerNew.downloadFirebaseCallback.downloadFirebaseCallback(result);
    }
    public static void filteredCallback(List<CoffeeAddress> result) {
        if (CoffeeDataManagerNew.filteredCallback != null)
            CoffeeDataManagerNew.filteredCallback.filteredCallback(result);
    }
    //----------------------------------------------------------------------------------------------
    public static void loadCoffee() {

        // filterSelected = 0;// CoffeeDataLocal.loadDataKeyInt(FILTER);

        loadLocal(new Runnable() { @Override public void run() {

                downloadFirebase(new Runnable() { @Override public void run() {

                }});
        }});
    }
    //----------------------------------------------------------------------------------------------
    public static void updateDownloadCoffeeFirebase() {

        downloadFirebase(new Runnable() { @Override public void run() {

        }});

    }
    //----------------------------------------------------------------------------------------------
    public static void updateDownloadCoffeeFirebaseTask() {

        // CoffeeDataManagerNew.updateRefresh = true;

        CoffeeDataFirebase.downloadJSONFirebaseType(new CoffeeDataFirebase.Callback() {

//            @Override public void compJson(HashMap<String, ArrayList> value) {
//
//            }

            @Override public void completion(final ArrayList<HashMap<String, String>> new_,
                                             final ArrayList<HashMap<String, String>> response,
                                             final ArrayList<HashMap<String, String>> spb,
                                             final ArrayList<HashMap<String, String>> kazan) {

                CoffeeDataManagerDownloadTask task = new CoffeeDataManagerDownloadTask(city, new CoffeeDataManagerDownloadTask.Callback() {

                    @Override public void callback(List<CoffeeAddress> result) {
                        CoffeeDataManagerNew.filterCoffeeAddresses = result;
                        downloadCallback(result);
                    }
                });

                ArrayList<ArrayList<HashMap<String, String>>> a = new ArrayList<>();
                a.add(new_);
                a.add(response);
                a.add(spb);
                a.add(kazan);
                task.execute( a );

            }
        });

    }
    //----------------------------------------------------------------------------------------------
    public static void loadLocal(final Runnable completion) {

        if (CoffeeDataManagerNew.loadLocalProcess) return;

        if (CoffeeDataLocalArray.getSharedPreferences() == null) {

            CoffeeDataManagerNew.emptyLoadLocalFirst = true;

            List<CoffeeAddress> array = new ArrayList<>();
            loadingCallback( array );

            if (completion != null) completion.run();
            return;
        }

        CoffeeDataManagerNew.updateRefresh = false;
        CoffeeDataManagerNew.loadLocalProcess = true;

        new Thread(new Runnable() { @Override public void run() {

            final List<Coffee>        coffees = CoffeeDataLocalArray.loadCoffeesFirebaseLocal();
            final List<CoffeeAddress> coffeeAddresses = createListAddresses(coffees);

//            CoffeeDataLocalArray.setContext(context);
            CoffeeDataLocalArray.context = context;

//            final List<CoffeeNew> news      = CoffeeDataLocal.loadType(NEWS);
//            final List<CoffeeNew> favorites = CoffeeDataLocal.loadType(FAVORITE);
//            final List<CoffeeNew> historys  = CoffeeDataLocal.loadType(HISTORY);

            CoffeeMapManager.loadLocations();

            runOnUiThread(new Runnable() { @Override public void run() {

                CoffeeDataManagerNew.coffees = coffees;
//                CoffeeDataManagerNew.news      = news;
//                CoffeeDataManagerNew.favorites = favorites;
//                CoffeeDataManagerNew.historys  = historys;
            }});

            final List<CoffeeAddress> filteredResult = CoffeeDataManagerFilter.sortCoffeeAddressDistance(
                    coffeeAddresses,
                    CoffeeDataLocalArray.news,
                    CoffeeDataLocalArray.favorites,
                    CoffeeDataLocalArray.historys,
                    searchText,
                    CoffeeMapManager.myLocation,
                    filterSelected
            );

            final List<CoffeeAddress> result = filterCoffeesCopy(filteredResult);

            runOnUiThread(new Runnable() { @Override public void run() {

                CoffeeDataManagerNew.loadLocalProcess = false;
                CoffeeDataManagerNew.filterCoffeeAddresses = filteredResult;

                loadingCallback(result);

                if (completion != null) completion.run();
            }});

        }}).start();
    }

    //----------------------------------------------------------------------------------------------
    // Firebase update
    public static void downloadFirebase(final Runnable completion) {

        new Thread(new Runnable() { @Override public void run() {

            if (CoffeeDataManagerNew.downloadFirebaseProcess) return;

            if (!NetworkConnected.check(context)) {

                CoffeeDataManagerNew.endDownloadFirebaseFirst = true;

                List<CoffeeAddress> array = new ArrayList<>();
                downloadCallback(array);

                if (completion != null) completion.run();
                return;
            }

            CoffeeDataManagerNew.updateRefresh = false;

            CoffeeDataFirebase.downloadJSONFirebaseType(new CoffeeDataFirebase.Callback() {

                @Override public void completion(final ArrayList<HashMap<String, String>> new_,
                                                 final ArrayList<HashMap<String, String>> response,
                                                 final ArrayList<HashMap<String, String>> spb,
                                                 final ArrayList<HashMap<String, String>> kazan) {

                    CoffeeDataManagerNew.downloadFirebaseProcess = true;

                    CoffeeDataLocalArray.saveFirebaseResponse(new_, response, spb, kazan);

                    runOnUiThread(new Runnable() { @Override public void run() {

                        // news         = CoffeeNew.convertArray(new_);

                        coffees = Coffee.convertArray(response);
                        coffeeAddresses = createListAddresses(CoffeeDataManagerNew.coffees);

                        coffeeSpb = Coffee.   convertArray(spb);
                        coffeeSpbAddress = createListAddresses(CoffeeDataManagerNew.coffeeSpb);

                        coffeeKazan = Coffee.   convertArray(kazan);
                        coffeeKazanAddress = createListAddresses(CoffeeDataManagerNew.coffeeKazan);

                    }});

                    List<CoffeeAddress> addresses = CoffeeDataManagerNew.coffeeAddresses;
                    if (city == 1) {
                        addresses = CoffeeDataManagerNew.coffeeSpbAddress;
                    } else if (city == 2) {
                        addresses = CoffeeDataManagerNew.coffeeKazanAddress;
                    }

                    final List<CoffeeAddress> filteredResult = CoffeeDataManagerFilter.sortCoffeeAddressDistance(
                            addresses,
                            CoffeeDataLocalArray.news,
                            CoffeeDataLocalArray.favorites,
                            CoffeeDataLocalArray.historys,
                            searchText,
                            CoffeeMapManager.myLocation,
                            filterSelected
                    );

                    final List<CoffeeAddress> result = filterCoffeesCopy(filteredResult);

                    runOnUiThread(new Runnable() { @Override public void run() {

                        CoffeeDataManagerNew.downloadFirebaseProcess = false;
                        CoffeeDataManagerNew.filterCoffeeAddresses = filteredResult;

                        CoffeeDataManagerNew.endDownloadFirebaseFirst = true;

                        downloadCallback(result);

                        if (completion != null) completion.run();
                    }});
                }
            });

        }}).start();


    }
    //----------------------------------------------------------------------------------------------
    public static void sortCoffeeAddressDistance(final int filterIndex) {

        if (CoffeeDataManagerNew.filterProcess) return;

        CoffeeDataManagerNew.updateRefresh = false;
        CoffeeDataManagerNew.filterSelected = filterIndex;

        new Thread(new Runnable() { @Override public void run() {

            CoffeeDataManagerNew.filterProcess = true;

            List<CoffeeAddress> addresses = CoffeeDataManagerNew.coffeeAddresses;
            if (city == 1) {
                addresses = CoffeeDataManagerNew.coffeeSpbAddress;
            } else if (city == 2) {
                addresses = CoffeeDataManagerNew.coffeeKazanAddress;
            }

            final List<CoffeeAddress> filteredResult = CoffeeDataManagerFilter.sortCoffeeAddressDistance(
                    addresses,
                    searchText,
                    CoffeeMapManager.myLocation,
                    filterIndex
            );

            final List<CoffeeAddress> result = filterCoffeesCopy(filteredResult);

            runOnUiThread(new Runnable() { @Override public void run() {

                CoffeeDataManagerNew.filterProcess = false;
                CoffeeDataManagerNew.filterCoffeeAddresses = filteredResult;

                filteredCallback(result);

            }});

        }}).start();

    }

    //----------------------------------------------------------------------------------------------
    // Создать массив адрессов
    public static List<CoffeeAddress> createListAddresses(List<Coffee> coffees) {

        List<CoffeeAddress> result = new ArrayList<>();
        for (Coffee coffee : coffees) {
            result.addAll(coffee.address);
        }
        return result;
    }


    //----------------------------------------------------------------------------------------------
    public static void updateItemsCell(final CoffeeListActivity activity, final CoffeeListActivity.UpdateItemsCallback callback) {

        new Thread(new Runnable() { @Override public void run() {

            final List<RecyclerAdapterCell> items = new ArrayList<>();

            for (CoffeeAddress address : filterCoffeeAddresses) {
                items.add(new CoffeeCell(address, (RecyclerAdapterClick) activity));
            }

            runOnUiThread(new Runnable() { @Override public void run() {
                if (callback != null) callback.updatedItemsResult(items);
            }});

        }}).start();
    }

    //----------------------------------------------------------------------------------------------
    // клонирование - отфильтрованного массива
    public static List <CoffeeAddress> filterCoffeesCopy() {
        return filterCoffeesCopy(filterCoffeeAddresses);
    }

    public static List <CoffeeAddress> filterCoffeesCopy(List <CoffeeAddress> original) {
        return new ArrayList<>(original);
    }
//        List <CoffeeAddress> result = new ArrayList<>();
//        for (CoffeeAddress addr : original) {
//            result.add(addr);
//        }
//        return result;

    //----------------------------------------------------------------------------------------------
//    public static CoffeeAddress getRealAddressToOldAddress(CoffeeAddress address) {
//        for (CoffeeAddress address1 : coffeeAddresses) {
//            if (    address1.coffee.title.  equals(address.coffee.title)) {
//                if (address1.street.        equals(address.street)) {
//                    return address1;
//                }
//            }
//        }
//        return null;
//    }
    public static List<Coffee> getCoffeesCity() {
        List<Coffee> coffees = CoffeeDataManagerNew.coffees;
        if (CoffeeDataManagerNew.city == 1) {
            coffees = CoffeeDataManagerNew.coffeeSpb;
        } else if (CoffeeDataManagerNew.city == 2) {
            coffees = CoffeeDataManagerNew.coffeeKazan;
        }
        return coffees;
    }
    //----------------------------------------------------------------------------------------------
    public static CoffeeAddress getAddressToIndex(CoffeeIndex index) {
        return getCoffeesCity().get(index.coffee).address.get(index.address);
    }
    //----------------------------------------------------------------------------------------------
    public static CoffeeIndex getIndexToAddress(CoffeeAddress sel_address) {

        List<Coffee> coffees = getCoffeesCity();
        int coffeeIndex = 0, addressIndex = 0;
        for (int i=0; i < coffees.size(); i++ ) {
            Coffee coffee = coffees.get(i);
            if (coffee.title.equals( sel_address.coffee.title )) {
                for (int y=0; y < coffee.address.size(); y++ ) {
                    CoffeeAddress address = coffee.address.get(y);
                    if (sel_address.street.equals( address.street )) {
                        coffeeIndex = i; addressIndex = y;
                    }
                }
            }
        }
        return new CoffeeIndex(coffeeIndex, addressIndex);
    }
    //----------------------------------------------------------------------------------------------
}

