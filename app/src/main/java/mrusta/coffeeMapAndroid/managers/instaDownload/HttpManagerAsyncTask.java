package mrusta.coffeeMapAndroid.managers.instaDownload;


import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


class HttpManagerAsyncTask extends AsyncTask<String, Void, String> {

    String urlHttpCode;
    private HttpManagerCompleted delegate;

    public HttpManagerAsyncTask(HttpManagerCompleted delegate) {
        this.delegate = delegate;
    }

    @Override protected String doInBackground(String... urls) {

        for (String url : urls) {

            try {
                return request(url);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return "";
    }

    String request(String url) throws IOException {

        StringBuilder builder = new StringBuilder(100000);

        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        HttpResponse execute = client.execute(httpGet);


        // execute = client.execute(httpGet);

        InputStream content = execute.getEntity().getContent();

        BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
        String s = "";
        while ((s = buffer.readLine()) != null) {
            builder.append(s);
        }

        return builder.toString();
    }

//    @Override protected void onProgressUpdate(Integer... values) {
//        super.onProgressUpdate(values);
//    }

    @Override protected void onPostExecute(String result) {
        this.urlHttpCode = result;
        delegate.onTaskCompleted(result);
    }
}



