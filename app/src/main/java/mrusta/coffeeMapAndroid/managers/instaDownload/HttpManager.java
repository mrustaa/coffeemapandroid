package mrusta.coffeeMapAndroid.managers.instaDownload;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.View;

// import com.google.android.gms.common.api.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import mrusta.coffeeMapAndroid.managers.CoffeeDataManagerNew;
import static mrusta.coffeeMapAndroid.managers.utils.MainThread.main;


public class HttpManager implements HttpManagerCompleted {

    public String urlHtmlCode;
    public List<String> instaURLs = new ArrayList<>();
    private HttpManagerCompleted delegate;
    public HttpManagerCompletedClose delegateClose;
    private String instaURL;

    public HttpManager(HttpManagerCompleted delegate) {
        this.delegate = delegate;
    }

    public void htmlCodeURL(String strURL) {

        Log.d("❤️ ", strURL);

        HttpManagerAsyncTask task = new HttpManagerAsyncTask(this);
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, strURL);
        } else {
            task.execute(strURL);
        }

    }

    public void onTaskCompleted(String htmlCode) {
                Log.d("\uD83D\uDFE0\uD83D\uDFE7\uD83E\uDDE1️️", String.format("jonTaskCompleted %s", htmlCode) );

        urlHtmlCode = htmlCode;
        getInstaURLs();
        delegate.onTaskCompleted(htmlCode);
    }

    public void setWebViewHtmlCode(final String htmlCode) {
        Log.d("\uD83D\uDFE0\uD83D\uDFE7\uD83E\uDDE1️️", String.format("setWebViewHtmlCode %s", htmlCode) );
        instaURL = CoffeeDataManagerNew.webViewInstaURL;
        urlHtmlCode = htmlCode;
        getInstaURLs();
        delegate.onTaskCompleted(htmlCode);
    }

    public void downloadImageURL(final String strURL) {


        new Thread(){
            public void run() {
                try {

                    Bitmap bitmap = BitmapFactory.decodeStream(new URL(strURL).openStream());
//                    Message msg = new Message();
//                    msg.obj = bitmap;
//                    imageHandler.sendMessage(msg);
                    Log.d("","");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();

    }

    public void getInstaURLs() {

        Log.d("\uD83D\uDFE0\uD83D\uDFE7\uD83E\uDDE1️️", String.format("getInstaURLs %s", "") );

        List<String> arr = new ArrayList<>();

        String[] find = urlHtmlCode.split("<script type=\"text/javascript\">window._sharedData = ");
        if (find.length == 2) {

            for (int count = 1; count < find.length; count++) {
                 String span = find[count].split(";</script>")[0];
                 arr.add(span);
            }
        }

        JSONObject jsonRoot = null;
        JSONObject entry_data = null;
        JSONArray edges = null;

        instaURLs = new ArrayList<>();

        if (arr.size() == 1) {
            String instaPhotos = arr.get(0);

            try {
                jsonRoot = new JSONObject(instaPhotos);

                Log.d("❤️❤️️❤❤️❤️️❤❤️❤️️❤❤️❤️️", jsonRoot.toString());

                entry_data = jsonRoot.getJSONObject("entry_data");
                JSONArray ProfilePage = entry_data.getJSONArray("ProfilePage");

                JSONObject Page = ProfilePage.getJSONObject(0);
                JSONObject graphql = Page.getJSONObject("graphql");
                JSONObject user = graphql.getJSONObject("user");
                JSONObject edge_owner = user.getJSONObject("edge_owner_to_timeline_media");

                edges = edge_owner.getJSONArray("edges");

                if (edges.length() != 0) {

                    Log.d("❤️❤️️❤❤️❤️️❤❤️❤️️❤❤️❤️️", edges.toString());

                    for (int i = 0; i < edges.length(); i++) {
                        JSONObject obj = edges.getJSONObject(i);
                        JSONObject node = obj.getJSONObject("node");
                        String src = node.getString("thumbnail_src");
                        instaURLs.add(src);
                    }

//                    if (delegateClose != null) {
//                        Log.d("\uD83D\uDE21\uD83D\uDD95\uD83D\uDDE3 \uD83E\uDD0F\uD83C\uDFFB \uD83D\uDCAD️️", "feedbackImgURLs");
//
//                        main(new Runnable() { @Override public void run() {
//                            delegateClose.feedbackImgURLs(instaURLs);
//                        }});
//                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            try {
                jsonRoot = new JSONObject(instaPhotos);

                entry_data = jsonRoot.getJSONObject("entry_data");
                JSONArray FeedPage = entry_data.getJSONArray("FeedPage");

                Log.d("❤️❤️️❤❤️❤️️❤❤️❤️️❤❤️❤️️", "FeedPage");
                //JSONObject FPage = ProfilePage.getJSONObject(0);
                if (delegateClose != null) {

                    //delegateClose.feedbackClose();
                    main(new Runnable() { @Override public void run() {
                        delegateClose.feedbackClose();
                    }});

                }

                htmlCodeURL(instaURL);

                main(new Runnable() { @Override public void run() {
                    htmlCodeURL(instaURL);
                }});


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Log.d("❤", "");
    }

}

