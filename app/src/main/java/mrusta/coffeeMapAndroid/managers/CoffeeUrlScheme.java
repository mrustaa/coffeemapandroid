package mrusta.coffeeMapAndroid.managers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import mrusta.coffeeMapAndroid.managers.utils.UtilsString;
import mrusta.coffeeMapAndroid.model.CoffeeAddress;

import static mrusta.coffeeMapAndroid.managers.utils.UtilsString.decimalFormat;

public class CoffeeUrlScheme {

    //----------------------------------------------------------------------------------------------
    public static void shareInfo(Activity a, CoffeeAddress coffeeAddress) {

        LatLng l1 = coffeeAddress.coord;
        String coord = String.format("%s, %s", decimalFormat(l1.latitude), decimalFormat(l1.longitude));

        String sendText = String.format("** %s **\n\n%s\n%s\n\n%s\n%s\n\nCappuccino %d\nEspresso %d\n\n%s\n\n%s\n\n%s"
                ,coffeeAddress.coffee.title
                ,coffeeAddress.coffee.content
                ,coffeeAddress.coffee.stuff
                ,coffeeAddress.coffee.url
                ,coffeeAddress.coffee.insta
                ,coffeeAddress.coffee.cappuccino
                ,coffeeAddress.coffee.espresso
                ,coffeeAddress.street
                ,coffeeAddress.time
                ,coord);

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] { sendText });
        intent.putExtra(Intent.EXTRA_SUBJECT, sendText);
        a.startActivity(Intent.createChooser(intent, sendText));
    }
    //----------------------------------------------------------------------------------------------
    public static void email(Activity a, String email, String text) {

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        String parse = String.format("mailto:%s", email);
        emailIntent.setData(Uri.parse(parse));

        String title = String.format("%s %s", text, email);
        a.startActivity(Intent.createChooser(emailIntent, title));

//        Intent i = new Intent(Intent.ACTION_SEND);
//        i.setType("message/rfc822");
//        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{email});
//        i.putExtra(Intent.EXTRA_SUBJECT, "Тема");
//        i.putExtra(Intent.EXTRA_TEXT   , "");
//        try {
//            a.startActivity(Intent.createChooser(i, "Написать в почту..."));
//        } catch (android.content.ActivityNotFoundException ex) {
//        }
    }
    //----------------------------------------------------------------------------------------------
    public static void mapRouteAlertMessage(Activity a, LatLng l1, LatLng l2) {

        // "geo:0,0?q=1600+Amphitheatre+Parkway,+Mountain+View,+California"
        // "geo:37.422219,-122.08364?z=14"

        String coord = String.format("geo:%s,%s?q=%s,%s",
                decimalFormat(l2.latitude), decimalFormat(l2.longitude),
                decimalFormat(l1.latitude), decimalFormat(l1.longitude));

        Uri location = Uri.parse(coord);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);
        a.startActivity(Intent.createChooser(mapIntent, "Выберите карту"));
    }
    //----------------------------------------------------------------------------------------------
//    public static void showAlertSheet(Activity a) {
//        AlertDialog.Builder adb = new AlertDialog.Builder(a);
//
////        adb.setTitle("R.string.exit"); // заголовок
////        adb.setMessage("R.string.save_data"); // сообщение
//        adb.setIcon(android.R.drawable.ic_dialog_info); // иконка
//
//        // кнопка положительного ответа
//        adb.setPositiveButton("Google Maps", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//            }
//        });
//
//        // кнопка отрицательного ответа
//        adb.setNegativeButton("Яндекс Карты", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//            }
//        });
//
//        // adb.setNeutralButton("Я", myClickListener); // кнопка нейтрального ответа
//
//        adb.create();
//        adb.show();
//    }
//----------------------------------------------------------------------------------------------
    public static void showAlert(Activity a) {

        AlertDialog alertDialog = new AlertDialog.Builder(a).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Alert message to be shown");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
    //----------------------------------------------------------------------------------------------
    public static void mapRoute(Activity a, LatLng l1, LatLng l2) {

        // "comgooglemaps://?saddr=%@,%@&daddr=%@,%@&zoom=12&directionsmode=walking"
        // "http://maps.google.com/maps?saddr=20.344,34.34&daddr=20.5666,45.345"
        // "google.navigation:q=%f,%f"

        String coord = String.format("http://maps.google.com/maps?saddr=%s,%s&daddr=%s,%s&mode=w",
                decimalFormat(l1.latitude), decimalFormat(l1.longitude),
                decimalFormat(l2.latitude), decimalFormat(l2.longitude));

        Uri gmmIntentUri = Uri.parse(coord);

        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(a.getPackageManager()) != null) {
            a.startActivity(mapIntent);
        }
    }
    //----------------------------------------------------------------------------------------------
    public static void mapYandexRoute(Activity a, LatLng l1, LatLng l2) {

        String coord = String.format("yandexmaps://maps.yandex.ru/?rtext=%s,%s~%s,%s&rtt=mt",
                decimalFormat(l1.latitude), decimalFormat(l1.longitude),
                decimalFormat(l2.latitude), decimalFormat(l2.longitude));

        Uri uri = Uri.parse(coord);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        a.startActivity(intent);

//        String coord = String.format("yandexmaps://maps.yandex.ru/?rtext=%s,%s~%s,%s&rtt=pd",
//                format(l1.latitude), format(l1.longitude),
//                format(l2.latitude), format(l2.longitude));
//
//        Uri gmmIntentUri = Uri.parse(coord);
//
//        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//        mapIntent.setPackage(coord);
//        if (mapIntent.resolveActivity(a.getPackageManager()) != null) {
//            a.startActivity(mapIntent);
//        }
    }
    //----------------------------------------------------------------------------------------------
    public static void telegram(Activity a, String telegram) {

        Uri uri = Uri.parse(telegram);
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("org.telegram.messenger");

        try {
            a.startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            a.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(telegram)));
        }
    }
    //----------------------------------------------------------------------------------------------
//    public static void facebook(Activity a, String id, String name) {
//
//        String p = String.format("https://www.facebook.com/%s",name);
//        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(p));
//        try {
//            a.getPackageManager().getPackageInfo("com.facebook.katana", 0);
//            p = String.format("fb://page/%s",id);
//            i = new Intent(Intent.ACTION_VIEW, Uri.parse(p));
//        } catch (Exception e) {
//        }
//        a.startActivity(i);
//    }
    //----------------------------------------------------------------------------------------------
    public static void facebook(Activity a, String url) {

        PackageManager pm = a.getPackageManager();
        Uri uri = Uri.parse(url);

        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0);
            if (applicationInfo.enabled) {
                uri = Uri.parse("fb://facewebmodal/f?href=" + url);
            }
        }

        catch (PackageManager.NameNotFoundException ignored) {
        }

        Intent i =  new Intent(Intent.ACTION_VIEW, uri);
        a.startActivity(i);
    }
    //----------------------------------------------------------------------------------------------
    public static void instagram(Activity a, String insta) {

        if (insta.isEmpty()) return;
        if (!UtilsString.findText(insta, "instagram")) return;

        Log.d("❤️ ", insta);

        Uri uri = Uri.parse(insta);
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.instagram.android");

        try {
            a.startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            a.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(insta)));
        }
    }
    //----------------------------------------------------------------------------------------------

}
