package mrusta.coffeeMapAndroid.model;

public class CoffeeIndex {

    public int coffee;
    public int address;

    public CoffeeIndex(int coffee, int address) {
        this.coffee = coffee;
        this.address = address;
    }
}