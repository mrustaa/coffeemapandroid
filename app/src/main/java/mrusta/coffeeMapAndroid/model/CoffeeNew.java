package mrusta.coffeeMapAndroid.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CoffeeNew {

    public String title;
    public String street;

    public CoffeeNew(JSONObject json) {

        try {
            title  = json.getString("title");
            street = json.getString("street");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<CoffeeNew> convertArray(List<HashMap<String, String>> array) {

        ArrayList<CoffeeNew> result = new ArrayList<CoffeeNew>();

        for (HashMap<String, String> hashMap : array) {
            JSONObject jsonObj = new JSONObject(hashMap);
            CoffeeNew coffeeNew = new CoffeeNew(jsonObj);
            result.add(coffeeNew);
        }
        return result;
    }

    public static ArrayList<HashMap<String, String>> saveArray(List<CoffeeNew> array) {

        ArrayList<HashMap<String, String>> result = new ArrayList<>();

        for (CoffeeNew coffeeNew : array) {

            HashMap<String,String> hashMap = new HashMap<String,String>();
            hashMap.put("title",  coffeeNew.title);
            hashMap.put("street", coffeeNew.street);
            result.add(hashMap);
        }
        return result;
    }


    public CoffeeNew(CoffeeAddress address) {
        title  = address.coffee.title;
        street = address.street;
    }
}
