package mrusta.coffeeMapAndroid.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import mrusta.coffeeMapAndroid.managers.CoffeeDataManagerFilter;
import mrusta.coffeeMapAndroid.managers.CoffeeJSON;
import mrusta.coffeeMapAndroid.managers.utils.UtilsString;


public class Coffee {

    public String title;
    public String content;
    public String stuff;

    /**  URLs  */
    public String url;
    public String insta;
    public String imageURL;

    /**  Numbers  */
    public String strCappuccino;
    public int       cappuccino;

    public String strEspresso;
    public int       espresso;

    /**  Arrays  */
    public List <CoffeeAddress> address = new ArrayList<>();
    public List <String>           tags = new ArrayList<>();

    /**  Filter Bool  */
    public boolean veg;
    public boolean v60;
    public boolean decaf;
    public boolean sale;
    public boolean dog;
    public boolean specialty;

    static final String K_TITLE = "title";
    static final String K_CONTENT = "content";
    static final String K_STUFF = "stuff";
    static final String K_URL = "url";
    static final String K_INSTA = "insta";
    static final String K_IMAGEURL = "imageURL";
    static final String K_ESPRESSO = "espresso";
    static final String K_CAPPUCCINO = "cappuccino";
    static final String K_TAG = "tags";
    static final String K_ADDRESS = "address";

    //----------------------------------------------------------------------------------------------
    public static ArrayList<Coffee> convertArray(List<HashMap<String, String>> array) {

        ArrayList<Coffee> result = new ArrayList<Coffee>();
        for (HashMap<String, String> hashMap : array) {
            JSONObject jsonObj = new JSONObject(hashMap);
            Coffee coffee = new Coffee(jsonObj);
            CoffeeDataManagerFilter.filterCoffee(coffee);
            result.add(coffee);
        }
        return result;
    }

    //----------------------------------------------------------------------------------------------
    private void initTags(JSONObject json) {
        if (!json.has(K_TAG)) return;

        JSONArray tagsArray = CoffeeJSON.jsonArray(json,K_TAG);
        String[] tags_ = new String[tagsArray.length()];

        for (int i=0; i < tagsArray.length(); i++ ) {
            tags_[i] = CoffeeJSON.jsonArrayString(tagsArray, i);
        }
        tags = Arrays.asList(tags_);
    }
    //----------------------------------------------------------------------------------------------
    private void initAddress(JSONObject json) {
        if (!json.has(K_ADDRESS)) return;

        address = new ArrayList<>();
        JSONArray addressArray = CoffeeJSON.jsonArray(json,K_ADDRESS);
        for (int i = 0; i < addressArray.length(); i++ ) {
            JSONObject jsonObj = CoffeeJSON.jsonArrayObject(addressArray, i);
            CoffeeAddress coffeeAddress = new CoffeeAddress(jsonObj);
            coffeeAddress.coffee = this;
            address.add(coffeeAddress);
        }
    }


    //----------------------------------------------------------------------------------------------
    public Coffee(JSONObject json) {

        title    = CoffeeJSON.jsonString(json,K_TITLE);
        content  = CoffeeJSON.jsonString(json,K_CONTENT);

        stuff    = CoffeeJSON.jsonString(json,K_STUFF);
        url      = CoffeeJSON.jsonString(json,K_URL);
        insta    = CoffeeJSON.jsonString(json, K_INSTA);
        imageURL = CoffeeJSON.jsonString(json, K_IMAGEURL);

        initTags(json);
        initAddress(json);

        strEspresso   =  CoffeeJSON.jsonString(json, K_ESPRESSO);
        strCappuccino =  CoffeeJSON.jsonString(json, K_CAPPUCCINO);

        try { espresso   = Integer.parseInt(strEspresso  ); } catch(NumberFormatException ignored) { }
        try { cappuccino = Integer.parseInt(strCappuccino); } catch(NumberFormatException ignored) { }

    }
    //----------------------------------------------------------------------------------------------
    public String strTags() {

        String tagsSrt = "";
        if (tags.size() != 0) {

            int count = 0;
            for (int i = 0; i < tags.size() ; i++) {
                String str = tags.get(i);
                if (count == 0) tagsSrt = str;
                else tagsSrt = (tagsSrt + ", " + str);
                count++;
            }
        }
        return tagsSrt;
    }
    //----------------------------------------------------------------------------------------------
}
