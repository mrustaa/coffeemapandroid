package mrusta.coffeeMapAndroid.model;

import com.google.android.gms.maps.model.LatLng;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mrusta.coffeeMapAndroid.managers.CoffeeJSON;


public class CoffeeAddress {
    //----------------------------------------------------------------------------------------------
    public LatLng coord;
    public String street;
    public String time;

    public Coffee coffee; // root

    public float distance;

    /**  Filter Bool  */
    public boolean new_;
    public boolean open;
    public boolean favorite;
    public boolean history;
    //----------------------------------------------------------------------------------------------
    public CoffeeAddress(JSONObject json) {

        time       = CoffeeJSON.jsonString(json,"time");
        street     = CoffeeJSON.jsonString(json,"street");

        double lat = CoffeeJSON.jsonDouble(json,"lat");
        double lng = CoffeeJSON.jsonDouble(json,"long");
        coord = new LatLng(lat, lng);
    }
    //----------------------------------------------------------------------------------------------
}