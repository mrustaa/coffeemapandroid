package mrusta.coffeeMapAndroid.framework.recyclerAdapter;

import android.view.View;

public interface RecyclerAdapterClick {
    void recyclerViewListClicked(View v, int position);
}
